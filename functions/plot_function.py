from functions.requirements import *

def time_v(data,s_r):
    # t_end = len(data)/s_r
    # time_vec = np.linspace(0,t_end,len(data))
    # time_vec = np.arange(0,t_end,1/s_r)
    time_vec = np.arange(len(data))/SAMPLING_RATE
    # assert len(time_vec) == len(data), "time length"
    return time_vec


def plot_1_channel_emg(emg,plot_title='EMG signal',xl = 'time (s)', yl = 'amplitude (V)',plot = True,figs=(6,2)):
    t = time_v(emg,SAMPLING_RATE)

    fig, ax = plt.subplots(figsize = figs)
    ax.set(xlabel=xl, ylabel=yl, title = plot_title)
    ax.plot(t,emg)
    # ax.scatter(t,emg)
    fig.tight_layout()
  

def plot_2_channel_emg(emg1, emg2, l2='EMG2', title='EMG signal with respect to time', xl='time (s)', yl='amplitude (V)', yl2 ='filtered EMG', al=1 ,ax_2 = None,one_axe = True,style_2='-',figs=(6,3)):
    assert len(emg1) == len(emg2), 'the 2 signals are not the same length'

    t = time_v(emg1,SAMPLING_RATE)
    
    c_l =sns.color_palette("Set2")
    c_l2 = sns.color_palette()
    # ax.plot(t,emg,color=c_l2[0])
    # ax.plot(t,emg)
    # if len(emg2) != 0:
    #     ax.plot(t,emg2,color=c_l2[-1],alpha=1,linewidth=2)

    if ax_2: 
        ax = ax_2
    else:
        fig, ax = plt.subplots(figsize = figs)
    ax.plot(t, emg1,)

    if not one_axe:
        ax1 = ax.twinx()
        ax1.plot(t, emg2, style_2, label=l2, alpha = al, color = c_l2[-1])
        ax1.set(ylabel = yl2)
        ax1.legend()
    else:
        ax.plot(t, emg2,style_2 , label=l2, alpha = al, color = c_l2[-1])
        ax.legend()

    ax.set(xlabel=xl, ylabel=yl,title= title)
    

    try:
        fig.tight_layout()
    except:
        pass



def plot_2_spectrum(frequency_axis,norm_amplitude,frequency_axis2,norm_amplitude2,figs=(7,2),l1='EMG spectrum 1',l2='EMG spectrum 2',one_axe = True,style='-'):
    fig,ax = plt.subplots(figsize = figs)
    import matplotlib.ticker as tick

    def y_fmt(x, y):
        return '{:1.1e}'.format(x)


    ax.plot(frequency_axis,norm_amplitude,label=l1)
    if one_axe:
        ax.plot(frequency_axis2,norm_amplitude2,label=l2, alpha = 0.7,linestyle = style)
    else:   
        ax1 = ax.twinx()
        ax1.plot(frequency_axis2,norm_amplitude2,label=l2, alpha = 0.7,color = 'orange',linestyle = style)
        ax1.yaxis.set_major_formatter(tick.FuncFormatter(y_fmt))


    ax.set(xlabel='Frequency (Hz)', ylabel= 'Amplitude (V)')
    ax.yaxis.set_major_formatter(tick.FuncFormatter(y_fmt))

    fig.legend()
    fig.tight_layout()


def plot_multiple(*args):
    N = len(args)
    multiple_row = False

    if N<=4:
        row = 1
        col = N
    else:
        row = N//4 +1 if N%4 != 0 else N//4
        col = 4
        multiple_row = True

    print(row,col)
    fig,axis = plt.subplots(row,col,figsize = (col*3,row*2))
    i = 0
    for emgs in args:
        if not 'm' in emgs:
            emgs = [emgs]
        else:
            emgs = emgs[1:]
        for emg in emgs:
            
            if multiple_row:
                axis[i//4,i%4].plot(emg)
            else:
                axis[i].plot(emg)
            
        i +=1

    fig.tight_layout()  




def plot_rating_measurement(df_rs_val,score,max_h=90,pc=False):
    fig,ax = plt.subplots(figsize = (6,3))
    if pc:    
        sns.set_theme()
        # hist = sns.histplot(df_rs_val.loc[df_rs_val['Group']=='Control',:],x=f'{score}_score',multiple='dodge',ax=ax,stat="percent")
        # histp = sns.histplot(df_rs_val.loc[df_rs_val['Group']=='Patient',:],x=f'{score}_score',multiple='dodge',ax=hist,stat="percent")
        hist = sns.histplot(df_rs_val,x=f'{score}_score',multiple='dodge',hue='Group',ax=ax,stat="percent")


        # hist = sns.histplot([df_rs_val.loc[df_rs_val['Group']=='Control'],df_rs_val.loc[df_rs_val['Group']=='Patient']],hue='Group',x=f'{score}_score',ax=ax,stat="percent")
        # hist_task = sns.histplot(df_rs_val.loc[df_rs_val['Group']=='Patient'],x=f'{score}_score',ax=ax,stat='percent',multiple='dodge')

    else: hist = sns.histplot(df_rs_val,x=f'{score}_score',ax=ax,stat="percent")
    ax.set(ylabel='percent (count)', title=f'Rating of {score}')

    ax.set_ylim(0,max_h)
    patches = hist.patches

    for i in range(len(patches)):
        x = patches[i].get_x() + patches[i].get_width()/2
        y = patches[i].get_height()+ 2
        ax.annotate(f'{patches[i].get_height():.1f}%\n({patches[i].get_height()*df_rs_val.shape[0]/100:.1f})' if patches[i].get_height() != 0 else '', (x, y), ha='center')
        fig.tight_layout()

    fig.tight_layout()





def hist_value(hist,ax,df,shift=2,count = True,h_max=0):
    """Annotate the height of the bar of an histogram

    Args:
        hist (_type_): sns.histplot
        ax (_type_): ax used to plot the histogram
        df (_type_): df used to plot the histogram
        shift (int, optional): _description_. Defaults to 2.
        count (bool, optional): if True, add the number of value in addition to the percentage
        h_max (int, optional): _description_. Defaults to 0.
    """
    patches = hist.patches
    y_list = []

    try:
        nb_samp =df.shape[0]/100
    except:
        nb_samp = len(df)

    
    for i in range(len(patches)):
        x = patches[i].get_x() + patches[i].get_width()/2
        y = patches[i].get_height()+ shift
        y_list.append(y)
        if count: ax.annotate(f'{patches[i].get_height():.1f}%\n({patches[i].get_height()*nb_samp:.1f})' if patches[i].get_height() != 0 else '', (x, y), ha='center',size=9)
        else: ax.annotate(f'{patches[i].get_height():.1f}%' if patches[i].get_height() != 0 else '', (x, y), ha='center',size=9)
    if h_max != 0: ax.set_ylim(0,h_max)



def plot_percent_task_channel_value(df_all_values,h_max=None):
    """Plot the distribution of outliers across muscles

    Args:
        df_all_values (DataFrame): output of the function get_all_values()
        h_max (_type_, optional): adjusts the maximal height of the plot
    """
    sns.set_style("darkgrid")

    fig,ax = plt.subplots(1,2,figsize = (10,3))
    hist1 = sns.histplot(data=df_all_values,x='task',binwidth=1,ax=ax[0],stat='percent',shrink=.8)
    hist2 =sns.histplot(data=df_all_values,x='channel',binwidth=1,ax=ax[1],stat='percent',shrink=.8)
    ax[0].set_xticklabels(ax[0].get_xticklabels(), rotation=30,horizontalalignment='right')
    ax[1].set_xticklabels(ax[1].get_xticklabels(), rotation=30,horizontalalignment='right')
    hist_value(hist1,ax[0],df_all_values,count=False,h_max=h_max)
    hist_value(hist2,ax[1],df_all_values,count=False,h_max=h_max)
    ax[0].tick_params(labelsize = 9)
    ax[1].tick_params(labelsize = 9)
    hist1.grid(axis='x')
    hist2.grid(axis='x')

    fig.suptitle( 'Percentages of tasks and muscles studied across all Biomarkers')

    fig.tight_layout()







def plot_extract_event(emg,emg2,event_,event_type,task_name = None,al=0.8,figs=FIG_SIZE,xl='time (s)',yl = 'Amplitude (V)',plot_title = 'EMG signal and its events',ax = None):
    if not ax:
        fig,ax = plt.subplots(figsize= figs)
    al1 = 0.15
    if event_type in ['Gait','']: 
        event = event_.copy(deep = True)
        event['time']= event.apply(lambda x: x.time_sec + 60* x.time_min, axis=1)
        event = event.sort_values(by = ['time']).reset_index(drop=True) 

    elif task_name == 'Endurance_Sorensen':
        event = event_.copy(deep = True)
        event = event.sort_values(by = ['time']).reset_index(drop=True) 

    event_line = -1* np.ones_like(emg) 

    start_mvc = 0
    stop_mvc = 0
    start_endu = 0
    stop_endu = 0
    
        
    # For Endurance Sorensens or Endurance Ito
    if event_type == 'Endu': # the signal is used from start to finish
        if task_name == 'Endurance_Sorensen':
            start_endu = event.loc[event['event'] == 'startEndurance']['time'].values[0] # in secondes
            stop_endu = event.loc[event['event'] == 'stopEndurance']['time'].values[0]
            event_line[int(start_endu*SAMPLING_RATE):int(stop_endu*SAMPLING_RATE)] = 1

        else: 
            event_line[int(3*SAMPLING_RATE):int(len(emg)*0.85)] = 1
            
        
    elif event_type == 'MVC': # only 3 secondes of the signal are used
        if task_name == 'Endurance_Sorensen':
            start_endu = event.loc[event['event'] == 'startEndurance']['time'].values[0] # in secondes
            event_line[int(start_endu*SAMPLING_RATE):int((start_endu+3)*SAMPLING_RATE)] = 1
        else: 
            event_line[int(5*SAMPLING_RATE):int(8*SAMPLING_RATE)] = 1
            


    # Other Tasks different from Gait
    elif event_type != 'Gait':
        current_event = 'stopMotion'
 
        for i in range(len(event['time'])-1): # the frames will have no labels from the first frame to the first event and from the last event to the last frame
            current_time = event['time'][i]
            future_time = event['time'][i+1]
            current_event = event['event'][i]
            event_line[int(current_time * SAMPLING_RATE): int(future_time*SAMPLING_RATE)] = 0 if current_event == 'stopMotion' else 1

    # Gait
    else:
        
        for i in range(1,len(event['time'])): # the frames will have no labels from the first frame to the first event and from the last event to the last frame
            following_time = event['time'][i]
            actual_time = event['time'][i-1]
            current_event = event['event'][i-1]
            event_line[int(actual_time * SAMPLING_RATE): int(following_time * SAMPLING_RATE)] = 0 if current_event == 'RHS' else 1 if current_event == 'RTO' else 2 if current_event == 'LHS' else 3

        # current_event = event['event'].iloc[-1]
        # event_line[int(event['time'].iloc[-1] * SAMPLING_RATE): ] = 0 if current_event == 'RHS' else 1 if current_event == 'RTO' else 2 if current_event == 'LHS' else 3


    #plotting the lines
    if event_type == 'Gait' or event_type == '' or task_name=='Endurance_Sorensen':

        for i in range(1,len(event['time'])):
            x,next_x,current_event = event['time'][i-1],event['time'][i], event['event'][i-1]
            p1 = plt.axvline(x=x,color='white')
            if event_type == 'Gait':
                ax.axvspan(x, next_x, color='red' if current_event == 'RHS' else 'orange' if current_event == 'RTO' else 'yellow' if current_event == 'LHS' else 'green', alpha=al1)
    
            elif event_type == '' or task_name=='Endurance_Sorensen':
                
                ax.axvspan(x, next_x, color= 'green' if current_event == 'stopMotion' else 'orange' , alpha=al1)


    else:
        ax.axvspan(start_mvc, stop_mvc, color='cyan', alpha = al1,label =  'mvc' )
        ax.axvspan(start_endu, stop_endu, color='green', alpha = al1 ,label =  'endurance' )
        ax.legend()
        print(f'Start Endurance: {start_endu}, Start MVC: {start_mvc}, Stop MVC: {stop_mvc}, Stop Endurance: {stop_endu}')

    
    t = np.arange(0,len(emg))/SAMPLING_RATE
    ax.set(xlabel=xl, ylabel=yl, title = plot_title)


    c_l =sns.color_palette("Set2")
    c_l2 = sns.color_palette()
    ax.plot(t,emg,color=c_l2[0])
    ax.plot(t,emg2,color=c_l2[-1],alpha=1,linewidth = 2.2)
    fig.tight_layout()



import math
from matplotlib.patches import Rectangle

def plot_colortable( color_ref,add='',name_l='',ncols=3, sort_colors=True):
    # not personnaly written, missing source
    # plot a palette

    cell_width = 212
    cell_height = 30
    swatch_width = 48
    margin = 12

    # Sort colors by hue, saturation, value and name.
    names = color_ref.keys()
   
    n = len(names)
    nrows = math.ceil(n / ncols)

    width = cell_width * 4 + 2 * margin
    height = cell_height * nrows + 2 * margin
    dpi = 72

    fig, ax = plt.subplots(figsize=(width / dpi, height / dpi), dpi=dpi)
    fig.subplots_adjust(margin/width, margin/height,
                        (width-margin)/width, (height-margin)/height)
    ax.set_xlim(0, cell_width * 4)
    ax.set_ylim(cell_height * (nrows-0.5), -cell_height/2.)
    ax.yaxis.set_visible(False)
    ax.xaxis.set_visible(False)
    ax.set_axis_off()

    for i, name in enumerate(names):
        row = i % nrows
        col = i // nrows
        y = row * cell_height

        swatch_start_x = cell_width * col
        text_pos_x = cell_width * col + swatch_width + 7

        ax.text(text_pos_x, y, f'{name}{add}', fontsize=14,
                horizontalalignment='left',
                verticalalignment='center')

        ax.add_patch(
            
            Rectangle(xy=(swatch_start_x, y-9), width=swatch_width,
                      height=18, facecolor=color_ref[name], edgecolor='0.7')
        )
    fig.suptitle(f'Labels {name_l}', fontsize=15)    
    fig.tight_layout()