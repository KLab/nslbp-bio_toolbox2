
from functions import *


# BMu_function, preprocessing,muscles,task, mvc_computation,mvc_task,contraction_computation,event_computation,event_type,kwargs = BMu_dict['BMu'+str(BMu_nb)]
# 'BMu37': [BMu_Du_2018,preprocessing_du_2018,['L_EO','R_EO'],'Trunk_Forward', False,'',False,True,'',{'nb_BM':37,'subphase':'all','muscle':'R_EO'}],

BMu_dict = {
    
    'BMu1': [BMu1,preprocessing_ahern_1988,['L_LES','R_LES'],'Trunk_Forward',False,'',False,True,'',{}],
    'BMu1_Neblett': [BMu1_Neblett,preprocessing_neblett_2014,['L_LES','R_LES'],'Trunk_Forward',False,'',False,True,'',{}],

    'BMu15_ahern': [BMu15,preprocessing_ahern_1988,['L_LES','R_LES'],'Trunk_Forward',False,'',False,True,'',{}],
    'BMu15': [BMu15_neblett,preprocessing_neblett_2014,['L_LES','R_LES'],'Trunk_Forward',False,'',False,True,'',{}],
    'BMu15_rl': [BMu15_neblett_rl,preprocessing_neblett_2014,['L_LES','R_LES'],'Trunk_Forward',False,'',False,True,'',{}],


    'BMu22': [BMu22,preprocessing_lima_2018,['L_LES','R_LES'],'S2S_Unconstrained',False,'',True,True,'',{}],
    'BMu23': [BMu23,preprocessing_lima_2018,['L_ESI','R_ESI'],'S2S_Unconstrained',False,'',True,True,'',{}],
    'BMu24': [BMu24,preprocessing_lima_2018,['L_MTF','R_MTF'],'S2S_Unconstrained',False,'',True,True,'',{}],
    'BMu29': [BMu29,preprocessing_du_2018,['L_MTF','R_MTF'],'Trunk_Forward',False,'',False,True,'',{}],
    'BMu29_norm': [BMu29_norm,preprocessing_du_2018,['L_MTF','R_MTF'],'Trunk_Forward',True,'Endurance_Sorensen',False,True,'',{}],

    'BMu34': [BMu34,preprocessing_du_2018,['L_MTF','R_MTF'],'Trunk_Lateral',False,'',False,True,'',{}],
    'BMu34_norm': [BMu34_norm,preprocessing_du_2018,['L_MTF','R_MTF'],'Trunk_Lateral',True,'Endurance_Sorensen',False,True,'',{}],

    'BMu35': [BMu35, preprocessing_neblett_2014, ['L_LES','R_LES'],'Trunk_Forward',False,'',False,True,'',{}],
    'BMu35_rl': [BMu35_rl, preprocessing_neblett_2014, ['L_LES','R_LES'],'Trunk_Forward',False,'',False,True,'',{}],

    'BMu36': [BMu36,preprocessing_dankaerts_2009, ['L_MTF','R_MTF'],'Trunk_Forward',True,'Endurance_Sorensen',False,True,'',{}],
    'BMu36_invfw': [BMu36_invfw,preprocessing_dankaerts_2009_invfw, ['L_MTF','R_MTF'],'Trunk_Forward',True,'Endurance_Sorensen',False,True,'',{}],
    'BMu36_Du': [BMu36_Du,preprocessing_du_2018, ['L_MTF','R_MTF'],'Trunk_Forward',False,'',False,True,'',{}],
    'BMu36_Du_norm': [BMu36_Du_norm,preprocessing_du_2018, ['L_MTF','R_MTF'],'Trunk_Forward',True,'Endurance_Sorensen',False,True,'',{}],


    'BMu37': [BMu37,preprocessing_du_2018,['L_EO','R_EO'],'Trunk_Forward',False,'',False,True,'',{}],
    'BMu37_norm': [BMu37_norm,preprocessing_du_2018,['L_EO','R_EO'],'Trunk_Forward',True,'Endurance_Ito',False,True,'',{}],
    'BMu38': [BMu38,preprocessing_du_2018,['L_MTF','R_MTF'],'Trunk_Forward',False,'',False,True,'',{}],
    'BMu38_norm': [BMu38_norm,preprocessing_du_2018,['L_MTF','R_MTF'],'Trunk_Forward',True,'Endurance_Sorensen',False,True,'',{}],
    'BMu40': [BMu40,preprocessing_du_2018,['L_EO','R_EO'],'Trunk_Forward',False,'',False,True,'',{}],
    'BMu40_norm': [BMu40_norm,preprocessing_du_2018,['L_EO','R_EO'],'Trunk_Forward',True,'Endurance_Ito',False,True,'',{}],
    'BMu41': [BMu41,preprocessing_du_2018,['L_EO','R_EO'],'Trunk_Forward',False,'',False,True,'',{}],
    'BMu41_norm': [BMu41_norm,preprocessing_du_2018,['L_EO','R_EO'],'Trunk_Forward',True,'Endurance_Ito',False,True,'',{}],

    'BMu42': [BMu42,preprocessing_neblett_2014,['L_LES','R_LES'],'Trunk_Forward',False,'',False,True,'',{}],
    'BMu42_watson': [BMu42_watson,preprocessing_watson_1997,['L_LES','R_LES'],'Trunk_Forward',False,'',False,True,'',{}],
    'BMu42_rl': [BMu42_rl,preprocessing_neblett_2014,['L_LES','R_LES'],'Trunk_Forward',False,'',False,True,'',{}],
    'BMu43': [BMu43,preprocessing_neblett_2014, ['L_LES','R_LES'],'Trunk_Forward', False,'',False,True,'',{}],
    'BMu43_rl': [BMu43_rl,preprocessing_neblett_2014, ['L_LES','R_LES'],'Trunk_Forward', False,'',False,True,'',{}],

    'BMu44': [BMu44, preprocessing_neblett_2014, ['L_LES','R_LES'],'Trunk_Forward',False,'',False,True,'',{}],
    'BMu44_rl': [BMu44_rl, preprocessing_neblett_2014, ['L_LES','R_LES'],'Trunk_Forward',False,'',False,True,'',{}],


    'BMu51': [BMu51, preprocessing_du_2018, ['L_MTF','R_MTF'], 'Trunk_Lateral',False,'',False,True,'',{}],
    'BMu51_norm': [BMu51_norm, preprocessing_du_2018, ['L_MTF','R_MTF'], 'Trunk_Lateral',True,'Endurance_Sorensen',False,True,'',{}],
    'BMu52': [BMu52, preprocessing_du_2018, ['L_EO','R_EO'], 'Trunk_Lateral',False,'',False,True,'',{}],
    'BMu52_norm': [BMu52_norm, preprocessing_du_2018, ['L_EO','R_EO'], 'Trunk_Lateral',True,'Endurance_Ito',False,True,'',{}],
    'BMu53': [BMu53, preprocessing_du_2018, ['L_MTF','R_MTF'], 'Trunk_Lateral',False,'',False,True,'',{}],
    'BMu53_norm': [BMu53_norm, preprocessing_du_2018, ['L_MTF','R_MTF'], 'Trunk_Lateral',True,'Endurance_Sorensen',False,True,'',{}],


    'BMu67': [BMu67, preprocessing_dankaerts_2006, ['L_ESI','R_ESI'], 'Posture_Sitting', True, 'Endurance_Sorensen',False,False,'',{}],
    'BMu67_abw': [BMu67_abw, preprocessing_dankaerts_2006_abw, ['L_ESI','R_ESI'], 'Posture_Sitting', True, 'Endurance_Sorensen',False,False,'',{}],
    'BMu67_invfw': [BMu67_invfw, preprocessing_dankaerts_2006_invfw, ['L_ESI','R_ESI'], 'Posture_Sitting', True, 'Endurance_Sorensen',False,False,'',{}],
    'BMu68': [BMu68, preprocessing_dankaerts_2006, ['L_MTF','R_MTF'],'Posture_Sitting', True, 'Endurance_Sorensen',False,False,'',{}],
    'BMu68_invfw': [BMu68_invfw, preprocessing_dankaerts_2006_invfw, ['L_MTF','R_MTF'],'Posture_Sitting', True, 'Endurance_Sorensen',False,False,'',{}],

    'BMu69': [BMu69,preprocessing_lima_2018,['L_LES','R_LES'],'S2S_Unconstrained',False,'',True,True,'',{}],

    'BMu81': [BMu81,preprocessing_lima_2018,['L_ESI','R_ESI'],'S2S_Unconstrained',False,'',True,True,'',{}],
    'BMu82': [BMu82,preprocessing_lima_2018,['L_MTF','R_MTF'],'S2S_Unconstrained',False,'',True,True,'',{}],

    'BMu86': [BMu86,preprocessing_mehta_2000,['L_EO','R_EO'],'Perturbation_R_Shoulder', False,'',True,False,'',{}], 
    'BMu86_abw': [BMu86_abw,preprocessing_mehta_2000_ABW,['L_EO','R_EO'],'Perturbation_R_Shoulder', False,'',True,False,'',{}], 
    'BMu86_fas': [BMu86_fas,preprocessing_mehta_2000_fas,['L_EO','R_EO'],'Perturbation_R_Shoulder', False,'',True,False,'',{}], 
    'BMu86_bwinv': [BMu86_bwinv,preprocessing_mehta_2000_bwinv,['L_EO','R_EO'],'Perturbation_R_Shoulder', False,'',True,False,'',{}], 
    'BMu86_bwinv2': [None,preprocessing_mehta_2000_bwinv2,['L_EO','R_EO'],'Perturbation_R_Shoulder', False,'',True,False,'',{}], 
    'BMu86_bwinvclip': [None,preprocessing_mehta_2000_bwinvclip,['L_EO','R_EO'],'Perturbation_R_Shoulder', False,'',True,False,'',{}], 
    'BMu86_fas_bwinv': [BMu86_fas_bwinv,preprocessing_mehta_2000_fas_bwinv,['L_EO','R_EO'],'Perturbation_R_Shoulder', False,'',True,False,'',{}], 
    'BMu86_fas_bwinv2': [None,preprocessing_mehta_2000_fas_bwinv2,['L_EO','R_EO'],'Perturbation_R_Shoulder', False,'',True,False,'',{}], 



    'BMu87': [BMu87,preprocessing_mehta_2000,['L_RA','R_RA'],'Perturbation_R_Shoulder', False,'',True,False,'',{}],
    'BMu87_fas': [BMu87_fas,preprocessing_mehta_2000_fas,['L_RA','R_RA'],'Perturbation_R_Shoulder', False,'',True,False,'',{}],
    'BMu87_bwinv': [BMu87_bwinv,preprocessing_mehta_2000_bwinv,['L_RA','R_RA'],'Perturbation_R_Shoulder', False,'',True,False,'',{}], 
    'BMu87_fas_bwinv': [BMu87_fas_bwinv,preprocessing_mehta_2000_fas_bwinv,['L_RA','R_RA'],'Perturbation_R_Shoulder', False,'',True,False,'',{}], 
    'BMu87_fas_bwinv2': [None,preprocessing_mehta_2000_fas_bwinv2,['L_RA','R_RA'],'Perturbation_R_Shoulder', False,'',True,False,'',{}], 

    'BMu88': [BMu88,preprocessing_mehta_2000,['L_MTF','R_MTF'],'Perturbation_R_Shoulder', False,'',True,False,'',{}],
    'BMu88_fas': [BMu88_fas,preprocessing_mehta_2000_fas,['L_MTF','R_MTF'],'Perturbation_R_Shoulder', False,'',True,False,'',{}],
    'BMu88_bwinv': [BMu88_bwinv,preprocessing_mehta_2000_bwinv,['L_MTF','R_MTF'],'Perturbation_R_Shoulder', False,'',True,False,'',{}], 
    'BMu88_fas_bwinv': [BMu88_fas_bwinv,preprocessing_mehta_2000_fas_bwinv,['L_MTF','R_MTF'],'Perturbation_R_Shoulder', False,'',True,False,'',{}], 
    'BMu88_fas_bwinv2': [None,preprocessing_mehta_2000_fas_bwinv2,['L_MTF','R_MTF'],'Perturbation_R_Shoulder', False,'',True,False,'',{}], 

    'BMu91': [BMu91_stand,preprocessing_neblett_2014, ['L_LES','R_LES'],'Posture_Standing',False,'',False,False,'',{}],
    'BMu91_rl': [BMu91_standrl,preprocessing_neblett_2014, ['L_LES','R_LES'],'Posture_Standing',False,'',False,False,'',{}],
    'BMu91_flex': [BMu91_rl,preprocessing_neblett_2014, ['L_LES','R_LES'],'Trunk_Forward',False,'',False,True,'',{}],


    'BMu96': [BMu96,preprocessing_lima_2018,['L_LES','R_LES'],'Weight_Unconstrained',False,'',True,True,'',{}],
    'BMu96_norm': [BMu96_norm,preprocessing_lima_2018,['L_LES','R_LES'],'Weight_Unconstrained',True,'Endurance_Sorensen',True,True,'',{}],
    'BMu96_Lariviere':[BMu96_Lariviere,preprocessing_lariviere_2002,['L_LES','R_LES'],'Weight_Unconstrained',False,'',False,True,'',{}],
    # 'BMu96_LariviereI':[BMu96_Lariviere,preprocessing_lariviereI_2002,['L_LES','R_LES'],'Weight_Unconstrained',False,'',False,True,'',{}],
    'BMu96_Courbalay':[BMu96_Courbalay, preprocessing_courbalay_2017,['L_LES','R_LES'],'Weight_Unconstrained',True,'Endurance_Sorensen',False,True,'',{}],
    'BMu97': [BMu97,preprocessing_lima_2018,['L_ESI','R_ESI'],'Weight_Unconstrained',False,'',True,True,'',{}],
    'BMu97_norm': [BMu97_norm,preprocessing_lima_2018,['L_ESI','R_ESI'],'Weight_Unconstrained',True,'Endurance_Sorensen',True,True,'',{}],
    'BMu98': [BMu98,preprocessing_lima_2018,['L_MTF','R_MTF'],'Weight_Unconstrained',False,'',True,True,'',{}],


    'BMu100': [BMu100,preprocessing_arendt_nielsen_1995,['L_ESI','R_ESI'],'Gait_Normal',False,'',False,True,'Gait',{}],
    'BMu100_ans': [BMu100_ans,preprocessing_ansari_2018,['L_ESI','R_ESI'],'Gait_Normal',True,'Endurance_Sorensen',False,True,'Gait',{}],
    'BMu101': [BMu101,preprocessing_arendt_nielsen_1995,['L_MTF','R_MTF'],'Gait_Normal',False,'',False,True,'Gait',{}],
    'BMu101_ans': [BMu101_ans,preprocessing_ansari_2018,['L_MTF','R_MTF'],'Gait_Normal',True,'Endurance_Sorensen',False,True,'Gait',{}],
    'BMu102': [BMu102,preprocessing_arendt_nielsen_1995,['L_ESI','R_ESI'],'Gait_Normal',False,'',False,True,'Gait',{}],
    'BMu102_norm': [BMu102_norm,preprocessing_arendt_nielsen_1995,['L_ESI','R_ESI'],'Gait_Normal',True,'Endurance_Sorensen',False,True,'Gait',{}],
    'BMu103': [BMu103,preprocessing_arendt_nielsen_1995,['L_MTF','R_MTF'],'Gait_Normal',False,'',False,True,'Gait',{}],
    'BMu104': [BMu104,preprocessing_arendt_nielsen_1995,['L_LES','R_LES'],'Gait_Normal',False,'',False,True,'Gait',{}],
    'BMu105': [BMu105,preprocessing_arendt_nielsen_1995,['L_ESI','R_ESI'],'Gait_Normal',False,'',False,True,'Gait',{}],
    'BMu106': [BMu106,preprocessing_arendt_nielsen_1995,['L_MTF','R_MTF'],'Gait_Normal',False,'',False,True,'Gait',{}],
    'BMu107': [BMu107,preprocessing_arendt_nielsen_1995,['L_LES','R_LES'],'Gait_Normal',False,'',False,True,'Gait',{}],
    'BMu108': [BMu108,preprocessing_arendt_nielsen_1995,['L_LES','R_LES'],'Gait_Normal',False,'',False,True,'Gait',{}],
    'BMu108_norm': [BMu108_norm,preprocessing_arendt_nielsen_1995,['L_LES','R_LES'],'Gait_Normal',True,'Endurance_Sorensen',False,True,'Gait',{}],
    'BMu109': [BMu109,preprocessing_lamoth_2006,['L_LES','R_LES'],'Gait_Normal',False,'',False,True,'Gait',{}],
    'BMu109_norm': [BMu109_norm,preprocessing_lamoth_2006,['L_LES','R_LES'],'Gait_Normal',True,'Endurance_Sorensen',False,True,'Gait',{}],


    'BMu110': [BMu110,preprocessing_pakdaz_2016,['L_RA','R_RA'],'Gait_Normal',True,'Endurance_Ito',False,True,'Gait',{}],
    'BMu111': [BMu111,preprocessing_pakdaz_2016,['L_EO','R_EO'],'Gait_Normal',True,'Endurance_Ito',False,True,'Gait',{}],
    'BMu112': [BMu112,preprocessing_pakdaz_2016,['L_ESI','R_ESI'],'Gait_Normal',True,'Endurance_Sorensen',False,True,'Gait',{}],
    'BMu113': [BMu113,preprocessing_pakdaz_2016,['L_MTF','R_MTF'],'Gait_Normal',True,'Endurance_Sorensen',False,True,'Gait',{}],


    'BMu114':[BMu114,preprocessing_van_der_hulst_2010,['L_LES','R_LES'],'Gait_Normal',False,'',False,True,'Gait',{}],
    'BMu115':[BMu115,preprocessing_van_der_hulst_2010,['L_LES','R_LES'],'Gait_Normal',False,'',False,True,'Gait',{}],
    # 'BMu117':[BMu117,lambda x: x,['L_LES','R_LES'],'Endurance_Sorensen',False,'',False,False,'',{}],
    'BMu117':[BMu117,preprocessing_paasuke_2002,['L_LES','R_LES'],'Endurance_Sorensen',False,'',False,True,'Endu',{}],
    # 'BMu118':[BMu118,lambda x: x,['L_LES','R_LES'],'Endurance_Sorensen',False,'',False,False,'',{}],
    'BMu118':[BMu118,preprocessing_suuden_2014,['L_LES','R_LES'],'Endurance_Sorensen',False,'',False,True,'Endu',{}],
    'BMu118_suter':[BMu118_suter,preprocessing_suter_2001,['L_LES','R_LES'],'Endurance_Sorensen',False,'',False,True,'Endu',{}],
    # 'BMu119':[BMu119,lambda x: x,['L_LES','R_LES'],'Endurance_Sorensen',False,'',False,False,'',{}],
    'BMu119':[BMu119,preprocessing_paasuke_2002,['L_LES','R_LES'],'Endurance_Sorensen',False,'',False,True,'Endu',{}]}

