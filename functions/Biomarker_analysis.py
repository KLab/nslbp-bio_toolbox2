
from functions.requirements import *
from functions.plot_function import *
from functions.c3d_function import *
from functions.processing_function import *
from functions.Biomarker_dict import BMu_dict

####################################################################################################
# This file comprises all functions related to the analysis of BMu once there are computed
####################################################################################################






def BMu_remove_outlier_PC(BMu,factor=1.5): 
    BMu_P = BMu.loc[BMu['P_C'] == 'P']
    BMu_C = BMu.loc[BMu['P_C'] == 'C']

    def IRQ(BM,BMref,L):
        Q1 = BMref['value'].quantile(0.25) # BMref instead of BMu
        Q3 = BMref['value'].quantile(0.75)

        IQR = Q3 - Q1
        bool_list = ( (BM['value'] < (Q1 - factor * IQR)) | (BM['value'] > (Q3 + factor * IQR)) ) 
        bool_list = bool_list & (BM['P_C']==L)
        return  bool_list
    
    bool_P = IRQ(BMu,BMu_P,'P') # The values of the participants LBP are the references for Q1 and Q3
    bool_C = IRQ(BMu,BMu_C,'C')

    outliers = [ True if p or c else False for p,c in zip(bool_P , bool_C)]
    outliers = bool_P|bool_C

    return outliers


# def BMu_remove_outlier_PC(BMu,factor=1.5): 
#     BMu_P = BMu.loc[BMu['P_C'] == 'P']
#     BMu_C = BMu.loc[BMu['P_C'] == 'C']

#     Q1_p,Q3_p = BMu_P['value'].quantile(0.25),BMu_P['value'].quantile(0.75)
#     Q1_c,Q3_c = BMu_C['value'].quantile(0.25),BMu_C['value'].quantile(0.75)
#     IQR_p = Q3_p - Q1_p
#     IQR_c = Q3_c - Q1_c

#     outliers = []
#     for i,row in BMu.iterrows():
#         if row['subject'] in SUBJECT_P:
#             outliers += [ (row['value'] < (Q1_p - factor * IQR_p)) | (row['value'] > (Q3_p + factor * IQR_p)) ]
#         if row['subject'] in SUBJECT_C:
#             outliers += [( row['value'] < (Q1_c - factor * IQR_c)) | (row['value'] > (Q3_c + factor * IQR_c)) ]

#     return np.array(outliers)


# def extract_values_BMu_PC(BMu_results_,outlier_present=True,left = False):  

#     if not outlier_present:
#         outlier = BMu_remove_outlier_PC(BMu_results_)
#         BMu_results = BMu_results_.loc[~outlier]
#     else:
#         BMu_results = BMu_results_


#     BMu_P = BMu_results.loc[BMu_results.P_C == 'P','value']
#     BMu_C = BMu_results.loc[BMu_results.P_C == 'C','value']
#     std_p = np.nanstd(BMu_P.values)
#     std_c = np.nanstd(BMu_C.values)
#     mean_c,mean_p = np.nanmean(BMu_C.values), np.nanmean(BMu_P.values)
#     mean_diff = mean_c - mean_p


#     return BMu_P,BMu_C, std_p,std_c, mean_p,mean_c,mean_diff

# Notebook short presentation------------------------------------------------------------------------------------------------------------


def extract_values_BMu_PC_RL(BMu_results_,outlier_present=True,factor=1.5,left = True,BMu_nb=0,desc='',both = False):  
    """compute mean and std for each group
    """

    if not outlier_present:
        outlier = BMu_remove_outlier_PC(BMu_results_,factor=factor)
        BMu_results = BMu_results_.loc[~outlier]
    else:
        BMu_results = BMu_results_

    muscle  = BMu_results.iloc[1]['channel'][2:]
    if (left and not both) and ((BMu_nb not in [44,43,91,42,35,15]) or ((BMu_nb in [44,43,91,42,35,15]) and desc=='_rl')):
        BMu_results = BMu_results.loc[BMu_results['channel']==f'L_{muscle}']
    elif (not left and not both):
        BMu_results = BMu_results.loc[BMu_results['channel']==f'R_{muscle}']


    BMu_P = BMu_results.loc[BMu_results.P_C == 'P','value']
    BMu_C = BMu_results.loc[BMu_results.P_C == 'C','value']
    std_p = np.nanstd(BMu_P.values)
    std_c = np.nanstd(BMu_C.values)
    mean_c,mean_p = np.nanmean(BMu_C.values), np.nanmean(BMu_P.values)
    mean_diff = mean_c - mean_p



    return BMu_P,BMu_C, std_p,std_c, mean_p,mean_c,mean_diff



def hist_values_BMu(BMu_,outlier_present=True):
    
    if not outlier_present:
        outlier = BMu_remove_outlier_PC(BMu_)
        BMu = BMu_.loc[~outlier]
    else:
        BMu = BMu_

    BMu_P = BMu.loc[BMu.P_C == 'P']
    BMu_C = BMu.loc[BMu.P_C == 'C']
    fig, ax = plt.subplots(nrows=2, ncols=2,figsize = [8,4])
    sns.histplot(BMu,x='value', hue='P_C',ax=ax[0,0],kde=True)
    ax[0,0].set(xlabel='BMu values', title = 'Histogramme of BMu values')
    sns.boxplot(BMu,x='value', y='P_C',ax= ax[0,1],showfliers=outlier_present)
    ax[0,1].set(xlabel='BMu values', title = 'Boxplot of BMu values')
    sns.histplot(BMu_P,x='value', hue='channel',ax=ax[1,0],kde=True)
    ax[1,0].set(xlabel='BMu values', title = 'Histogramme of BMu values Patient')
    sns.histplot(BMu_C,x='value', hue='channel',ax=ax[1,1],kde=True)
    ax[1,1].set(xlabel='BMu values', title = 'Histogramme of BMu values Control')
    fig.tight_layout()


def box_values_BMu(BMu_,outlier_present=True,factor=1.5,save=True,name='boxplot',repo='',unit=''): # replaced by  box_values_BMu_llbp_hlbp
    
    if not outlier_present:
        outlier = BMu_remove_outlier_PC(BMu_,factor=factor)
        BMu = BMu_.loc[~outlier]
    else:
        BMu = BMu_

    BMu_P = BMu.loc[BMu.P_C == 'P']
    BMu_C = BMu.loc[BMu.P_C == 'C']

    outlier_present=True

    fig, ax = plt.subplots(nrows=3, ncols=2,figsize = [9,7])
    sns.histplot(data=BMu,x='value', hue='P_C',ax=ax[0,0],kde=True)
    ax[0,0].set(xlabel=f'BMu values ({unit})', title = 'Histogram of BMu values')

    sns.boxplot(data=BMu,x='value', y='P_C',ax= ax[0,1],showfliers=outlier_present,showmeans=True,
            meanprops={"marker": "+",
                       "markeredgecolor": "black",
                       "markersize": "10"})
    ax[0,1].set(xlabel=f'BMu values ({unit})', title = 'Boxplot of BMu values')


    sns.boxplot(data=BMu_P,x='value', y='channel',ax=ax[2,0],showfliers=outlier_present,showmeans=True,
            meanprops={"marker": "+",
                       "markeredgecolor": "black",
                       "markersize": "10"})
    ax[2,0].set(xlabel=f'BMu values ({unit})', title = 'Boxplot of BMu values Patient')


    sns.boxplot(data=BMu_C,x='value', y='channel',ax=ax[2,1],showfliers=outlier_present,showmeans=True,
            meanprops={"marker": "+",
                       "markeredgecolor": "black",
                       "markersize": "10"})
    ax[2,1].set(xlabel=f'BMu values ({unit})', title = 'Boxplot of BMu values Control')


    gs = ax[1, 0].get_gridspec()
    ax[1,0].remove()
    ax[1,1].remove()
    axbig = fig.add_subplot(gs[1, 0:])
    sns.boxplot(data=BMu,x='value', y='channel',hue ='P_C', ax=axbig,showfliers=outlier_present,showmeans=True,
            meanprops={"marker": "+",
                       "markeredgecolor": "black",
                       "markersize": "10"})
    axbig.set(xlabel=f'BMu values ({unit})', title = 'Boxplot of BMu values left/right sides for patients and controls')
    sns.move_legend(axbig,'lower right')

    fig.tight_layout()
    
    if save :
        fig.savefig(f'{repo}{name}.png')





# def box_values_BMu_llbp_hlbp(BMu_,outlier_present=True,save=True,name='boxplot',repo='',unit='',ini_only=True, group_muscle=True):
    
#     if not outlier_present:
#         outlier = BMu_remove_outlier_PC(BMu_)
#         BMu = BMu_.loc[~outlier]
#     else:
#         BMu = BMu_

#     BMu_['LBP_Group'] = BMu_['subject'].apply(lambda x: 'LLBP' if x in SUBJECT_LLBP else 'HLBP' if x in SUBJECT_HLBP else 'C' )
#     BMu_P = BMu.loc[BMu.P_C == 'P']

#     palette = {'LLBP':sns.color_palette("pastel").as_hex()[1],'HLBP':sns.color_palette().as_hex()[1],'C':sns.color_palette().as_hex()[0]}

#     BMu_C = BMu.loc[BMu.P_C == 'C']
#     fig, ax = plt.subplots(nrows=3, ncols=2,figsize = [9,7])
#     sns.histplot(BMu,x='value', hue='P_C',ax=ax[0,0],kde=True)
#     ax[0,0].set(xlabel=f'BMu values ({unit})', title = 'Histogram of BMu values')
#     sns.boxplot(data=BMu,x='value', y='LBP_Group', palette=palette,ax= ax[0,1],showfliers=outlier_present,showmeans=True,
#             meanprops={"marker": "+",
#                        "markeredgecolor": "black",
#                        "markersize": "10"})
#     ax[0,1].set(xlabel=f'BMu values ({unit})', title = 'Boxplot of BMu values')
   

#     sns.boxplot(data=BMu_P,x='value', y='channel',hue='LBP_Group',ax=ax[2,0],showfliers=outlier_present,showmeans=True,
#             meanprops={"marker": "+",
#                        "markeredgecolor": "black",
#                        "markersize": "10"})
#     ax[2,0].set(xlabel=f'BMu values ({unit})', title = 'Boxplot of BMu values Patient')
#     sns.boxplot(data=BMu_C,x='value', y='channel',ax=ax[2,1],showfliers=outlier_present,showmeans=True,
#             meanprops={"marker": "+",
#                        "markeredgecolor": "black",
#                        "markersize": "10"})
#     ax[2,1].set(xlabel=f'BMu values ({unit})', title = 'Boxplot of BMu values Control')

#     gs = ax[1, 0].get_gridspec()
#     ax[1,0].remove()
#     ax[1,1].remove()
#     axbig = fig.add_subplot(gs[1, 0:])
#     sns.boxplot(data=BMu,x='value', y='channel',hue ='LBP_Group', palette=palette,ax=axbig,showfliers=outlier_present,showmeans=True,
#             meanprops={"marker": "+",
#                        "markeredgecolor": "black",
#                        "markersize": "10"})
#     axbig.set(xlabel=f'BMu values ({unit})', title = 'Histogram of BMu values left/right sides for patients and controls')
#     sns.move_legend(axbig,'lower right')

#     fig.tight_layout()
    
#     if save :
#         fig.savefig(f'{repo}{name}.png')



def box_values_BMu_llbp_hlbp(BMu_,outlier_present=True,save=True,name='boxplot',repo='',unit='',factor=1.5):
    
    if not outlier_present:
        outlier = BMu_remove_outlier_PC(BMu_,factor=factor)
        BMu = BMu_.loc[~outlier]
    else:
        BMu = BMu_

    BMu_['LBP_Group'] = BMu_['subject'].apply(lambda x: 'LLBP' if x in SUBJECT_LLBP else 'HLBP' if x in SUBJECT_HLBP else 'C' )
    BMu_P = BMu.loc[BMu.P_C == 'P']
    BMu_P_llbp = BMu_P.loc[BMu_P.LBP_Group == 'LLBP']
    BMu_P_hlbp = BMu_P.loc[BMu_P.LBP_Group == 'HLBP']

    

    palette = {'LLBP':sns.color_palette("pastel").as_hex()[1],'HLBP':sns.color_palette().as_hex()[1],'C':sns.color_palette().as_hex()[0]}

    BMu_C = BMu.loc[BMu.P_C == 'C']
    fig, ax = plt.subplots(nrows=3, ncols=2,figsize = [9,7])

    sns.histplot(BMu,x='value', hue='P_C',ax=ax[0,0],kde=True)
    ax[0,0].set(xlabel=f'BMu values ({unit})', title = 'Histogram of BMu values')
    sns.boxplot(data=BMu,x='value', y='LBP_Group', palette=palette,ax= ax[0,1],showfliers=outlier_present,showmeans=True,
            meanprops={"marker": "+",
                       "markeredgecolor": "black",
                       "markersize": "10"})
    ax[0,1].set(xlabel=f'BMu values ({unit})', title = 'Boxplot of BMu values')
   

    sns.boxplot(data=BMu_P,x='value', y='channel',hue='LBP_Group',ax=ax[2,0],showfliers=outlier_present,showmeans=True,
            meanprops={"marker": "+",
                       "markeredgecolor": "black",
                       "markersize": "10"})
    ax[2,0].set(xlabel=f'BMu values ({unit})', title = 'Boxplot of BMu values Patient')
    sns.boxplot(data=BMu_C,x='value', y='channel',ax=ax[2,1],showfliers=outlier_present,showmeans=True,
            meanprops={"marker": "+",
                       "markeredgecolor": "black",
                       "markersize": "10"})
    ax[2,1].set(xlabel=f'BMu values ({unit})', title = 'Boxplot of BMu values Control')

    gs = ax[1, 0].get_gridspec()
    ax[1,0].remove()
    ax[1,1].remove()
    axbig = fig.add_subplot(gs[1, 0:])
    sns.boxplot(data=BMu,x='value', y='channel',hue ='LBP_Group', palette=palette,ax=axbig,showfliers=outlier_present,showmeans=True,
            meanprops={"marker": "+",
                       "markeredgecolor": "black",
                       "markersize": "10"})
    axbig.set(xlabel=f'BMu values ({unit})', title = 'Histogram of BMu values left/right sides for patients and controls')
    sns.move_legend(axbig,'lower right')

    fig.tight_layout()
    
    if save :
        fig.savefig(f'{repo}{name}.png')




def check_normal_distribution_P_C(BMu,outlier_present = False,factor=1.5,show=True):
    """Check if the values of each population (control and LBP) are normaly distributed with Shapiro Wilk test

    Args:
        BMu (dataframe):output of the computation of BMu for all patient
        outlier_present (bool, optional): _description_. Defaults to False.

    Returns:
        list of floats: output of the Shapiro Wilk test respectively for LBP patient and control
    """
    normality_P_C = []
    
    if not outlier_present:
        outlier = BMu_remove_outlier_PC(BMu,factor=factor)
        BMu_2 = BMu.loc[~outlier]
    else:
        BMu_2 = BMu

    for p in ['P','C']:
        BMu_PC = BMu_2.loc[BMu_2.P_C == p]
        p_val = shapiro(BMu_PC['value'].dropna()).pvalue
        if p_val <= 0.05:
            if show:
                print(f"The distribution of the values of {'CONTROL subject' if p == 'C' else 'LBP subject'} IS NOT normal")
            normality_P_C.append(False)
        else:
            if show:
                print(f"The distribution of the values of {'CONTROL subject' if p == 'C' else 'LBP subject'} IS normal")
            normality_P_C.append(True)

    return normality_P_C




# def mannwhitneyu_PC_RL(BMu_,outlier_present=True,show = False, RL = False,hypothesis ='two-sided'):
#     """ Mannwhitney U test between population and side

#     Args:
#         BMu_ (dataFrame): output of the computation of BMu for all patient
#         outlier_present (bool, optional): _description_. Defaults to False.
#         show (bool, optional): _description_. Defaults to False.
#         RL (bool, optional): _description_. Defaults to False.
#         alternative (dict, optional): {‘two-sided’, ‘less’, ‘greater’}

#     Returns:
#         floats : p value of each Mannwhitney U test
#     """
                                                                        
#     if not outlier_present:
#         outlier = BMu_remove_outlier_PC(BMu_)
#         BMu = BMu_.loc[~outlier]
#     else:
#         BMu = BMu_

#     BMu_R = BMu.loc[[True if u[0] == 'R' else False for u in BMu.channel]]
#     BMu_L = BMu.loc[[True if u[0] == 'L' else False for u in BMu.channel]]


#     _, pPC = mannwhitneyu(BMu.loc[BMu.P_C == 'P']['value'].dropna(), BMu.loc[BMu.P_C == 'C']['value'].dropna(),alternative=hypothesis)
#     _, pPCR = mannwhitneyu(BMu_R.loc[BMu_R.P_C == 'P']['value'].dropna(), BMu_R[BMu_R.P_C == 'C']['value'].dropna(),alternative=hypothesis)
#     _, pPCL = mannwhitneyu(BMu_L.loc[BMu_L.P_C == 'P']['value'].dropna(), BMu_L[BMu_L.P_C == 'C']['value'].dropna(),alternative=hypothesis)

#     if RL:
#         _, pRL = mannwhitneyu(BMu_R['value'].dropna(), BMu_L['value'].dropna(),alternative=hypothesis)
#         # alternative: Defines the alternative hypothesis. Default is ‘two-sided’. Let F(u) and G(u) be the cumulative distribution functions of the distributions underlying x and y, respectively. Then the following alternative hypotheses are available:
#                         #‘two-sided’: the distributions are not equal, i.e. F(u) ≠ G(u) for at least one u.
#         # p: The associated p-value for the chosen alternative.
#         _, pRLC = mannwhitneyu(BMu_R.loc[BMu_R.P_C == 'C']['value'].dropna(), BMu_L[BMu_L.P_C == 'C']['value'].dropna(),alternative=hypothesis)
#         _, pRLP = mannwhitneyu(BMu_R.loc[BMu_R.P_C == 'P']['value'].dropna(), BMu_L[BMu_L.P_C == 'P']['value'].dropna(),alternative=hypothesis)
    
#     else:
#         pRL,pRLC,pRLP = None,None,None

#     if show:
#         for p,t in zip([pPC,pPCR,pPCL,pRL,pRLC,pRLP,],['control and lbp','right side of control and lbp','left side of control and lbp','right and left of all','right and left of control','right and left of lbp']):
#             if p != None and p <= 0.05 :
#                 print(f"The difference between {t.upper()} patients IS significant, p = {p:0.4f} <= 0.05 ")
#             elif p != None and p > 0.05:
#                 print(f"The difference between {t.upper()} patients IS NOT significant, p = {p:0.4f} > 0.05 ")
#             else:
#                 pass

#     return pPC,pRL,pRLC,pRLP,pPCR,pPCL







# def comparison_PC_RL(BMu_,test,outlier_present=True,show = False, RL = False,group_muscle=True,hypothesis ='two-sided'):
#     """ Mannwhitney U or t test between population and side

#     Args:
#         BMu_ (dataFrame): output of the computation of BMu for all patient
#         outlier_present (bool, optional): _description_. Defaults to False.
#         show (bool, optional): _description_. Defaults to False.
#         RL (bool, optional): _description_. Defaults to False.
#         alternative (dict, optional): {‘two-sided’, ‘less’, ‘greater’}

#     Returns:
#         floats : p value of each Mannwhitney U test
#     """
#     if test == 'ttest':
#         func = ttest_ind
#     elif test == "mannwhitneyu":
#         func = mannwhitneyu
#     else:
#         raise Exception('unkown test')


#     if not outlier_present:
#         outlier = BMu_remove_outlier_PC(BMu_)
#         BMu = BMu_.loc[~outlier]

#         for i,row in BMu.iterrows():
#             sub = row['subject']
#             ses = row['session']
#             if (BMu.loc[(BMu['subject'] == sub) & (BMu['session'] == ses),:].shape[0] != 2 ) and  (int(BMu['BM'].values[0][3:]) not in [44,43,91,42,35,15]):
#                 try:
#                     BMu= BMu.drop(index = BMu.loc[(BMu['subject'] == sub) & (BMu['session'] == ses),:].index.values)

#                 except:
#                     pass

#     else:
#         BMu = BMu_

#     for i,row in BMu.iterrows():
#         sub = row['subject']
#         ses = row['session']
#         if (np.isnan(row['value'])) or (BMu.loc[(BMu['subject'] == sub) & (BMu['session'] == ses),:].shape[0]<2):
#             try:
#                 BMu= BMu.drop(index = BMu.loc[(BMu['subject'] == sub) & (BMu['session'] == ses),:].index.values)
#             except:
#                 pass

#     if group_muscle :
#         BMu_values_val = BMu.copy(deep = True)
#         BMu_values_val['id'] = BMu_values_val.apply(lambda x: f"{x['subject']}{x['session']}" ,axis=1)
#         val_grouped = BMu_values_val.groupby('id').mean()['value']
#         BMu_values_val =  BMu_values_val.loc[BMu_values_val['channel']==BMu_values_val['channel'].unique()[0],:]
#         BMu_values_val = BMu_values_val.merge(val_grouped,how='right',on='id').drop('value_x',axis=1).rename(columns={'value_y':'value'})
#     else:
#         BMu_values_val = BMu

#     if RL:
#         BMu_R = BMu.loc[[True if u[0] == 'R' else False for u in BMu.channel]]
#         BMu_L = BMu.loc[[True if u[0] == 'L' else False for u in BMu.channel]]

#     zpc, pPC = func(BMu_values_val.loc[BMu_values_val.P_C == 'P']['value'].dropna(), BMu_values_val.loc[BMu_values_val.P_C == 'C']['value'].dropna(),alternative=hypothesis)
    

#     if RL:
#         _, pPCR = func(BMu_R.loc[BMu_R.P_C == 'P']['value'].dropna(), BMu_R[BMu_R.P_C == 'C']['value'].dropna(),alternative=hypothesis)
#         _, pPCL = func(BMu_L.loc[BMu_L.P_C == 'P']['value'].dropna(), BMu_L[BMu_L.P_C == 'C']['value'].dropna(),alternative=hypothesis)

#     if RL:
#         func1 = wilcoxon
#         _, pRL = func1(BMu_R['value'].dropna(), BMu_L['value'].dropna(),alternative=hypothesis)
#         # alternative: Defines the alternative hypothesis. Default is ‘two-sided’. Let F(u) and G(u) be the cumulative distribution functions of the distributions underlying x and y, respectively. Then the following alternative hypotheses are available:
#                         #‘two-sided’: the distributions are not equal, i.e. F(u) ≠ G(u) for at least one u.
#         # p: The associated p-value for the chosen alternative.
#         _, pRLC = func1(BMu_R.loc[BMu_R.P_C == 'C']['value'].dropna(), BMu_L[BMu_L.P_C == 'C']['value'].dropna(),alternative=hypothesis)
#         _, pRLP = func1(BMu_R.loc[BMu_R.P_C == 'P']['value'].dropna(), BMu_L[BMu_L.P_C == 'P']['value'].dropna(),alternative=hypothesis)
    
#     else:
#         pRL,pRLC,pRLP,pPCL,pPCR = None,None,None,None,None

#     if test == 'ttest': 
#         d = (np.nanmean(BMu_values_val.loc[BMu_values_val.P_C == 'P']['value']) + np.nanmean(BMu_values_val.loc[BMu_values_val.P_C == 'C']['value']))/np.std(BMu_values_val['value'].values)
#     else:
#         d = abs(zpc)/np.sqrt(BMu_values_val.shape[0])
#     d_list = {0.2:'small effect',0.5:'moderate effect',0.8:'large effect'}

#     if show:
#         for p,t in zip([pPC,pPCR,pPCL,pRL,pRLC,pRLP,],['control and lbp','right side of control and lbp','left side of control and lbp','right and left of all','right and left of control','right and left of lbp']):
#             if p != None and p <= 0.05 :
#                 print(f"The difference between {t.upper()} patients IS significant, p = {p:0.4f} <= 0.05{f', Cohen s d = {d:0.2f}' if t == 'control and lbp' else ''  }")
#             elif p != None and p > 0.05:
#                 print(f"The difference between {t.upper()} patients IS NOT significant, p = {p:0.4f} > 0.05{f', Cohen s d = {d:0.2f}' if t == 'control and lbp' else ''  } ")
#             else:
#                 pass

#     return pPC,pRL,pRLC,pRLP,pPCR,pPCL,d






# def comparison_PC_RL_HLLBP_0(BMu_,test,outlier_present=True,factor=1.5,show = False,text=False, RL = False,hypothesis ='two-sided'):
#     """Mannwhitney U test et T test

#     Args:
#         BMu_ (_type_): results of the computatiom of the BMu
#         test (_type_): statistical test to perform
#         outlier_present (bool, optional): True = outliers are present
#         show (bool, optional): True = print the text
#         text (bool, optional): True = return the text
#         RL (bool, optional): True = Comparison for each side and between sides ( BUT it uses a t test pairwise test even thought the samples are propably not normally distributed)
#         group_muscle (bool, optional): right and left are averaged , default to False
#         hypothesis (str, optional): _description_. Defaults to 'two-sided'.

#     Raises:
#         Exception: _description_

#     Returns:
#         _type_: _description_
#     """
#     dict_sig = {}
#     if test == 'ttest':
#         func = ttest_ind
#     elif test == "mannwhitneyu":
#         func = mannwhitneyu
#     else:
#         raise Exception('unkown test')

#     BMu_2 = BMu_.copy(deep=True)
#     if not outlier_present:
#         outlier = BMu_remove_outlier_PC(BMu_2,factor=factor)
#         BMu = BMu_2.loc[~outlier]
#     else:
#         BMu = BMu_2

#     BMu['LBP_Group'] = BMu['subject'].apply(lambda x: 'LLBP' if x in SUBJECT_LLBP else 'HLBP' if x in SUBJECT_HLBP else 'C' )
  

#     #average right and left channel
#     BMu_values_val = BMu.copy(deep = True)
#     BMu_values_val['id'] = BMu_values_val.apply(lambda x: f"{x['subject']}{x['session']}" ,axis=1)
#     val_grouped = BMu_values_val.groupby('id').mean()['value']
#     BMu_values_val =  BMu_values_val.loc[BMu_values_val['channel']==BMu_values_val['channel'].unique()[0],:]
#     BMu_values_val = BMu_values_val.merge(val_grouped,how='right',on='id').drop('value_x',axis=1).rename(columns={'value_y':'value'})


#     BMu_LLBP = BMu_values_val.loc[BMu_values_val['LBP_Group']=='LLBP',:]['value'].dropna()
#     BMu_HLBP = BMu_values_val.loc[BMu_values_val['LBP_Group']=='HLBP',:]['value'].dropna()
#     BMu_C =  BMu_values_val.loc[BMu_values_val.P_C == 'C']['value'].dropna()
#     BMu_P =  BMu_values_val.loc[BMu_values_val.P_C == 'P']['value'].dropna()

#     # computation of each side
#     if RL:

#         for s in ['R','L']:

#             BMu_R = BMu.loc[[True if u[0] == s else False for u in BMu.channel],:]
#             BMu_RPl = BMu_R.loc[BMu_R['LBP_Group']=='LLBP']['value'].dropna()
#             BMu_RPh = BMu_R.loc[BMu_R['LBP_Group']=='HLBP']['value'].dropna()
#             BMu_RP = BMu_R.loc[BMu_R['P_C']=='P']['value'].dropna()
#             BMu_RC = BMu_R.loc[BMu_R['P_C']=='C']['value'].dropna()

#             dict_sig[('LBP','C',s)] = func(BMu_RP, BMu_RC,alternative=hypothesis)[1]
#             dict_sig[('LLBP','C',s)] = func(BMu_RPl, BMu_RC,alternative=hypothesis)[1]
#             dict_sig[('HLBP','C',s)] = func(BMu_RPh, BMu_RC,alternative=hypothesis)[1]
#             dict_sig[('HLBP','LLBP',s)] = func(BMu_RPl, BMu_RPh,alternative=hypothesis)[1]

#         BMu_cop = BMu.copy(deep=True)
#         for s in BMu_cop['subject'].unique():
#             if BMu_cop.loc[BMu_cop['subject'] == s,:].shape[0] <2:
#                 # then one side have been excluded, the other is removed
#                 BMu_cop = BMu_cop.drop(BMu_cop.loc[BMu_cop['subject'] == s,:].index)


#         BMu_R = BMu_cop.loc[[True if u[0] == 'R' else False for u in BMu_cop.channel]]
#         BMu_L = BMu_cop.loc[[True if u[0] == 'L' else False for u in BMu_cop.channel]]
#         BMu_RP = BMu_R.loc[BMu_R['P_C']=='P']['value'].dropna()
#         BMu_LP = BMu_L.loc[BMu_L['P_C']=='P']['value'].dropna()
#         BMu_RC = BMu_R.loc[BMu_R['P_C']=='C']['value'].dropna()
#         BMu_LC = BMu_L.loc[BMu_L['P_C']=='C']['value'].dropna()

#         dict_sig[('R','L','PC')] = ttest_rel(BMu_R['value'].dropna(), BMu_L['value'].dropna(),alternative=hypothesis)[1]
#         dict_sig[('R','L','P')] = ttest_rel(BMu_RP, BMu_LP,alternative=hypothesis)[1]
#         dict_sig[('R','L','C')] = ttest_rel(BMu_RC, BMu_LC,alternative=hypothesis)[1]

#     else:
#         dict_sig[('R','L','PC')] = None
#         dict_sig[('R','L','P')] = None
#         dict_sig[('R','L','C')] = None

#         for s in ['R','L']:
#             dict_sig[('LBP','C',s)] = None
#             dict_sig[('LLBP','C',s)] = None
#             dict_sig[('HLBP','C',s)] = None
#             dict_sig[('HLBP','LLBP',s)] = None

#     # computation with averaged channels
#     dict_sig[('LBP','C','RL')] = func(BMu_P, BMu_C,alternative=hypothesis)[1]
#     dict_sig[('LLBP','C','RL')] = func(BMu_LLBP,BMu_C,alternative=hypothesis)[1]
#     dict_sig[('HLBP','C','RL')] = func(BMu_HLBP,BMu_C,alternative=hypothesis)[1]
#     dict_sig[('HLBP','LLBP','RL')] =  func(BMu_HLBP,BMu_LLBP,alternative=hypothesis)[1]


#     if show:
#         for g,p in dict_sig.items():
#             t = f'{g[0]} and {g[1]} for {g[2]}'
#             if p != None and p <= 0.05 :
#                 print(f"The difference between {t} patients IS significant, p = {p:0.4f} <= 0.05")
#             elif p != None and p > 0.05:
#                 print(f"The difference between {t} patients IS NOT significant, p = {p:0.4f} > 0.05")
#             else:
#                 pass
    
#     if text:
#         text_dict =''
#         i= 1
#         for g,p in dict_sig.items():
#             t = f'{g[0]} and {g[1]} for {g[2]}'
#             if p != None and p <= 0.05 :
#                 text_dict +=f"<br>The difference between {t} patients IS significant, p = {p:0.4f} <= 0.05"
#             elif p != None and p > 0.05:
#                  text_dict +=f"<br>The difference between {t} patients IS NOT significant, p = {p:0.4f} > 0.05"
#             i+= 1
#             if i in [5,9,12]:
#                 text_dict += '<br>'
#             else:
#                 pass

#     if text:
#         return text_dict

#     return dict_sig






def normality_function(BM1,BM2):

    p_val1 = shapiro(BM1).pvalue
    p_val2 = shapiro(BM2).pvalue

    p_val = np.array([p_val1,p_val2])
    p_norm = p_val<= 0.05
    if False in p_norm:
        return ttest_ind
    else:
        return mannwhitneyu



def comparison_PC_RL_HLLBP(BMu_,test,outlier_present=True,factor=1.5,show = False,text=False, RL = False,hypothesis ='two-sided'):
    """Mannwhitney U test et T test

    Args:
        BMu_ (_type_): results of the computatiom of the BMu
        test (_type_): statistical test to perform
        outlier_present (bool, optional): True = outliers are present
        show (bool, optional): True = print the text
        text (bool, optional): True = return the text
        RL (bool, optional): True = Comparison for each side and between sides ( BUT it uses a t test pairwise test even thought the samples are propably not normally distributed)
        group_muscle (bool, optional): right and left are averaged , default to False
        hypothesis (str, optional): _description_. Defaults to 'two-sided'.

    Raises:
        Exception: _description_

    Returns:
        _type_: _description_
    """
    dict_sig = {}
    if test == 'ttest':
        func = ttest_ind
    elif test == "mannwhitneyu":
        func = mannwhitneyu
    else:
        raise Exception('unkown test')

    BMu_2 = BMu_.copy(deep=True)
    if not outlier_present:
        outlier = BMu_remove_outlier_PC(BMu_2,factor=factor)
        BMu = BMu_2.loc[~outlier]
    else:
        BMu = BMu_2

    BMu['LBP_Group'] = BMu['subject'].apply(lambda x: 'LLBP' if x in SUBJECT_LLBP else 'HLBP' if x in SUBJECT_HLBP else 'C' )
  

   

    # computation of each side
    if RL:

        for s in ['R','L']:

            BMu_R = BMu.loc[[True if u[0] == s else False for u in BMu.channel],:]
            BMu_RPl = BMu_R.loc[BMu_R['LBP_Group']=='LLBP']['value'].dropna()
            BMu_RPh = BMu_R.loc[BMu_R['LBP_Group']=='HLBP']['value'].dropna()
            BMu_RP = BMu_R.loc[BMu_R['P_C']=='P']['value'].dropna()
            BMu_RC = BMu_R.loc[BMu_R['P_C']=='C']['value'].dropna()

            dict_sig[('LBP','C',s)] = normality_function(BMu_RP,BMu_RC)(BMu_RP, BMu_RC,alternative=hypothesis)[1]
            dict_sig[('LLBP','C',s)] = normality_function(BMu_RPl,BMu_RC)(BMu_RPl, BMu_RC,alternative=hypothesis)[1]
            dict_sig[('HLBP','C',s)] = normality_function(BMu_RPh,BMu_RC)(BMu_RPh, BMu_RC,alternative=hypothesis)[1]
            dict_sig[('HLBP','LLBP',s)] = normality_function(BMu_RPl,BMu_RPh)(BMu_RPl, BMu_RPh,alternative=hypothesis)[1]

        BMu_cop = BMu.copy(deep=True)
        BMu_cop = BMu_cop.dropna()
        for s in BMu_cop['subject'].unique():
            if BMu_cop.loc[BMu_cop['subject'] == s,:].shape[0] <2:
                # then one side have been excluded, the other is removed
                BMu_cop = BMu_cop.drop(BMu_cop.loc[BMu_cop['subject'] == s,:].index)


        BMu_R = BMu_cop.loc[[True if u[0] == 'R' else False for u in BMu_cop.channel]]
        BMu_L = BMu_cop.loc[[True if u[0] == 'L' else False for u in BMu_cop.channel]]
        BMu_RP = BMu_R.loc[BMu_R['P_C']=='P']['value']
        BMu_LP = BMu_L.loc[BMu_L['P_C']=='P']['value']
        BMu_RC = BMu_R.loc[BMu_R['P_C']=='C']['value']
        BMu_LC = BMu_L.loc[BMu_L['P_C']=='C']['value']


        

        dict_sig[('R','L','PC')] = ttest_rel(BMu_R['value'], BMu_L['value'],alternative=hypothesis)[1]
        dict_sig[('R','L','P')] = ttest_rel(BMu_RP, BMu_LP,alternative=hypothesis)[1]
        dict_sig[('R','L','C')] = ttest_rel(BMu_RC, BMu_LC,alternative=hypothesis)[1]

    else:
        dict_sig[('R','L','PC')] = None
        dict_sig[('R','L','P')] = None
        dict_sig[('R','L','C')] = None

        for s in ['R','L']:
            dict_sig[('LBP','C',s)] = None
            dict_sig[('LLBP','C',s)] = None
            dict_sig[('HLBP','C',s)] = None
            dict_sig[('HLBP','LLBP',s)] = None
    # computation with averaged channels


     #average right and left channel
    BMu_values_val = BMu.copy(deep = True)
    BMu_values_val['id'] = BMu_values_val.apply(lambda x: f"{x['subject']}{x['session']}" ,axis=1)
    val_grouped = BMu_values_val.groupby('id').mean()['value']
    BMu_values_val =  BMu_values_val.loc[BMu_values_val['channel']==BMu_values_val['channel'].unique()[0],:]
    BMu_values_val = BMu_values_val.merge(val_grouped,how='right',on='id').drop('value_x',axis=1).rename(columns={'value_y':'value'})


    BMu_LLBP = BMu_values_val.loc[BMu_values_val['LBP_Group']=='LLBP',:]['value'].dropna()
    BMu_HLBP = BMu_values_val.loc[BMu_values_val['LBP_Group']=='HLBP',:]['value'].dropna()
    BMu_C =  BMu_values_val.loc[BMu_values_val.P_C == 'C']['value'].dropna()
    BMu_P =  BMu_values_val.loc[BMu_values_val.P_C == 'P']['value'].dropna()


    dict_sig[('LBP','C','RL')] = normality_function(BMu_P, BMu_C)(BMu_P, BMu_C,alternative=hypothesis)[1]
    dict_sig[('LLBP','C','RL')] = normality_function(BMu_LLBP,BMu_C)(BMu_LLBP,BMu_C,alternative=hypothesis)[1]
    dict_sig[('HLBP','C','RL')] = normality_function(BMu_HLBP,BMu_C)(BMu_HLBP,BMu_C,alternative=hypothesis)[1]
    dict_sig[('HLBP','LLBP','RL')] =  normality_function(BMu_HLBP,BMu_LLBP)(BMu_HLBP,BMu_LLBP,alternative=hypothesis)[1]


    if show:
        for g,p in dict_sig.items():
            t = f'{g[0]} and {g[1]} for {g[2]}'
            if p != None and p <= 0.05 :
                print(f"The difference between {t} patients IS significant, p = {p:0.4f} <= 0.05")
            elif p != None and p > 0.05:
                print(f"The difference between {t} patients IS NOT significant, p = {p:0.4f} > 0.05")
            else:
                pass
    
    if text:
        text_dict =''
        i= 1
        for g,p in dict_sig.items():
            t = f'{g[0]} and {g[1]} for {g[2]}'
            if p != None and p <= 0.05 :
                text_dict +=f"<br>The difference between <span style= 'background-color: #E0F0D1'>{t} patients IS significant</span>, p = {p:0.4f} <= 0.05"
            elif p != None and p > 0.05:
                 text_dict +=f"<br>The difference between {t} patients IS NOT significant, p = {p:0.4f} > 0.05"
            i+= 1
            if i in [5,9,12]:
                text_dict += '<br>'
            else:
                pass

    if text:
        return text_dict

    return dict_sig






# Notebook analysis all------------------------------------------------------------------------------------------------------------




def get_last_nb(BMu_nb,desc): 
    """
    Returns:
        int: the number of the last computation of the BMu
    """

    file_name = f'BMu{BMu_nb}{desc}_*'
    file = glob('results\\' + file_name + '_values' + '.csv')
    return np.max([int(f.split('_')[-2]) for f in file])



def load_results_presentation(BMu_nb,trial = None,author=''):
    """ 
    Returns:
        retrives the tables of results of the last computation of the BMu
    """

    nb = get_last_nb(BMu_nb,author)
    nb_BM = BMu_nb
    if trial == None:
        all_files_value = glob(f'results\\BMu{str(nb_BM)}{author}_{nb}*.csv')
        all_files_detailed = glob(f'results\\detailed\\BMu{str(nb_BM)}{author}_{nb}*.csv')
        all_files_error = glob(f'results\\error\\BMu{str(nb_BM)}{author}_{nb}*.csv')
    else:
        all_files_value = glob(f'results\\BMu{str(nb_BM)}{author}_{trial}*.csv')
        all_files_detailed = glob(f'results\\detailed\\BMu{str(nb_BM)}{author}_{trial}*.csv')
        all_files_error = glob(f'results\\error\\BMu{str(nb_BM)}{author}_{trial}*.csv')


    BMu_detailed_csv,BMu_error_csv, BMu_values_csv = all_files_detailed[-1],all_files_error[-1],all_files_value[-1]

    BMu_values = pd.read_csv( BMu_values_csv )
    BMu_detailed = pd.read_csv( BMu_detailed_csv )
    BMu_error = pd.read_csv( BMu_error_csv )
    return BMu_values,BMu_detailed,BMu_error

        



def comparison_PC_input_val(BMu_,test,outlier_present=True,show = False,hypothesis ='two-sided'):
    """ Mannwhitney U or t test between population and side

    Args:
        BMu_ (dataFrame): output of the computation of BMu for all patient
        outlier_present (bool, optional): _description_. Defaults to False.
        show (bool, optional): _description_. Defaults to False.
        RL (bool, optional): _description_. Defaults to False.
        alternative (dict, optional): {‘two-sided’, ‘less’, ‘greater’}

    Returns:
        floats : p value of each Mannwhitney U test
    """
    if test == 'ttest':
        func = ttest_ind
    elif test == "mannwhitneyu":
        func = mannwhitneyu
    else:
        raise Exception('unkown test')


    if not outlier_present:
        outlier = BMu_remove_outlier_PC(BMu_)
        BMu = BMu_.loc[~outlier]
    else:
        BMu = BMu_


    _, pPC = func(BMu.loc[BMu.Group == 'PAT']['Value'].dropna(), BMu.loc[BMu.Group == 'CTR']['Value'].dropna(),alternative=hypothesis)
    
    

    if show:
        for p,t in zip([pPC],['control and lbp']):
            if p != None and p <= 0.05 :
                print(f"The difference between {t.upper()} patients IS significant, p = {p:0.4f} <= 0.05 ")
            elif p != None and p > 0.05:
                print(f"The difference between {t.upper()} patients IS NOT significant, p = {p:0.4f} > 0.05 ")
            else:
                pass

    return pPC







def check_normal_distribution_input_val(BMu,outlier_present = False,show=True):
    """Check if the values of each population (control and LBP) are normaly distributed with Shapiro Wilk test

    Args:
        BMu (dataframe):output of the computation of BMu for all patient
        outlier_present (bool, optional): _description_. Defaults to False.

    Returns:
        list of floats: output of the Shapiro Wilk test respectively for LBP patient and control
    """
    normality_P_C = []
    
    if not outlier_present:
        outlier = BMu_remove_outlier_PC(BMu)
        BMu_2 = BMu.loc[~outlier]
    else:
        BMu_2 = BMu

    for p in ['PAT','CTR']:
        BMu_PC = BMu_2.loc[BMu_2.Group == p]
        p_val = shapiro(BMu_PC['Value'].dropna()).pvalue
        if p_val <= 0.05:
            if show:
                print(f"The distribution of the values of {'CONTROL subject' if p == 'C' else 'LBP subject'} IS NOT normal")
            normality_P_C.append(False)
        else:
            if show:
                print(f"The distribution of the values of {'CONTROL subject' if p == 'C' else 'LBP subject'} IS normal")
            normality_P_C.append(True)

    return normality_P_C








from functions.Biomarker_dict_final import BMu_dict_final

def get_all_outliers(factor = 1.5,exclude=[]):# with final dictionnary of BMu 

    df_BMu_outlier = pd.DataFrame(data=None, columns=['BM','subject','P_C','session','task','channel','value'])

    error = []

    #for each biomarker
    for BMu_name,val in BMu_dict_final.items():
        BMu_nb = BMu_name[3:]
        try:
            desc = val[-1]['desc']
        except:
            desc= ''

        # load the file
        try:
            
            BMu_values,_,_  = load_results_presentation(BMu_nb,author=desc)
            _,_,_,task, _,_,_,_,_,_ = BMu_dict['BMu'+str(BMu_nb)]

            if len(exclude) != 0:
                df_val = pd.read_csv("C:\\Users\\Anais\\Studio Code\\Project 1\\support doc\\all_signals_param.csv")
                th_mean = exclude[0]
                th_max = exclude[1]
                th_mean_freq = exclude[2]
                df_excluded = df_val.loc[(df_val['max']>=th_max) & (df_val['mean']>=th_mean) & (df_val['mean freq']>=th_mean_freq),:]
                df_excluded = df_excluded.loc[df_excluded['task']==task,:]
                df_excluded = df_excluded.loc[df_excluded['session']=='INI',:]
            

                for i,row in df_excluded.iterrows():
                    sub_ex = row['subject']
                    ses_ex = row['session']
                    rep_ex = row['trial']
                    channel_ex = row['channel']
                    try:
                        BMu_values = BMu_values.drop(BMu_values.loc[ (BMu_values['subject']==sub_ex) & (BMu_values['session']==ses_ex)  & (BMu_values['repetition']==rep_ex) & (BMu_values['channel']==channel_ex),:].index)
                    except:
                        continue

                
            if task == 'Gait_Normal':
                
                # Grouping columns and applying agg function
                group = BMu_values.groupby(['BM','subject','P_C','session','task','channel'], as_index=False)
                BMu_values = group.agg({'value':np.mean})

                # Joining res column and values
                BMu_values.columns = list(map(''.join, BMu_values.columns.values))
                BMu_values['repetition'] = 1

            else:
                pass
            BMu_outlier = BMu_values[BMu_remove_outlier_PC(BMu_values,factor=factor)]
            df_BMu_outlier = pd.concat([df_BMu_outlier,BMu_outlier],axis=0,ignore_index=True)

            

        except Exception as e:
            print(BMu_nb,e)
            error += [[BMu_nb,e]]

    return df_BMu_outlier





def get_all_values(): # with the final dictionnary of BMus

    df_BMu_all_values = pd.DataFrame(data=None, columns=['BM','subject','P_C','session','task','channel','value'])

    error = []

    for BMu_name,val in BMu_dict_final.items():

        BMu_nb = BMu_name[3:]
        try:
            desc = val[-1]['desc']
        except:
            desc= ''

        # load the file
        try:

            BMu_values,_,_  = load_results_presentation(BMu_nb,author=desc)
            _,_,_,task, _,_,_,_,_,_ = BMu_dict['BMu'+str(BMu_nb)]

            if task == 'Gait_Normal':
                
                # Grouping columns and applying agg function
                group = BMu_values.groupby(['BM','subject','P_C','session','task','channel'], as_index=False)
                BMu_values = group.agg({'value':np.mean})

                # Joining res column and values
                BMu_values.columns = list(map(''.join, BMu_values.columns.values))


            df_BMu_all_values = pd.concat([df_BMu_all_values,BMu_values],axis=0,ignore_index=True)

        except Exception as e:
            error += [[BMu_nb,e]]
            print(BMu_nb,e)

    return df_BMu_all_values





def get_all_values_and_outliers(factor = 1.5,exclude=[]): # with the final dictionnary of BMu (makes get_all_values() and get_all_outliers() obsolete)

    df_BMu_all_values = pd.DataFrame(data=None, columns=['BM','subject','P_C','session','task','channel','value','outlier'])

    error = [[]]
    dict_verif_ex = {}

    for BMu_name,val in BMu_dict_final.items():

        BMu_nb = BMu_name[3:]
        try:
            desc = val[-1]['desc']
        except:
            desc= ''

        # load the file
        try:

            BMu_values,_,_  = load_results_presentation(BMu_nb,author=desc)
            _,_,_,task, _,_,_,_,_,_ = BMu_dict['BMu'+str(BMu_nb)]


            if len(exclude) != 0:
                df_val = pd.read_csv("C:\\Users\\Anais\\Studio Code\\Project 1\\support doc\\all_signals_param.csv")
                th_mean = exclude[0]
                th_max = exclude[1]
                th_mean_freq = exclude[2]
                df_excluded = df_val.loc[(df_val['max']>=th_max) & (df_val['mean']>=th_mean) & (df_val['mean freq']>=th_mean_freq),:]
                df_excluded = df_excluded.loc[df_excluded['task']==task,:]
                df_excluded = df_excluded.loc[df_excluded['session']=='INI',:]
            
                dict_verif_ex[BMu_nb] = []
                dict_verif_ex[BMu_nb]+=[[BMu_values.shape]]
                print(BMu_nb,BMu_values.shape[0])
                for i,row in df_excluded.iterrows():
                    sub_ex = row['subject']
                    ses_ex = row['session']
                    rep_ex = row['trial']
                    channel_ex = row['channel']
                    try:
                        BMu_values = BMu_values.drop(BMu_values.loc[ (BMu_values['subject']==sub_ex) & (BMu_values['session']==ses_ex)  & (BMu_values['repetition']==rep_ex) & (BMu_values['channel']==channel_ex),:].index)
                        dict_verif_ex[BMu_nb] += [[sub_ex,ses_ex,rep_ex,channel_ex]]
                    except:
                        continue
                dict_verif_ex[BMu_nb]+=[[BMu_values.shape]]
                print(BMu_nb,BMu_values.shape[0])



            if task == 'Gait_Normal':
                
                # Grouping columns and applying agg function
                group = BMu_values.groupby(['BM','subject','P_C','session','task','channel'], as_index=False)
                BMu_values = group.agg({'value':np.mean})

                # Joining res column and values
                BMu_values.columns = list(map(''.join, BMu_values.columns.values))

            BMu_values['outlier'] = BMu_remove_outlier_PC(BMu_values,factor=factor)
            df_BMu_all_values = pd.concat([df_BMu_all_values,BMu_values],axis=0,ignore_index=True)

        except Exception as e:
            error += [[BMu_nb,e]]
            print(BMu_nb,e)

    return df_BMu_all_values,dict_verif_ex







def get_count_task_channel(df_all_values):
    tasks_count = {}
    for unique_task in df_all_values['task'].unique():
        tasks_count[unique_task] = df_all_values.loc[df_all_values['task'] == unique_task].shape[0]

    channel_count = {}
    for unique_channel in df_all_values['channel'].unique():
        channel_count[unique_channel] = df_all_values.loc[df_all_values['channel'] == unique_channel].shape[0]

    return tasks_count, channel_count





def get_percent_outliers(df_all_values,df_outliers):
    tasks_count, channel_count = get_count_task_channel(df_all_values)
    task_outliers_count, channel_outliers_count = {},{}
    
    for unique_task in df_outliers['task'].unique():
        task_outliers_count[unique_task] = df_outliers.loc[df_outliers['task'] == unique_task].shape[0]


    for unique_channel in df_outliers['channel'].unique():
        channel_outliers_count[unique_channel] = df_outliers.loc[df_outliers['channel'] == unique_channel].shape[0]

    def transform_percent(dict_out, dict_ref):
        for k,v in dict_out.items():
            dict_out[k] = dict_out[k]/dict_ref[k]*100
        return dict_out
    
    task_outliers_count = transform_percent(task_outliers_count,tasks_count)
    channel_outliers_count = transform_percent(channel_outliers_count,channel_count)

    return task_outliers_count, channel_outliers_count





def plot_percent_task_channel_outliers(df_all_values,df_outliers):
    task_outliers_percent, channel_outliers_percent = get_percent_outliers(df_all_values,df_outliers)
    fig,ax = plt.subplots(1,2,figsize = (15,4))
    hist1 = sns.barplot(pd.DataFrame(task_outliers_percent,index=[0]),ax=ax[0],color = sns.color_palette('muted').as_hex()[0])
    hist2 = sns.barplot(pd.DataFrame(channel_outliers_percent,index=[0]),ax=ax[1],color = sns.color_palette('muted').as_hex()[0])

    for a,h in zip(ax,[hist1,hist2]):
        a.set_xticklabels(a.get_xticklabels(), rotation=30,horizontalalignment='right')
        a.set(ylabel='percent')
    hist_value(hist1,ax[0],pd.DataFrame(task_outliers_percent,index=[0]),shift=1,count = False,h_max=24)
    hist_value(hist2,ax[1],pd.DataFrame(channel_outliers_percent,index=[0]),shift=1,count = False,h_max=24)

    fig.suptitle('Percentage of outliers per task and per muscle')
    fig.tight_layout()




