from functions.requirements import *


# C3D FUNCTION--------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------------------

#create an empty structure
def create_empty_c3d():
    c = c3d()
    print(c['parameters']['POINT']['USED']['value'][0]);


#read C3D
def read_c3d(path):
    """
    https://github.com/pyomeca/ezc3d#python-3

    :param path: string, path to the c3d files, eg: 'path_to_c3d.c3d'
    :return: c,point_data,points_residuals,analog_data
        point_data :  the shape of point_data is 4xNxT, where 4 represent the components XYZ1 (the 3D coordinates of
        the point add with a 1 so it can be used with homogeneous matrices), N is the number of points and T is the
        number of frames
        analog_data : the shape of analog_data are 1xNxT, where 1 is the value, N is the number of analogous data and
        T  is the number of frames.
    """

    c = c3d(path)
    # print(f"Number of point {c['parameters']['POINT']['USED']['value'][0]}");  # Print the number of points used
    point_data = c['data']['points']
    points_residuals = c['data']['meta_points']['residuals']
    analog_data = c['data']['analogs']

    return c,point_data,points_residuals,analog_data






# CHECK IF THE REPOSITORY EXIST---------------------------------------------------------------------------------
# see EDA Notebook for more function to assess missing files and handle them

def check_ini_rel_repo(subject):
    
    ini_rel = [True,False]
    for c in ini_rel:
        path = PATH_REPO + f"NSLBP-BIO-0{subject:02d}" + "\\* - "+ f"{'INI' if c else 'REL'}_session"
    
        file = glob(path)
        if len(file) == 0:
            print(f"{'INI' if c else 'REL' }, subject {subject}: not found")
        elif len(file) > 1:
            print(f"{'INI' if c else 'REL' }, subject {subject}: mutiple files found")






# GET C3D FILE --------------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------------------

def get_file_subject_c3d(subject, ini_rel = 'INI', path_repo = PATH_REPO, name = '') : 
    """get the liste of c3d files corresponding to the subject and the session

    Args:
        subject (int): 
        ini_rel (str, optional): 'INI' or 'REL'
        path_repo (): Defaults to PATH_REPO.

    Returns:
        _type_: list of paths
    """
    
    assert subject in SUBJECT
    assert ini_rel in ['INI','REL']
    if name == '':
        path = path_repo + f"NSLBP-BIO-0{subject:02d}" + "\\* - "+ f"{ini_rel}_session\\*.c3d"
    else:
        path = path_repo + f"NSLBP-BIO-0{subject:02d}" + "\\* - "+ f"{ini_rel}_session\\{name}*.c3d"

    file = glob(path)

    return file







# C3D TO PYTHON STRUCTURES --------------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------------------

def get_data_c3d(subject, name,ini_rel = 'INI', path_repo = PATH_REPO):

    file = get_file_subject_c3d(subject, ini_rel = ini_rel, path_repo = path_repo, name = name)
    c = c3d(file[0])
    return c






def extract_channels_c3d(c,analog_data, file_path = '', save = False, replace = False):
    """ extract each emg signal from the data, save the resulting dataframe if wanted

    Args:
        c (ndaray): data points extracted from c3d file
        analog_d (ndarray): analog data extracted from c3d file
        file_path (str, optional): file path to save the data to csv. Defaults to ''.
        save (bool, optional): save the extracted data. Defaults to False.
        replace (bool, optional): replace the already existing file. Defaults to False.
        comment (bool, optional): output the head of the saved data frame. Defaults to False.

    Returns:
        Dataframe: each column corresponds to a channel
    """

    data_dict = {}
    i = 0
    for ch in c['parameters']['ANALOG']['LABELS']['value']:
        if ch in CHANNEL:
            
            if len(analog_data[0,i,:]) == 0:
                print(f'emg empty for {ch}')
            data_dict[ch] = analog_data[0,i,:]
            i += 1

    if sorted(list(data_dict.keys())) != CHANNEL:
        print(f'missing emg {[e for e in list(data_dict.keys()) if e not in CHANNEL]}')


    if save:
        assert file_path != '', 'empty file path'

        if file_path[-4:] == '.csv':
            save_csv =  True
        elif file_path[-4:] == '.json':
            save_json = True

        else:
            raise Exception("Unknown file format")

        if (exists(file_path)):
            print(f' the file already exists {file_path}')
            if replace:
                df = pd.DataFrame(data_dict)

                if save_csv:
                    df.to_csv(file_path,index=False)
                elif save_json:
                    df.to_json(file_path,orient='index')
                print(f' saved again to {file_path}') 

        else:
            df = pd.DataFrame(data_dict)
            if save_csv:
                df.to_csv(file_path,index=False)
            elif save_json:
                df.to_json(file_path,orient='index')

            print(f' saved to {file_path}') 

        
    return pd.DataFrame(data_dict)









# C3D TO CSV --------------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------------------

# EMG 

def save_csv_subject(subject, ask_for_next = False, note = True):
    """saved all c3d files of a patient into csv while reseting the number of the file

    Args:
        subject (_type_): 
        ask_for_next (bool, optional): ask for confirmation before extracting the next movement. Defaults to True.

    Returns: 
        list: list of detected error for the subject [subject, session, task, type of error, Number of file]
        
    """
    assert subject in SUBJECT
    number_per_file = {'Gait_Fast': 5, 'Gait_Normal':10, 'Gait_Slow': 2}
    output = []
    

    for ini_rel in ['INI','REL']:
        #get all c3d file corresponding to the subject and the session
        all_file = get_file_subject_c3d(subject, ini_rel)

        for mvt in TASKS_NAME_2 :
            err = False

            if ask_for_next:
                print(f"patient {subject}, session {ini_rel}, movement {mvt}")

            # find the file(s) corresponding to the movement
            pattern = f"*{mvt}*.c3d"
            file_all_mvt = fnmatch.filter(all_file,pattern)
            nb = len(file_all_mvt)


            # check the number of files
            # if a file has more than the indicated number of files ask wich files should be kept 
            # if the movement is not in the dictionnary it should have one file
            if mvt not in list(number_per_file.keys()) and nb == 0 :
                print(f"There is no file for subject {subject} {ini_rel} {mvt}")
                output += [[subject,ini_rel,mvt,'missing',nb]]     
                err = True

                if note and err:
                    ans_noted = ' '
                    while ans_noted  != 'y' :
                        ans_noted = input(f"¨!! {output[-1][-2]} files for {subject} {ini_rel} {mvt}. [y]")

                continue

            elif mvt not in list(number_per_file.keys()) and nb > 1 :
                print(f"There is too many files  ( {nb} files )  for subject {subject} {ini_rel} {mvt}")
                output += [[subject,ini_rel,mvt,'too many',nb]]
                ans = np.arange(1,nb+1)
                err = True

            elif mvt in list(number_per_file.keys()) and nb == 0:
                print(f"There is no files for subject {subject} {ini_rel} {mvt}")
                output += [[subject,ini_rel,mvt,'missing',nb]]
                err = True

                if note and err:
                    ans_noted = ' '
                    while ans_noted  != 'y' :
                        ans_noted = input(f"¨!! {output[-1][-2]} files for {subject} {ini_rel} {mvt}. [y]")

                continue

            elif mvt in list(number_per_file.keys()) and nb > number_per_file[mvt]:
                print(f"There is too many files ( {nb} files ) for subject {subject} {ini_rel} {mvt}")
                output += [[subject,ini_rel,mvt,'too many',nb]]
                ans = np.arange(1,nb+1)
                err = True

            elif mvt in list(number_per_file.keys()) and nb < number_per_file[mvt]:
                print(f"There is some missing files ( {nb} files ) for subject {subject} {ini_rel} {mvt}")
                output += [[subject,ini_rel,mvt,'missing',nb]]
                ans = np.arange(1,nb+1)
                err = True
            
            elif mvt in list(number_per_file.keys()) and nb == number_per_file[mvt]:
                ans = np.arange(1,number_per_file[mvt]+1)
                err = False
            
            else:
                err = False
                ans = [1]
            

            if note and err:
                ans_noted = ' '
                while ans_noted  != 'y' :
                    ans_noted = input(f"¨!! {output[-1][-2]} files for {subject} {ini_rel} {mvt}. [y]")
            else:
                pass

            # for each files: extract the data into a DataFrame and save it
            n = 1
            if len(ans) == 1:
                c,_,_,analog_data = read_c3d(file_all_mvt[0])
                # path_save = f"Data_NSLBP_VS/NSLBP-BIO-0{subject:02d}/{ini_rel}/{mvt} 1.csv"
                path_save = f"Data_NSLBP_VS_test/NSLBP-BIO-0{subject:02d}/{ini_rel}/{mvt} 1.csv"

                _ = extract_channels_c3d(c, analog_data, save = True, file_path = path_save )


            else:
                for f in file_all_mvt:
                    print(f)
                    c,_,_,analog_data = read_c3d(f)
                    # path_save = f"Data_NSLBP_VS/NSLBP-BIO-0{subject:02d}/{ini_rel}/{mvt} {n}.csv"
                    path_save = f"Data_NSLBP_VS_test/NSLBP-BIO-0{subject:02d}/{ini_rel}/{mvt} {n}.csv"
                    _ = extract_channels_c3d(c, analog_data, save = True, file_path = path_save )
                    n += 1
     

            # (ask for confirmation before going on with the next file )
            if ask_for_next:
                x = ' '
                while x not in ['y','n','False']:
                    x = input('Extract the next movement [y] ? stop [n] ? if [False] the question will not be asked for the future movements')

                if x == 'n':
                    return 0
                
                elif x == 'False':
                    ask_for_next = False

    return output
           





def save_csv_all_subjects(ask_for_next_s= True, ask_for_next_m = False, ask_for_note = True):
    """save the c3d files from all subject into csv while renaming the file

    Args:
        ask_for_next_s (bool, optional): ask before analysing the files of next subject. Defaults to True.
        ask_for_next_m (bool, optional): ask before analysing the next file for one subject . Defaults to False.
        ask_for_note (bool, optional): aks before analysing the next file when an error has been detected. Defaults to True.

    Returns:
        list: nested list describing each error : [subject, session, task, type of error, number of files]
    """
    error = []
    for s in SUBJECT:
        if ask_for_next_s:
            print(f'SUBJECT {s}')

        out = save_csv_subject(s,ask_for_next = ask_for_next_m, note = ask_for_note)
        error += out 

        if ask_for_next_s:
            x = ' '
            y = ' '
            while (x not in ['y','n','False']) and (x not in ['y','n']):
                x = input('Extract the next SUBJECT [y] ? stop [n] ? if [False] the question will not be asked for the future subjects')
                if ask_for_next_m:
                    y = input('Extract the next MOVEMENT [y] ? if [n] the question will not be asked for the future movements')


            if x == 'n':
                return error
            elif x == 'False':
                ask_for_next_s = False
                ask_for_next_m = False
            
            if y == 'n' and ask_for_next_m:
                ask_for_next_m = False

        if ask_for_note:
            ans_note = input('Ask for pause to note ? [y n]')
            if ans_note == 'n':
                ask_for_note = False

    return error





# EVENT

def save_event_subject(subject, ask_for_next = False, note = True):
    """ !!! glob files will get a file named *10* before *1_* thus for Normal Gait files are shifted apriori by one if there are >10 files
        ['C:\\Users\\Anais\\Documents\\NSLBP\\Data\\NSLBP-BIO-006\\20200804 - REL_session\\Gait_Normal 1.c3d',
        'C:\\Users\\Anais\\Documents\\NSLBP\\Data\\NSLBP-BIO-006\\20200804 - REL_session\\Gait_Normal 10.c3d',
        'C:\\Users\\Anais\\Documents\\NSLBP\\Data\\NSLBP-BIO-006\\20200804 - REL_session\\Gait_Normal 11.c3d',
        'C:\\Users\\Anais\\Documents\\NSLBP\\Data\\NSLBP-BIO-006\\20200804 - REL_session\\Gait_Normal 2.c3d',

    Args:
        subject (_type_): _description_
        ask_for_next (bool, optional): _description_. Defaults to False.
        note (bool, optional): _description_. Defaults to True.

    Returns:
        _type_: _description_
    """

    assert subject in SUBJECT
    number_per_file = {'Gait_Fast': 5, 'Gait_Normal':10, 'Gait_Slow': 2}
    output = []
    no_event = []
    

    for ini_rel in ['INI','REL']:
        #get all c3d file corresponding to the subject and the session
        # path = PATH_REPO + f"NSLBP-BIO-0{subject:02d}" + "\\* - "+ f"{ini_rel}_session\\output\\*.c3d"
        
        # path = PATH_REPO + f"NSLBP-BIO-0{subject:02d}" + "\\* - "+ f"{ini_rel}_session\\*.c3d"
        # all_file = glob(path)
        all_file = get_file_subject_c3d(subject, ini_rel)



        for mvt in TASKS_NAME_EVENT + TASKS_NAME_EVENT_MVIC:
            err = False
            if ask_for_next:
                print(f"patient {subject}, session {ini_rel}, movement {mvt}")

            # find the file(s) corresponding to the movement
            pattern = f"*{mvt}*.c3d"
            file_all_mvt = fnmatch.filter(all_file,pattern)
            nb = len(file_all_mvt)


            # check the number of files
            # if the movement is not in the dictionnary it should have one file
            if mvt not in list(number_per_file.keys()) and nb == 0 :
                print(f"There is no file for subject {subject} {ini_rel} {mvt}")
                output += [[subject,ini_rel,mvt,'missing',nb]]     
                err = True

                if note and err:
                    ans_noted = ' '
                    while ans_noted  != 'y' :
                        ans_noted = input(f"¨!! {output[-1][-2]} files for {subject} {ini_rel} {mvt}. [y]")

                continue

            elif mvt not in list(number_per_file.keys()) and nb > 1 :
                print(f"There is too many files  ( {nb} files )  for subject {subject} {ini_rel} {mvt}")
                output += [[subject,ini_rel,mvt,'too many',nb]]
                ans = np.arange(1,nb+1)
                err = True

            elif mvt in list(number_per_file.keys()) and nb == 0:
                print(f"There is no files for subject {subject} {ini_rel} {mvt}")
                output += [[subject,ini_rel,mvt,'missing',nb]]
                err = True

                if note and err:
                    ans_noted = ' '
                    while ans_noted  != 'y' :
                        ans_noted = input(f"¨!! {output[-1][-2]} files for {subject} {ini_rel} {mvt}. [y]")

                continue

            elif mvt in list(number_per_file.keys()) and nb > number_per_file[mvt]:
                print(f"There is too many files ( {nb} files ) for subject {subject} {ini_rel} {mvt}")
                output += [[subject,ini_rel,mvt,'too many',nb]]
                ans = np.arange(1,nb+1)
                err = True

            elif mvt in list(number_per_file.keys()) and nb < number_per_file[mvt]:
                print(f"There is some missing files ( {nb} files ) for subject {subject} {ini_rel} {mvt}")
                output += [[subject,ini_rel,mvt,'missing',nb]]
                ans = np.arange(1,nb+1)
                err = True
            
            elif mvt in list(number_per_file.keys()) and nb == number_per_file[mvt]:
                ans = np.arange(1,number_per_file[mvt]+1)
                err = False
            
            else:
                err = False
                ans = [1]
            

            if note and err:
                ans_noted = ' '
                while ans_noted  != 'y' :
                    ans_noted = input(f"¨!! {output[-1][-2]} files for {subject} {ini_rel} {mvt}. [y]")
            else:
                pass

            # for each files: extract the data into a DataFrame and save it
            n = 1
            if len(ans) == 1:
                start = file_all_mvt[0][:len("C:/Users/Anais/Documents/NSLBP/Data/NSLBP-BIO-001/20200603 - INI_session/")] 
                file_event =  start + "output\\" + file_all_mvt[0][len("C:/Users/Anais/Documents/NSLBP/Data/NSLBP-BIO-001/20200603 - INI_session/"):-4] + "_processed.c3d"
                c,_,_,_= read_c3d(file_event)

                # c,_,_,_= read_c3d(file_all_mvt[0])
                path_save = f"Data_NSLBP_VS_final/NSLBP-BIO-0{subject:02d}/{ini_rel}/output/{mvt} 1_processed.csv"
                try:
                    value = c['parameters']['EVENT']['LABELS']['value']
                    times = list(c['parameters']['EVENT']['TIMES']['value'][1])
                    times2 = list(c['parameters']['EVENT']['TIMES']['value'][0])

                    arr = np.array(list(zip(value,times,times2)))
                    df = pd.DataFrame(arr, columns = ['event','time_sec','time_min'])
                    df.to_csv(path_save,index = False)

                except Exception as e:
                    no_event += [[subject,ini_rel,mvt,n,e]]


            else:
                for f in file_all_mvt:
                    start = f[:len("C:/Users/Anais/Documents/NSLBP/Data/NSLBP-BIO-001/20200603 - INI_session/")] 
                    file_event = start + "output\\" + f[len("C:/Users/Anais/Documents/NSLBP/Data/NSLBP-BIO-001/20200603 - INI_session/"):-4] + "_processed.c3d"

                    print(file_event)
                    c,_,_,_= read_c3d(file_event)

                    # c,_,_,_ = read_c3d(f)
                    path_save = f"Data_NSLBP_VS_final/NSLBP-BIO-0{subject:02d}/{ini_rel}/output/{mvt} {n}_processed.csv"
                   
                    try:
                        value = c['parameters']['EVENT']['LABELS']['value']
                        times = list(c['parameters']['EVENT']['TIMES']['value'][1])
                        times2 = list(c['parameters']['EVENT']['TIMES']['value'][0])

                        arr = np.array(list(zip(value,times,times2)))
                        df = pd.DataFrame(arr, columns = ['event','time_sec','time_min'])
                        df.to_csv(path_save,index = False)
                    except Exception as e:
                        print([subject,ini_rel,mvt,n,e])
                        no_event += [[subject,ini_rel,mvt,n,e]]
                    
                    n += 1

      

            # (ask for confirmation before going on with the next file )
            if ask_for_next:
                x = ' '
                while x not in ['y','n','False']:
                    x = input('Extract the next movement [y] ? stop [n] ? if [False] the question will not be asked for the future movements')

                if x == 'n':
                    return 0
                
                elif x == 'False':
                    ask_for_next = False

    return output,no_event





def save_event_all_subjects(ask_for_next_s= True, ask_for_next_m = False, ask_for_note = False):
    """save the c3d files from all subject into csv while renaming the file

    Args:
        ask_for_next_s (bool, optional): ask before analysing the files of next subject. Defaults to True.
        ask_for_next_m (bool, optional): ask before analysing the next file for one subject . Defaults to False.
        ask_for_note (bool, optional): aks before analysing the next file when an error has been detected. Defaults to True.

    Returns:
        list: nested list describing each error : [subject, session, task, type of error, number of files]
    """
    error = []
    no_event = []
    for s in SUBJECT:
        if ask_for_next_s:
            print(f'SUBJECT {s}')

        out,no_ev = save_event_subject(s,ask_for_next = ask_for_next_m, note = ask_for_note)
        error += out 
        no_event += no_ev


        if ask_for_next_s:
            x = ' '
            y = ' '
            while (x not in ['y','n','False']) and (x not in ['y','n']):
                x = input('Extract the next SUBJECT [y] ? stop [n] ? if [False] the question will not be asked for the future subjects')
                if ask_for_next_m:
                    y = input('Extract the next MOVEMENT [y] ? if [n] the question will not be asked for the future movements')


            if x == 'n':
                return error
            elif x == 'False':
                ask_for_next_s = False
                ask_for_next_m = False
            
            if y == 'n' and ask_for_next_m:
                ask_for_next_m = False

        if ask_for_note:
            ans_note = input('Ask for pause to note ? [y n]')
            if ans_note == 'n':
                ask_for_note = False

    return error,no_event






# FROM CSV TO PYTHON STRUCTURES ------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------------------

def get_number_movement(subject,mvt,session,format = 'csv'):

    try:
        path = f"Data_NSLBP_VS_final/NSLBP-BIO-0{subject:02d}/{session}/{mvt}*.{format}"
        file = glob(path)
    except Exception as e:
        return [e,subject,session,mvt,'all','all']
    return len(file)




def load_file_subject_mvt_session2(subject,mvt,session = 'INI',format = '.csv', n = 1,repo = '_final'):
    """load csv or json file into dataframe

    Args:
        file_path (str): .csv or . json
    Raises:
        Exception: 

    Returns:
        Dataframe : each column corresponds to a channel
    """

    assert subject in SUBJECT, 'load file: unknown subject'
    assert mvt in TASKS_NAME, 'load file: unknown task'
    assert session in ['INI','REL'], 'load file: unknown session'

   
    file_path = f"Data_NSLBP_VS{repo}/NSLBP-BIO-0{subject:02d}/{session}/{mvt} {n}{format}"


    try: 
        if format == '.csv':
            df = pd.read_csv(file_path)
        elif format == '.json':
            df = pd.read_json(file_path)
        else:
            raise Exception("Unknown file format")
        
    except:
        raise Exception(f'Unable to load emg file {subject:02d} {mvt} {session} {n}')

    return df







def load_event_subject_mvt_session2(subject,mvt,session = 'INI',format = '.csv', n = 1):
    """load csv or json file into dataframe

    Args:
        file_path (str): .csv or . json
    Raises:
        Exception: 

    Returns:
        Dataframe : each column corresponds to a channel
    """

    assert subject in SUBJECT, 'load file: unknown subject'
    assert mvt in TASKS_NAME, 'load file: unknown task'
    assert session in ['INI','REL'], 'load file: unknown session'

    if mvt == 'Endurance_Sorensen':
        file_path = f'C:\\Users\\Anais\\Studio Code\\Project 1\\Data_NSLBP_VS_final_0\\NSLBP-BIO-0{subject:02d}\\{session}\\output\\{mvt} 1_processed.csv'
    else:
        file_path = f"Data_NSLBP_VS_final/NSLBP-BIO-0{subject:02d}/{session}/output/{mvt} {n}_processed{format}"


    try: 
        if format == '.csv':
            df = pd.read_csv(file_path)
        elif format == '.json':
            df = pd.read_json(file_path)
        else:
            raise Exception("Unknown file format")
        
    except:
        raise Exception(f'Unable to load event file {subject:02d} {mvt} {session} {n}')

    return df





from functions.processing_function import kinematic_interpolate,extract_event

# Selection of the side tht performed the smallest ROM (assumption hips stay horitonzal)

def side_min_flex(subject,session,trial,emg=[],show=False):

    task = 'Trunk_Lateral'
    if len(emg) == 0:
        emg = load_file_subject_mvt_session2(subject,task,session,n=trial)['L_LES']
    event = load_event_subject_mvt_session2(subject,task,session,n=trial)


    kinematic = pd.read_csv(f"Data_NSLBP_VS_final/NSLBP-BIO-0{subject:02d}/{session}/output/Trunk_Lateral 1_kine2.csv")

    # Identificatoin of the axis // to the hips
    diff = []
    for i in range(4):
        diff.append( np.abs(np.mean(kinematic[f'axis_ref_r{i}'][:200]) - np.mean(kinematic[f'axis_ref_l{i}'][:200])) )
    axis = np.argmax(diff) 

    # Identificatoin of the axis // to the spine
    shift_origin = (kinematic[f'axis_ref_r{axis}'] + kinematic[f'axis_ref_l{axis}'])/2

    diff_z = []
    for i in range(4):
        diff_z.append(np.abs(np.mean(kinematic[f'axis{i}'][:200]) - np.mean(np.mean(kinematic[f'axis_ref_r{i}'][:200]) - np.mean(kinematic[f'axis_ref_l{i}'][:200]))))
    axis_z = np.argmax(diff_z)

    # Identification of the sens of the axis // to the hips
    left_neg = True if diff[axis] > 0 else False #True id negative values correspond to left side flexion

    # center of the hips = center of the repere
    shift_origin_z = (kinematic[f'axis_ref_r{axis_z}'] + kinematic[f'axis_ref_l{axis_z}'])/2


    event = load_event_subject_mvt_session2(subject,mvt=task,session=session,n=trial)
    kine_interp = kinematic_interpolate(kinematic[f'axis{axis}'],emg) # C7 trajectory
    kine_interp_rep = kine_interp-kinematic_interpolate(shift_origin,emg) # center of the repere trajectory
    kine_interp_z = kinematic_interpolate(kinematic[f'axis{axis_z}'],emg)
    kine_interp_z_rep = kine_interp_z-kinematic_interpolate(shift_origin_z,emg)
    
    angle = np.arctan(kine_interp_rep/kine_interp_z_rep)*180/np.pi

    if show:
        fig,ax = plt.subplots(figsize=(7,4))
        ax.plot(emg)
        ax.plot(abs(angle*np.max(emg)/np.max(angle)))
        ax.plot(event_line*np.max(emg))
        ax.legend(['emg L_EO','angle normalized to max EMG','event'])
        ax.set(title=f'Trunk Lateral flexion P {subject} {session}',xlabel = 'frame',ylabel='amplitude(V)')


    event_line = extract_event(emg,event,'',show=show)
   
    # retrieve the frame that marks the end of flexion, ie the subject is in maximal flexion
    event_flex_min = pd.DataFrame(event.loc[event['event']=='stopMotion',:].apply(lambda x: (x['time_sec'] + x['time_min']*60) * SAMPLING_RATE , axis = 1),columns=['flexion_frame'])  # frame where the participant is in maximal flexion
    
    
    event_flex_min['mean'] = [ np.mean(angle[int(x-1*SAMPLING_RATE):int(x+1*SAMPLING_RATE)]) for x in event_flex_min['flexion_frame'].values ] # compute the mean of the kinematic values during 2 sec of max flexion
    event_flex_min['side'] = event_flex_min['mean'].apply(lambda x: 'left' if (x<0 and left_neg) or (x>0 and not left_neg) else 'right')

    amp_min = np.min(abs(event_flex_min['mean']))
    amp_min_row = event_flex_min.loc[abs(event_flex_min['mean'])==amp_min,:]
    side = amp_min_row['side'].values[0]
    
    return side






# OUTDATED, not used----------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------------



def get_file_subject_mvt_c3d(subject, mvt, ini_rel = 'INI', path_repo = PATH_REPO,print_path=False) : 
    """get file path corresponding to the subject, session and movement

    Args:
        subject (int): number of the subject
        mvt (str): name of the task
        ini_rel (bool, optional): INI, initial, reliability, REL,
        path_repo (_type_, optional):  Defaults to PATH_REPO.
        print_path (bool, optional):  Defaults to False.

    Returns:
        str: corresponding file path
    """
    assert ini_rel in ['INI','REL']
    
    path = path_repo + f"NSLBP-BIO-0{subject:02d}" + "\\* - "+ f"{ini_rel}_session\\{mvt}.c3d"
    
    file = glob(path)
    if print_path:
        print(f"path: {path}")
        print(file)
    
    assert len(file) != 0, f'No file found, path: {path}'
    assert len(file) == 1, f'many files: {file}'
    return file[0]







def extract_one_channel_c3d(file,ch, save_ = False, file_path_='', replace_ = False):

    assert ch in CHANNEL, 'unknown channel'
    if save_:
        assert file_path_ != '', 'empty file path'

    c, _, _, analog_data = read_c3d(file)
    data_test = extract_channels_c3d(c, analog_data, save=save_, file_path=file_path_, replace=replace_)

    return data_test[ch]









def save_file(data,file_path, replace = False):

    assert file_path != '', 'empty file path'
    assert type(data) == pd.core.frame.DataFrame,'the data is not a dataframe'

    if file_path[-4:] == '.csv':
        save_csv =  True
    elif file_path[-4:] == '.json':
        save_json = True

    else:
        raise Exception("Unknown file format")

    if (exists(file_path)):
        print(' the file already exists')
        if replace:
            if save_csv:
                data.to_csv(file_path,index=False)
            elif save_json:
                data.to_json(file_path,orient='index')
            print(f' save again to {file_path}') 

    else:
        if save_csv:
            data.to_csv(file_path,index=False)
        elif save_json:
            data.to_json(file_path,orient='index')

        print(f' save to {file_path}') 

        
    return 0


