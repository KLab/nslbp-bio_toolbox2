library(ggstatsplot)
# Select working directories
folder_inputs        <- "C:/Users/Anais/Studio Code/Project 1/nslbp-bio_toolbox2/R/Neblett rl/left_mean_0,1_max_1_HLBP/Input_validity"
folder_outputs       <- "C:/Users/Anais/Studio Code/Project 1/nslbp-bio_toolbox2/R/Neblett rl/left_mean_0,1_max_1_HLBP/Output_validity"
folder_outputs_plots <- "C:/Users/Anais/Studio Code/Project 1/nslbp-bio_toolbox2/R/Neblett rl/left_mean_0,1_max_1_HLBP/plots"
setwd(folder_inputs)
# Set dataset
Biomarkers <- list.files(folder_inputs)
tmp        <- dim.data.frame(Biomarkers)
# Initialisation
Unit      <- 0
AUC       <- 0
Youden    <- 0
Normality <- 0
P         <- 0
# START - Loop on biomarkers
for (i in 1:tmp[2]){
# Load data
file       <- Biomarkers[i]
Input_Data <- read.csv(paste(folder_inputs,file,sep="/"),header=TRUE,sep=",",dec=".")
Input_Data <- na.omit(Input_Data)
Unit[i]    <- Input_Data$Unit[1]
dim(Input_Data)
# Initialisation
roctemp     <- 0
shapirotemp <- 0
studenttemp <- 0
manntemp    <- 0
# ROC / AUC analysis
png(paste(folder_outputs_plots,str_replace(file, pattern='.csv', '_ROC.png'),sep="/"),
width = 1500, height = 1500, units = "px", bg = "white", res = 300,
pointsize=12)
roctemp   <- roc(response = Input_Data$Group,
predictor = Input_Data$Value,
levels=c("CTR", "PAT"),
direction="auto",
percent=FALSE,
plot=TRUE, auc.polygon=TRUE, max.auc.polygon=FALSE, grid=TRUE,
print.auc=TRUE, show.thres=TRUE,
main = paste(str_replace(file, pattern='.csv', ''))
)
AUC[i]    <- roctemp$auc*100
Youden[i] <- (max(roctemp$sensitivities+roctemp$specificities)-1)*100
dev.off()
# Check normality
model        <- lm(Value ~ Group, data = Input_Data)
shapirotemp  <- shapiro_test(residuals(model))
Normality[i] <- shapirotemp$p.value
ggqqplot(residuals(model),
title = paste(str_replace(file, pattern='.csv', '')),
ggtheme = theme_bw()) +
theme(plot.title = element_text(hjust = 0.5, face="bold"),
axis.text = element_text(size = 11),
panel.border = element_rect(colour = "black", fill=NA, size=0.5),
panel.grid.major = element_line(size = 0.25, linetype = 'dashed', colour = "grey"),
panel.grid.minor = element_line(size = 0.25, linetype = 'dashed', colour = "grey"))
ggsave(paste(folder_outputs_plots,str_replace(file, pattern='.csv', '_normality.png'),sep="/"),
plot = last_plot(), device = NULL, path = NULL,
scale = 1, width = 5, height = 5, dpi = 300, limitsize = TRUE)
# Compute the p-value using the correct test depending on normality test results
if (Normality[i] > 0.05){
# Student's t-Test
studenttemp <- t.test(Value ~ Group, data = Input_Data,
paired = FALSE,
conf.level = 0.95)
P[i]        <- studenttemp$p.value
}
else {
# Mann-Whitney U test
manntemp <- wilcox.test(Value ~ Group, data = Input_Data)
P[i]     <- manntemp$p.value
}
# Generate boxplots
if (Normality[i] > 0.05){
ggbetweenstats(data = Input_Data, x = Group, y = Value,
title = paste(str_replace(file, pattern='.csv', '')),
xlab = "Groups",
ylab = paste("Values (",Input_Data$Unit[1],")"),
plot.type = "boxviolin",
type = "p", # parametric test
test = "t.test",
paired = FALSE,
conf.level = 0.95,
outlier.tagging = FALSE,
centrality.plotting = FALSE,
point.args = list(position = ggplot2::position_jitterdodge(dodge.width = 0.6),
alpha = 1, size = 2, stroke = 0),
messages = FALSE,
results.subtitle = FALSE,
ggtheme = ggplot2::theme_bw()) +
scale_x_discrete(labels = c("CTR" = "Asymptomatics", "PAT" = "Patients")) +
geom_boxplot(color = "black", fill = "grey", alpha = 0.5, outlier.color = "black", width = 0.3) +
geom_signif(comparisons = list(c("CTR", "PAT")),
test = "t.test",
map_signif_level = FALSE,
annotation = c(paste('p = ',round(P[i],4),' (Student\'s t-Test)'))
) +
ggplot2::scale_color_manual(values = c("#111111", "#111111")) +
theme(plot.title = element_text(hjust = 0.5, face="bold"),
axis.text = element_text(size = 11),
panel.border = element_rect(colour = "black", fill=NA, size=0.5),
panel.grid.major = element_line(size = 0.25, linetype = 'dashed', colour = "grey"),
panel.grid.minor = element_line(size = 0.25, linetype = 'dashed', colour = "grey"))
}
else {
ggbetweenstats(data = Input_Data, x = Group, y = Value,
title = paste(str_replace(file, pattern='.csv', '')),
xlab = "Groups",
ylab = paste("Values (",Input_Data$Unit[1],")"),
plot.type = "boxviolin",
type = "np", # non-parametric test
test = "wilcox.test",
paired = FALSE,
conf.level = 0.95,
outlier.tagging = FALSE,
centrality.plotting = FALSE,
point.args = list(position = ggplot2::position_jitterdodge(dodge.width = 0.6),
alpha = 1, size = 2, stroke = 0),
messages = FALSE,
results.subtitle = FALSE,
ggtheme = ggplot2::theme_bw()) +
scale_x_discrete(labels = c("CTR" = "Asymptomatics", "PAT" = "Patients")) +
geom_boxplot(color = "black", fill = "grey", alpha = 0.5, outlier.color = "black", width = 0.3) +
geom_signif(comparisons = list(c("CTR", "PAT")),
test = "wilcox.test",
map_signif_level = FALSE,
annotation = c(paste('p = ',round(P[i],4),' (Mann-Whitney U Test)'))
) +
ggplot2::scale_color_manual(values = c("#111111", "#111111")) +
theme(plot.title = element_text(hjust = 0.5, face="bold"),
axis.text = element_text(size = 11),
panel.border = element_rect(colour = "black", fill=NA, size=0.5),
panel.grid.major = element_line(size = 0.25, linetype = 'dashed', colour = "grey"),
panel.grid.minor = element_line(size = 0.25, linetype = 'dashed', colour = "grey"))
}
ggsave(paste(folder_outputs_plots,str_replace(file, pattern='.csv', '_boxplot.png'),sep="/"),
plot = last_plot(), device = NULL, path = NULL,
scale = 1, width = 5, height = 5, dpi = 300, limitsize = TRUE)
# Clean data
rm("file","Input_Data","roctemp","shapirotemp","studenttemp","manntemp","model");
}
# NSLBP-BIO Project
# Compute validity parameters (AUC, YOUDEN INDEX, P-VALUE)
# Clear workspace
rm(list = ls())
# Load libraries
library(rstudioapi)
library(pROC)
library(ggpubr)
library(stringr)
library(rstatix)
library(tidyverse)
library(ggstatsplot)
# Select working directories
folder_inputs        <- "C:/Users/Anais/Studio Code/Project 1/nslbp-bio_toolbox2/R/Neblett rl/left_mean_0,1_max_1_HLBP/Input_validity"
folder_outputs       <- "C:/Users/Anais/Studio Code/Project 1/nslbp-bio_toolbox2/R/Neblett rl/left_mean_0,1_max_1_HLBP/Output_validity"
folder_outputs_plots <- "C:/Users/Anais/Studio Code/Project 1/nslbp-bio_toolbox2/R/Neblett rl/left_mean_0,1_max_1_HLBP/plots"
setwd(folder_inputs)
# Set dataset
Biomarkers <- list.files(folder_inputs)
tmp        <- dim.data.frame(Biomarkers)
# Initialisation
Unit      <- 0
AUC       <- 0
Youden    <- 0
Normality <- 0
P         <- 0
# START - Loop on biomarkers
for (i in 1:tmp[2]){
# Load data
file       <- Biomarkers[i]
Input_Data <- read.csv(paste(folder_inputs,file,sep="/"),header=TRUE,sep=",",dec=".")
Input_Data <- na.omit(Input_Data)
Unit[i]    <- Input_Data$Unit[1]
dim(Input_Data)
# Initialisation
roctemp     <- 0
shapirotemp <- 0
studenttemp <- 0
manntemp    <- 0
# ROC / AUC analysis
png(paste(folder_outputs_plots,str_replace(file, pattern='.csv', '_ROC.png'),sep="/"),
width = 1500, height = 1500, units = "px", bg = "white", res = 300,
pointsize=12)
roctemp   <- roc(response = Input_Data$Group,
predictor = Input_Data$Value,
levels=c("CTR", "PAT"),
direction="auto",
percent=FALSE,
plot=TRUE, auc.polygon=TRUE, max.auc.polygon=FALSE, grid=TRUE,
print.auc=TRUE, show.thres=TRUE,
main = paste(str_replace(file, pattern='.csv', ''))
)
AUC[i]    <- roctemp$auc*100
Youden[i] <- (max(roctemp$sensitivities+roctemp$specificities)-1)*100
dev.off()
# Check normality
model        <- lm(Value ~ Group, data = Input_Data)
shapirotemp  <- shapiro_test(residuals(model))
Normality[i] <- shapirotemp$p.value
ggqqplot(residuals(model),
title = paste(str_replace(file, pattern='.csv', '')),
ggtheme = theme_bw()) +
theme(plot.title = element_text(hjust = 0.5, face="bold"),
axis.text = element_text(size = 11),
panel.border = element_rect(colour = "black", fill=NA, size=0.5),
panel.grid.major = element_line(size = 0.25, linetype = 'dashed', colour = "grey"),
panel.grid.minor = element_line(size = 0.25, linetype = 'dashed', colour = "grey"))
ggsave(paste(folder_outputs_plots,str_replace(file, pattern='.csv', '_normality.png'),sep="/"),
plot = last_plot(), device = NULL, path = NULL,
scale = 1, width = 5, height = 5, dpi = 300, limitsize = TRUE)
# Compute the p-value using the correct test depending on normality test results
if (Normality[i] > 0.05){
# Student's t-Test
studenttemp <- t.test(Value ~ Group, data = Input_Data,
paired = FALSE,
conf.level = 0.95)
P[i]        <- studenttemp$p.value
}
else {
# Mann-Whitney U test
manntemp <- wilcox.test(Value ~ Group, data = Input_Data)
P[i]     <- manntemp$p.value
}
# Generate boxplots
if (Normality[i] > 0.05){
ggbetweenstats(data = Input_Data, x = Group, y = Value,
title = paste(str_replace(file, pattern='.csv', '')),
xlab = "Groups",
ylab = paste("Values (",Input_Data$Unit[1],")"),
plot.type = "boxviolin",
type = "p", # parametric test
test = "t.test",
paired = FALSE,
conf.level = 0.95,
outlier.tagging = FALSE,
centrality.plotting = FALSE,
point.args = list(position = ggplot2::position_jitterdodge(dodge.width = 0.6),
alpha = 1, size = 2, stroke = 0),
messages = FALSE,
results.subtitle = FALSE,
ggtheme = ggplot2::theme_bw()) +
scale_x_discrete(labels = c("CTR" = "Asymptomatics", "PAT" = "Patients")) +
geom_boxplot(color = "black", fill = "grey", alpha = 0.5, outlier.color = "black", width = 0.3) +
geom_signif(comparisons = list(c("CTR", "PAT")),
test = "t.test",
map_signif_level = FALSE,
annotation = c(paste('p = ',round(P[i],4),' (Student\'s t-Test)'))
) +
ggplot2::scale_color_manual(values = c("#111111", "#111111")) +
theme(plot.title = element_text(hjust = 0.5, face="bold"),
axis.text = element_text(size = 11),
panel.border = element_rect(colour = "black", fill=NA, size=0.5),
panel.grid.major = element_line(size = 0.25, linetype = 'dashed', colour = "grey"),
panel.grid.minor = element_line(size = 0.25, linetype = 'dashed', colour = "grey"))
}
else {
ggbetweenstats(data = Input_Data, x = Group, y = Value,
title = paste(str_replace(file, pattern='.csv', '')),
xlab = "Groups",
ylab = paste("Values (",Input_Data$Unit[1],")"),
plot.type = "boxviolin",
type = "np", # non-parametric test
test = "wilcox.test",
paired = FALSE,
conf.level = 0.95,
outlier.tagging = FALSE,
centrality.plotting = FALSE,
point.args = list(position = ggplot2::position_jitterdodge(dodge.width = 0.6),
alpha = 1, size = 2, stroke = 0),
messages = FALSE,
results.subtitle = FALSE,
ggtheme = ggplot2::theme_bw()) +
scale_x_discrete(labels = c("CTR" = "Asymptomatics", "PAT" = "Patients")) +
geom_boxplot(color = "black", fill = "grey", alpha = 0.5, outlier.color = "black", width = 0.3) +
geom_signif(comparisons = list(c("CTR", "PAT")),
test = "wilcox.test",
map_signif_level = FALSE,
annotation = c(paste('p = ',round(P[i],4),' (Mann-Whitney U Test)'))
) +
ggplot2::scale_color_manual(values = c("#111111", "#111111")) +
theme(plot.title = element_text(hjust = 0.5, face="bold"),
axis.text = element_text(size = 11),
panel.border = element_rect(colour = "black", fill=NA, size=0.5),
panel.grid.major = element_line(size = 0.25, linetype = 'dashed', colour = "grey"),
panel.grid.minor = element_line(size = 0.25, linetype = 'dashed', colour = "grey"))
}
ggsave(paste(folder_outputs_plots,str_replace(file, pattern='.csv', '_boxplot.png'),sep="/"),
plot = last_plot(), device = NULL, path = NULL,
scale = 1, width = 5, height = 5, dpi = 300, limitsize = TRUE)
# Clean data
rm("file","Input_Data","roctemp","shapirotemp","studenttemp","manntemp","model");
}
# END - Loop on biomarkers
# Export Data
Validity <- cbind(Biomarkers, Unit, Normality, P, AUC, Youden)
write.csv(Validity,paste(folder_outputs, "Validity.csv", sep="/"))
# Clean data
rm("Biomarkers","folder_inputs","folder_outputs","folder_outputs_plots","i","tmp","Validity")
# NSLBP-BIO Project
# Compute reliability and interpretability parameters (ICC, SEM, MDC)
# with a Fit Linear Mixed-Effects Models
# Clear workspace
rm(list = ls())
# Load libraries
library(rstudioapi)
library(tidyr)
library(magrittr)
library(lme4)
# Select working directories
folder_inputs  <- "C:/Users/Anais/Studio Code/Project 1/nslbp-bio_toolbox2/R/Neblett rl/left_mean_0,1_max_1_HLBP/Input_reliability"
folder_outputs <- "C:/Users/Anais/Studio Code/Project 1/nslbp-bio_toolbox2/R/Neblett rl/left_mean_0,1_max_1_HLBP/Output_reliability"
setwd(folder_inputs)
# Set dataset
Biomarkers <- list.files(folder_inputs)
tmp        <- dim.data.frame(Biomarkers)
# Initialisation
Unit            <- 0
ICC_intra       <- 0
ICC_testretest  <- 0
SEM_intra       <- 0
SEM_testretest  <- 0
SEMp_intra      <- 0
SEMp_testretest <- 0
MDC_intra       <- 0
MDC_testretest  <- 0
MDCp_intra      <- 0
MDCp_testretest <- 0
# START - Loop on biomarkers
for (i in 1:tmp[2]){
# Load data
file       <- Biomarkers[i]
Input_Data <- read.csv(paste(folder_inputs,file,sep="/"),header=TRUE,sep=",",dec=".")
Unit[i]    <- Input_Data$Unit[1]
dim(Input_Data)
Input_Data <- na.omit(Input_Data)
# Prepare data
Input_Data$Group    <- factor(Input_Data$Group, levels=c("CTR", "PAT"))
Input_Data$Session  <- factor(Input_Data$Session, levels=c("INI", "REL"))
Input_Data$Operator <- factor(Input_Data$Operator, levels=c("FM"))
# Compute variance Components with LMER Model (Fit Linear Mixed-Effects Models)
mod       <- lmer(Value~1 + (1|Participant) + (1|Participant:Session) + (1|Participant:Measure), data = Input_Data)
variance  <- as.data.frame(VarCorr(mod))
v_par_mes <- variance$vcov[1] # Variance associated with Measures
v_par_ses <- variance$vcov[2] # Variance associated with Sessions
v_par     <- variance$vcov[3] # Variance associated with Participants
v_res     <- variance$vcov[4] # Residual Variance
# Total variance
v_tot <- v_par_mes + v_par_ses + v_par + v_res
# Variance outcome
v_intra      <- v_par_mes + v_res
v_testretest <- v_par_ses + v_res
# ICC
ICC_intra[i]      <- (v_tot - v_intra) / v_tot
ICC_testretest[i] <- (v_tot - v_testretest) / v_tot
# SEM
SEM_intra[i]      <- sqrt(v_tot * (1 - ICC_intra[i]))
SEM_testretest[i] <- sqrt(v_tot * (1 - ICC_testretest[i]))
# SEM%
SEMp_intra[i]      <- SEM_intra[i]/mean(Input_Data$Value[])*100
SEMp_testretest[i] <- SEM_testretest[i]/mean(Input_Data$Value[])*100
# MDC95
MDC_intra[i]      <- 1.96 * sqrt(2) * SEM_intra[i]
MDC_testretest[i] <- 1.96 * sqrt(2) * SEM_testretest[i]
# MDC%
MDCp_intra[i]      <- MDC_intra[i]/mean(Input_Data$Value[])*100
MDCp_testretest[i] <- MDC_testretest[i]/mean(Input_Data$Value[])*100
# Clean data
rm("Input_Data", "mod", "variance", "v_par_mes", "v_par_ses",
"v_par", "v_res", "v_tot", "v_intra", "v_testretest")
}
# END - Loop on biomarkers
# Export Data
Reliability      <- cbind(Biomarkers, Unit, ICC_intra,  ICC_testretest, SEM_intra,  SEM_testretest, SEMp_intra,  SEMp_testretest)
Interpretability <- cbind(Biomarkers, Unit, MDC_intra,  MDC_testretest, MDCp_intra,  MDCp_testretest)
write.csv(Reliability,paste(folder_outputs, "Reliability.csv", sep="/"))
write.csv(Interpretability,paste(folder_outputs, "Interpretability.csv", sep="/"))
# NSLBP-BIO Project
# Compute validity parameters (AUC, YOUDEN INDEX, P-VALUE)
# Clear workspace
rm(list = ls())
# Load libraries
library(rstudioapi)
library(pROC)
library(ggpubr)
library(stringr)
library(rstatix)
library(tidyverse)
library(ggstatsplot)
# Select working directories
folder_inputs        <- "C:/Users/Anais/Studio Code/Project 1/nslbp-bio_toolbox2/R/left_mean_0,1_max_1_testu/Input_validity"
folder_outputs       <- "C:/Users/Anais/Studio Code/Project 1/nslbp-bio_toolbox2/R/left_mean_0,1_max_1_testu/Output_validity_testU"
folder_outputs_plots <- "C:/Users/Anais/Studio Code/Project 1/nslbp-bio_toolbox2/R/left_mean_0,1_max_1_testu/plots_testU"
setwd(folder_inputs)
# NSLBP-BIO Project
# Compute validity parameters (AUC, YOUDEN INDEX, P-VALUE)
# Clear workspace
rm(list = ls())
# Load libraries
library(rstudioapi)
library(pROC)
library(ggpubr)
library(stringr)
library(rstatix)
library(tidyverse)
library(ggstatsplot)
# Select working directories
folder_inputs        <- "C:/Users/Anais/Studio Code/Project 1/nslbp-bio_toolbox2/R/left_mean_0,1_max_1_testu/Input_validity"
folder_outputs       <- "C:/Users/Anais/Studio Code/Project 1/nslbp-bio_toolbox2/R/left_mean_0,1_max_1_testu/Output_validity"
folder_outputs_plots <- "C:/Users/Anais/Studio Code/Project 1/nslbp-bio_toolbox2/R/left_mean_0,1_max_1_testu/plots"
setwd(folder_inputs)
# NSLBP-BIO Project
# Compute validity parameters (AUC, YOUDEN INDEX, P-VALUE)
# Clear workspace
rm(list = ls())
# Load libraries
library(rstudioapi)
library(pROC)
library(ggpubr)
library(stringr)
library(rstatix)
library(tidyverse)
library(ggstatsplot)
# Select working directories
folder_inputs        <- "C:/Users/Anais/Studio Code/Project 1/nslbp-bio_toolbox2/R/Neblett rl/left_mean_0,1_max_1_testu/Input_validity"
folder_outputs       <- "C:/Users/Anais/Studio Code/Project 1/nslbp-bio_toolbox2/R/Neblett rl/left_mean_0,1_max_1_testu/Output_validity"
folder_outputs_plots <- "C:/Users/Anais/Studio Code/Project 1/nslbp-bio_toolbox2/R/Neblett rl/left_mean_0,1_max_1_testu/plots"
setwd(folder_inputs)
# Set dataset
Biomarkers <- list.files(folder_inputs)
tmp        <- dim.data.frame(Biomarkers)
# Initialisation
Unit      <- 0
AUC       <- 0
Youden    <- 0
Normality <- 0
P         <- 0
# START - Loop on biomarkers
for (i in 1:tmp[2]){
# Load data
file       <- Biomarkers[i]
Input_Data <- read.csv(paste(folder_inputs,file,sep="/"),header=TRUE,sep=",",dec=".")
Input_Data <- na.omit(Input_Data)
Unit[i]    <- Input_Data$Unit[1]
dim(Input_Data)
# Initialisation
roctemp     <- 0
shapirotemp <- 0
studenttemp <- 0
manntemp    <- 0
# ROC / AUC analysis
png(paste(folder_outputs_plots,str_replace(file, pattern='.csv', '_ROC.png'),sep="/"),
width = 1500, height = 1500, units = "px", bg = "white", res = 300,
pointsize=12)
roctemp   <- roc(response = Input_Data$Group,
predictor = Input_Data$Value,
levels=c("CTR", "PAT"),
direction="auto",
percent=FALSE,
plot=TRUE, auc.polygon=TRUE, max.auc.polygon=FALSE, grid=TRUE,
print.auc=TRUE, show.thres=TRUE,
main = paste(str_replace(file, pattern='.csv', ''))
)
AUC[i]    <- roctemp$auc*100
Youden[i] <- (max(roctemp$sensitivities+roctemp$specificities)-1)*100
dev.off()
# Compute the p-value using the correct test depending on normality test results
# Mann-Whitney U test
manntemp <- wilcox.test(Value ~ Group, data = Input_Data)
P[i]     <- manntemp$p.value
# Generate boxplots
ggbetweenstats(data = Input_Data, x = Group, y = Value,
title = paste(str_replace(file, pattern='.csv', '')),
xlab = "Groups",
ylab = paste("Values (",Input_Data$Unit[1],")"),
plot.type = "boxviolin",
type = "np", # non-parametric test
test = "wilcox.test",
paired = FALSE,
conf.level = 0.95,
outlier.tagging = FALSE,
centrality.plotting = FALSE,
point.args = list(position = ggplot2::position_jitterdodge(dodge.width = 0.6),
alpha = 1, size = 2, stroke = 0),
messages = FALSE,
results.subtitle = FALSE,
ggtheme = ggplot2::theme_bw()) +
scale_x_discrete(labels = c("CTR" = "Asymptomatics", "PAT" = "Patients")) +
geom_boxplot(color = "black", fill = "grey", alpha = 0.5, outlier.color = "black", width = 0.3) +
geom_signif(comparisons = list(c("CTR", "PAT")),
test = "wilcox.test",
map_signif_level = FALSE,
annotation = c(paste('p = ',round(P[i],4),' (Mann-Whitney U Test)'))
) +
ggplot2::scale_color_manual(values = c("#111111", "#111111")) +
theme(plot.title = element_text(hjust = 0.5, face="bold"),
axis.text = element_text(size = 11),
panel.border = element_rect(colour = "black", fill=NA, size=0.5),
panel.grid.major = element_line(size = 0.25, linetype = 'dashed', colour = "grey"),
panel.grid.minor = element_line(size = 0.25, linetype = 'dashed', colour = "grey"))
ggsave(paste(folder_outputs_plots,str_replace(file, pattern='.csv', '_boxplot.png'),sep="/"),
plot = last_plot(), device = NULL, path = NULL,
scale = 1, width = 5, height = 5, dpi = 300, limitsize = TRUE)
# Clean data
rm("file","Input_Data","roctemp","shapirotemp","studenttemp","manntemp","model");
}
# END - Loop on biomarkers
# Export Data
Validity <- cbind(Biomarkers, Unit, Normality, P, AUC, Youden)
write.csv(Validity,paste(folder_outputs, "Validity.csv", sep="/"))
# Clean data
rm("Biomarkers","folder_inputs","folder_outputs","folder_outputs_plots","i","tmp","Validity")
