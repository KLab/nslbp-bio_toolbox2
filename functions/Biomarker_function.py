
from functions.requirements import *
from functions.plot_function import *
from functions.c3d_function import *
from functions.processing_function import *

#####################################################################################
# This file comprises the computation of all biomarkers
# The majority of them are ordered according to their corresponding study
#####################################################################################




#####################
# LIMA et al., 2018 #
#####################

#see also BMu96, 69


def preprocessing_lima_2018(emg):

    filtered_emg = comb_filter(notch_filter( butterfilter_dual( linear_detrend( mean_removal( emg ) ),2,499 ), fl=49,fh=51  ),  f0=50, ft = 500 )

    return filtered_emg



def preprocessing_contraction_lima_2018(filtered_emg,win = 2):

    assert not empty_value_check(filtered_emg), 'Empty values'
    # preprocessing of the emg for onset detection
    filtered_emg_onset = butterfilter_dual(full_wave(filtered_emg),order = 1,c_f=2,type = 'lowpass') 
    # extract the baseline from the first (win) seconds of the signal: the baseline will be used for the double threshold aglorithm
    baseline = filtered_emg_onset[:int(win*SAMPLING_RATE)]

    return filtered_emg_onset,baseline




def contraction_detection_lima_2018(filtered_emg_onset,baseline ):
    
    assert not empty_value_check(filtered_emg_onset)

    _, state_contraction, _,_ = double_threshold (filtered_emg_onset ,time_win=1,baseline=baseline)
    contracted = [ True if e == 1 else False for e in state_contraction ]
    emg_contracted = filtered_emg_onset[contracted]
   
    return emg_contracted
   






def BMu_lima_norm(emg,event,sub_MVIC = None,sub_MVIC_event=None, show = False, **kwargs ):
    """"
    Args:
        emg (ndarray): 
        event (Dataframe): columns are event time_sec time_min. events are bending and return
        show (bool, optional): display the results. Defaults to False.

    Returns:
        output: float: unique result of the BMu computation according to the chosen subphase/computation, 
        detailed_output: Dataframe: results of BMu computation for each subphase
    """

    try:
        nb_BM = kwargs['nb_BM']
    except:
        pass

    assert not empty_value_check(emg), 'empty values'

    # preprocessing Lima
    filtered_emg = preprocessing_lima_2018(emg)

    # preprocessing for event + extraction of baseline 
    event_emg,baseline = preprocessing_contraction_lima_2018(filtered_emg)

    # normalization
    event_line_mvc = extract_event(sub_MVIC,sub_MVIC_event,event_type = 'MVC', task_name = 'Endurance_Sorensen')
    filtered_sub_MVIC = full_wave(preprocessing_lima_2018(sub_MVIC[event_line_mvc == 1]))

    #signal
    filtered_sub_MVIC_mean = np.mean(filtered_sub_MVIC)
    event_emg = normalization(event_emg,filtered_sub_MVIC_mean)
    # #rms
    # filtered_sub_MVIC_rms = rms(filtered_sub_MVIC)

    # event detection and segmentation
    event_line_emg = extract_event(event_emg,event,event_type = '')
    df_emg_subphase = event_2_segmentation(event_emg,event_line_emg )


    # BMu :   
    # Computation of the RMS value of the signal when the muscle is contracted for each subphase (bending and return)

    # normalisation signal
    df_emg_subphase['value'] = df_emg_subphase['emg'].apply(lambda x: rms (contraction_detection_lima_2018(x,baseline=baseline)) )

    # #normalisation rms
    # df_emg_subphase['value'] = df_emg_subphase['emg'].apply(lambda x: rms (contraction_detection_lima_2018(x,baseline=baseline)) / filtered_sub_MVIC_rms * 100 )

    if nb_BM in [97,98,96] : # all subphases
        # selection of the subparts where the patient is holding the load ie Lowering anf Lifting, not reaching for the load nor returning from lowering the load
        df_output_subphase = df_emg_subphase.loc[((df_emg_subphase['rep']%2 == 0) & (df_emg_subphase['event'] == 'bending')) | ((df_emg_subphase['rep']%2 == 1) & (df_emg_subphase['event'] == 'return')),: ].reset_index().drop('index',axis=1)
        # output is the mean of the BM computed for each subphases (lowering and lifting) of each repetition ( one lowering and lifting )
        output = df_output_subphase['value'].mean()
        detailed_output = df_output_subphase[['value','event','rep']]
        # extract the mean computed during bending and return (! only if they can be paired), associated 1 return with the following bending
        # compute the mean of the bending and return value 
        output_rel_list = np.nanmean(list(zip(df_output_subphase.loc[df_output_subphase['event'] == 'return']['value'].iloc[0:np.max(df_output_subphase['rep'])//2],df_output_subphase.loc[df_output_subphase['event'] == 'bending']['value'].iloc[0:np.max(df_output_subphase['rep'])//2])),axis=1)
        output_rel = pd.DataFrame({'Measure':[f'M{i+1}' for i in range(len(output_rel_list))],'Value':output_rel_list})
  


    elif nb_BM in [22,23,24] : # return : stand to sit
        # selection of the right subphase
        output = df_emg_subphase.loc[df_emg_subphase['event'] == 'return']['value'].mean()
        detailed_output = df_emg_subphase[['value','event','rep']]
        output_rel_list = df_emg_subphase.loc[df_emg_subphase['event'] == 'return']['value'].values
        output_rel = pd.DataFrame({'Measure':[f'M{i+1}' for i in range(len(output_rel_list))],'Value':output_rel_list})

    elif nb_BM in [81,82,69] : # bending : sit to stand
        # selection of the right subphase
        output_rel_list = df_emg_subphase.loc[df_emg_subphase['event'] == 'bending']['value'].values
        output = np.nanmean(df_emg_subphase.loc[df_emg_subphase['event'] == 'bending']['value'])
        detailed_output = df_emg_subphase[['value','event','rep']]
        output_rel = pd.DataFrame({'Measure':[f'M{i+1}' for i in range(len(output_rel_list))],'Value':output_rel_list})



    if show:
        print(' The result for BMu97 is: %.5f V +/- %0.5f \n' % (output,np.nanstd(df_emg_subphase['value'])))

    return output,detailed_output, output_rel




def BMu_lima(emg,event,sub_MVIC = None,sub_MVIC_event=None, show = False, **kwargs ):
    """
    Args:
        emg (ndarray): 
        event (Dataframe): columns are event time_sec time_min. events are bending and return
        show (bool, optional): display the results. Defaults to False.

    Returns:
        output: float: unique result of the BMu computation according to the chosen subphase/computation, 
        detailed_output: Dataframe: results of BMu computation for each subphase
    """

    try:
        nb_BM = kwargs['nb_BM']
    except:
        pass

    assert not empty_value_check(emg), 'empty values'

    # preprocessing Lima
    filtered_emg = preprocessing_lima_2018(emg)

    # preprocessing for event + extraction of baseline 
    event_emg,baseline = preprocessing_contraction_lima_2018(filtered_emg)

    # event detection and segmentation
    event_line_emg = extract_event(event_emg,event,event_type = '')
    df_emg_subphase = event_2_segmentation(event_emg,event_line_emg )


    # BMu : 
    # Computation of the RMS value of the signal when the muscle is contracted for each subphase (bending and return)
    df_emg_subphase['value'] = df_emg_subphase['emg'].apply(lambda x: rms (contraction_detection_lima_2018(x,baseline=baseline)) )

    if nb_BM in [97,98,96] : # all subphases
        # selection of the subparts where the patient is holding the load ie Lowering anf Lifting, not reaching for the load nor returning from lowering the load
        df_output_subphase = df_emg_subphase.loc[((df_emg_subphase['rep']%2 == 0) & (df_emg_subphase['event'] == 'bending')) | ((df_emg_subphase['rep']%2 == 1) & (df_emg_subphase['event'] == 'return')),: ].reset_index().drop('index',axis=1)
        # output is the mean of the BM computed for each subphases (lowering and lifting) of each repetition ( one lowering and lifting )
        output = df_output_subphase['value'].mean()
        detailed_output = df_output_subphase[['value','event','rep']]
        # extract the mean computed during bending and return (! only if they can be paired), associated 1 return with the following bending
        # compute the mean of the bending and return value 
        output_rel_list = np.nanmean(list(zip(df_output_subphase.loc[df_output_subphase['event'] == 'return']['value'].iloc[0:np.max(df_output_subphase['rep'])//2],df_output_subphase.loc[df_output_subphase['event'] == 'bending']['value'].iloc[0:np.max(df_output_subphase['rep'])//2])),axis=1)
        output_rel = pd.DataFrame({'Measure':[f'M{i+1}' for i in range(len(output_rel_list))],'Value':output_rel_list})
  


    elif nb_BM in [22,23,24] : # return : stand to sit
        # selection of the right subphase
        output = df_emg_subphase.loc[df_emg_subphase['event'] == 'return']['value'].mean()
        detailed_output = df_emg_subphase[['value','event','rep']]
        output_rel_list = df_emg_subphase.loc[df_emg_subphase['event'] == 'return']['value'].values
        output_rel = pd.DataFrame({'Measure':[f'M{i+1}' for i in range(len(output_rel_list))],'Value':output_rel_list})

    elif nb_BM in [81,82,69] : # bending : sit to stand
        # selection of the right subphase
        output_rel_list = df_emg_subphase.loc[df_emg_subphase['event'] == 'bending']['value'].values
        output = np.nanmean(df_emg_subphase.loc[df_emg_subphase['event'] == 'bending']['value'])
        detailed_output = df_emg_subphase[['value','event','rep']]
        output_rel = pd.DataFrame({'Measure':[f'M{i+1}' for i in range(len(output_rel_list))],'Value':output_rel_list})



    if show:
        print(' The result for BMu97 is: %.5f V +/- %0.5f \n' % (output,np.nanstd(df_emg_subphase['value'])))

    return output,detailed_output, output_rel











def BMu69(emg,event,sub_MVIC = None,sub_MVIC_event=None, show = False, **kwargs ):
    return BMu_lima(emg,event,sub_MVIC = None,sub_MVIC_event=None, show = False, **{'nb_BM':69})
 
def BMu97(emg,event,sub_MVIC = None,sub_MVIC_event=None, show = False, **kwargs ):
    return BMu_lima(emg,event,sub_MVIC = None,sub_MVIC_event=None, show = False, **{'nb_BM':97})

def BMu96(emg,event,sub_MVIC = None,sub_MVIC_event=None, show = False, **kwargs ):
    return BMu_lima(emg,event,sub_MVIC = None,sub_MVIC_event=None, show = False, **{'nb_BM':96})

def BMu98(emg,event,sub_MVIC = None,sub_MVIC_event=None, show = False, **kwargs ):
    return BMu_lima(emg,event,sub_MVIC = None,sub_MVIC_event=None, show = False, **{'nb_BM':98})

def BMu22(emg,event,sub_MVIC = None,sub_MVIC_event=None, show = False, **kwargs ):
    return BMu_lima(emg,event,sub_MVIC = None,sub_MVIC_event=None, show = False, **{'nb_BM':22})

def BMu23(emg,event,sub_MVIC = None,sub_MVIC_event=None, show = False, **kwargs ):
    return BMu_lima(emg,event,sub_MVIC = None,sub_MVIC_event=None, show = False, **{'nb_BM':23})

def BMu24(emg,event,sub_MVIC = None,sub_MVIC_event=None, show = False, **kwargs ):
    return BMu_lima(emg,event,sub_MVIC = None,sub_MVIC_event=None, show = False, **{'nb_BM':24})

def BMu81(emg,event,sub_MVIC = None,sub_MVIC_event=None, show = False, **kwargs ):
    return BMu_lima(emg,event,sub_MVIC = None,sub_MVIC_event=None, show = False, **{'nb_BM':81})

def BMu82(emg,event,sub_MVIC = None,sub_MVIC_event=None, show = False, **kwargs ):
    return BMu_lima(emg,event,sub_MVIC = None,sub_MVIC_event=None, show = False, **{'nb_BM':82})


def BMu96_norm(emg,event,sub_MVIC = None,sub_MVIC_event=None, show = False, **kwargs ):
    return BMu_lima_norm(emg,event,sub_MVIC ,sub_MVIC_event, show = False, **{'nb_BM':96})

def BMu97_norm(emg,event,sub_MVIC = None,sub_MVIC_event=None, show = False, **kwargs ):
    return BMu_lima_norm(emg,event,sub_MVIC ,sub_MVIC_event, show = False, **{'nb_BM':97})




########################
# Neblett et al., 2014 #
########################


def preprocessing_neblett_2014(emg):

    # filtered_emg = rolling_function( butterfilter_bandpass_dual( full_wave( mean_removal(emg) ), order = 4, c_f_low=20, c_f_high=499),win = 0.5,func= lambda x: rms(x))
    filtered_emg = rolling_function( ( butterfilter_bandpass_dual( mean_removal(emg), order = 4, c_f_low=20, c_f_high=499)),win = 0.5,func= lambda x: rms(x))

    return filtered_emg


def BMu_neblett(emg,event,sub_MVIC = None,sub_MVIC_event=None, show = False, **kwargs ):

    # preprocessing emg
    filtered_emg = preprocessing_neblett_2014(emg)

    return BMu_neblett_wo_preprocessing(filtered_emg,event,sub_MVIC = None,sub_MVIC_event=None, show = False, **kwargs )



def BMu_neblett_wo_preprocessing(filtered_emg,event,sub_MVIC = None,sub_MVIC_event=None, show = False, **kwargs ):

    nb_BM = kwargs['nb_BM']

    #event detection
    event_inbtw_phase = event_segmentation_inbtw_subpart(filtered_emg,event,win = 1.5)

    # BMu
    mean_stand = event_inbtw_phase.loc[event_inbtw_phase['event'] == 'stand'].apply(lambda x: np.nanmean(x['emg']),axis=1).reset_index().drop('index',axis=1)
    mean_flexion= event_inbtw_phase.loc[event_inbtw_phase['event'] == 'flexion'].apply(lambda x: np.nanmean(x['emg']),axis=1).reset_index().drop('index',axis=1)

    if nb_BM == 44:
        event_line_emg = extract_event(filtered_emg,event,event_type = '')
        df_emg_subphase = event_2_segmentation(filtered_emg,event_line_emg )
        return_df = df_emg_subphase.loc[df_emg_subphase['event']=='return'].apply(lambda x: np.nanmax(x['emg']),axis=1).reset_index().drop('index',axis=1)
        output_rep = return_df/mean_flexion
        output =  output_rep.mean().values[0]

    elif nb_BM == 43:
        output_rep = mean_stand/mean_flexion
        output =  output_rep.mean().values[0]

    elif nb_BM == 91:
        output_rep = mean_stand
        output = output_rep.mean().values[0]

    elif nb_BM == 42:
        event_line_emg = extract_event(filtered_emg,event,event_type = '')
        df_emg_subphase = event_2_segmentation(filtered_emg,event_line_emg )
        bending_df = df_emg_subphase.loc[df_emg_subphase['event']=='bending'].apply(lambda x: np.nanmax(x['emg']),axis=1).reset_index().drop('index',axis=1)
        output_rep = bending_df/mean_flexion
        output = output_rep.mean().values[0]

    elif nb_BM == 35:
        event_line_emg = extract_event(filtered_emg,event,event_type = '')
        df_emg_subphase = event_2_segmentation(filtered_emg,event_line_emg )
        bending_df = df_emg_subphase.loc[df_emg_subphase['event']=='bending'].apply(lambda x: np.nanmax(x['emg']),axis=1).reset_index().drop('index',axis=1)
        return_df = df_emg_subphase.loc[df_emg_subphase['event']=='return'].apply(lambda x: np.nanmax(x['emg']),axis=1).reset_index().drop('index',axis=1)
        output_rep = bending_df/return_df
        output = output_rep.mean().values[0]

    elif nb_BM == 15:
        output_rep = mean_flexion
        output = mean_flexion.mean().values[0]


    output_detailed =  pd.DataFrame({'value':output_rep.values.flatten(),'event':None,'rep':np.arange(1,len(output_rep.values.flatten())+1)})
    output_rel = pd.DataFrame({'Measure':[f'M{i+1}' for i in range(len(output_rep.values.flatten()))],'Value':output_rep.values.flatten()})    

    return output,output_detailed,output_rel

# EMGs of right and left sides have to be preprocessed and summed up before BMu computation
def BMu42(emg,event,sub_MVIC = None,sub_MVIC_event=None, show = False, **kwargs ):
    return BMu_neblett_wo_preprocessing(emg,event,sub_MVIC,sub_MVIC_event, show , **{'nb_BM':42})

def BMu44(emg,event,sub_MVIC = None,sub_MVIC_event=None, show = False, **kwargs ):
    return BMu_neblett_wo_preprocessing(emg,event,sub_MVIC ,sub_MVIC_event, show , **{'nb_BM':44})

def BMu43(emg,event,sub_MVIC = None,sub_MVIC_event=None, show = False, **kwargs ):
    return BMu_neblett_wo_preprocessing(emg,event,sub_MVIC,sub_MVIC_event, show , **{'nb_BM':43})

def BMu35(emg,event,sub_MVIC = None,sub_MVIC_event=None, show = False, **kwargs ):
    return BMu_neblett_wo_preprocessing(emg,event,sub_MVIC,sub_MVIC_event, show , **{'nb_BM':35})

def BMu91(emg,event,sub_MVIC = None,sub_MVIC_event=None, show = False, **kwargs ):
    return BMu_neblett_wo_preprocessing(emg,event,sub_MVIC,sub_MVIC_event, show , **{'nb_BM':91})

def BMu15_neblett(emg,event,sub_MVIC = None,sub_MVIC_event=None, show = False, **kwargs ):
    return BMu_neblett_wo_preprocessing(emg,event,sub_MVIC,sub_MVIC_event, show , **{'nb_BM':15})



def BMu42_rl(emg,event,sub_MVIC = None,sub_MVIC_event=None, show = False, **kwargs ):
    return BMu_neblett(emg,event,sub_MVIC,sub_MVIC_event, show , **{'nb_BM':42})

def BMu44_rl(emg,event,sub_MVIC = None,sub_MVIC_event=None, show = False, **kwargs ):
    return BMu_neblett(emg,event,sub_MVIC ,sub_MVIC_event, show , **{'nb_BM':44})

def BMu43_rl(emg,event,sub_MVIC = None,sub_MVIC_event=None, show = False, **kwargs ):
    return BMu_neblett(emg,event,sub_MVIC,sub_MVIC_event, show , **{'nb_BM':43})

def BMu35_rl(emg,event,sub_MVIC = None,sub_MVIC_event=None, show = False, **kwargs ):
    return BMu_neblett(emg,event,sub_MVIC,sub_MVIC_event, show , **{'nb_BM':35})

def BMu91_rl(emg,event,sub_MVIC = None,sub_MVIC_event=None, show = False, **kwargs ):
    return BMu_neblett(emg,event,sub_MVIC,sub_MVIC_event, show , **{'nb_BM':91})

def BMu15_neblett_rl(emg,event,sub_MVIC = None,sub_MVIC_event=None, show = False, **kwargs ):
    return BMu_neblett(emg,event,sub_MVIC,sub_MVIC_event, show , **{'nb_BM':15})


def BMu91_standrl(emg,event=None,sub_MVIC = None,sub_MVIC_event=None, show = False, **kwargs ):
    filtered_emg = preprocessing_neblett_2014(emg)
    n = len(filtered_emg)//3
    output_rel_list = [np.nanmean(filtered_emg[ i*n : (i+1)*n]) for i in range(3)]
    output = np.nanmean(output_rel_list)
    output_rel = pd.DataFrame({'Measure':[f'M{i+1}' for i in range(len(output_rel_list))],'Value':output_rel_list}) 

    return output,pd.DataFrame({'value':output_rel_list,'event':None,'rep':[i for i in range(1,4)]}),output_rel

def BMu91_stand(filtered_emg,event,sub_MVIC = None,sub_MVIC_event=None, show = False, **kwargs ):

    n = len(filtered_emg)//3
    output_rel_list = [np.nanmean(filtered_emg[ i*n : (i+1)*n]) for i in range(3)]
    output = np.nanmean(output_rel_list)
    output_rel = pd.DataFrame({'Measure':[f'M{i+1}' for i in range(len(output_rel_list))],'Value':output_rel_list})  
    # preprocessing emg
    # filtered_emg = preprocessing_neblett_2014(emg[10*SAMPLING_RATE:20*SAMPLING_RATE])
    return output,pd.DataFrame({'value':output_rel_list,'event':None,'rep':[i for i in range(1,4)]}),output_rel



#######################
# WATSON et al., 1997 #
#######################


def preprocessing_watson_1997(emg):
    filtered_emg = notch_filter(butterfilter_bandpass_dual(mean_removal(emg), order = 2, c_f_high=430, c_f_low=20),f0=50)
    return filtered_emg


def BMu42_watson(emg,event,sub_MVIC = None,sub_MVIC_event=None, show = False, **kwargs ):


    # preprocessing emg
    filtered_emg = preprocessing_watson_1997(emg)
    

    # segmentation_inbtw_subpart(filtered_emg,event,win = 1.5)

    # extraction of the events start/stop_standing_flexion
    df_extracted_event = event_extraction_flex_return(filtered_emg,event)
    # segmentation of the emg according to the previously computed event
    df_emg_subphases = event_segmentation_from_df_time(filtered_emg,df_extracted_event)

    # isolate lfexion and full flexion subphases
    df_flexion = df_emg_subphases.loc[df_emg_subphases['event']=='stop_standing',:]
    df_full_flexion = df_emg_subphases.loc[df_emg_subphases['event']=='start_flexion',:]

    df_flexion['value']=df_flexion['emg'].apply(lambda x: np.nanmax(rolling_function(x,win=1,func=rms)) )
    output_rel_list = df_flexion['value'].values / df_full_flexion['emg'].apply(lambda x: rms(x[len(x)//2-500:len(x)//2+500])).values
    output = output_rel_list.mean()
    output_rel = pd.DataFrame({'Measure':[f'M{i+1}' for i in range(len(output_rel_list))],'Value':output_rel_list})  
    output_detailed =   pd.DataFrame(data=list(zip(output_rel_list,[None,None,None],np.arange(1,len(output_rel_list)+1))), columns=['value','event','rep'])

    
    return output,output_detailed, output_rel




######################
# AHERN et al., 1988 #
######################

def preprocessing_ahern_1988(emg):
    filtered_emg = butterfilter_dual( full_wave( mean_removal(emg) ), order=1, c_f=1/(2*np.pi*0.5), type='lowpass')
    return filtered_emg


def BMu_Ahern(emg,event,sub_MVIC = None,sub_MVIC_event=None, show = False, **kwargs ):


    prepro = kwargs['preprocessing'] 
    # preprocessing emg
    filtered_emg = prepro(emg)
    

    #event detection
    event_inbtw_phase = event_segmentation_inbtw_subpart(filtered_emg,event,win = 1.5)

    # BMu
    max_flexion = event_inbtw_phase.loc[event_inbtw_phase['event'] == 'flexion'].apply(lambda x: np.nanmax(x['emg']),axis=1).reset_index().drop('index',axis=1)
    min_flexion = event_inbtw_phase.loc[event_inbtw_phase['event'] == 'flexion'].apply(lambda x: np.nanmin(x['emg']),axis=1).reset_index().drop('index',axis=1)

    diff_flexion = max_flexion - min_flexion
    output_rel = pd.DataFrame({'Measure':[f'M{i+1}' for i in range(len(diff_flexion.values.flatten()))],'Value':diff_flexion.values.flatten()})  
    output_detailed = pd.DataFrame({'value':diff_flexion.values.flatten(),'event':None,'rep':[i for i in range(1,len(diff_flexion.values.flatten())+1)]})
    
    return np.nanmean(diff_flexion.values.flatten()),output_detailed,output_rel


def BMu1(emg,event,sub_MVIC = None,sub_MVIC_event=None, show = False, **kwargs):
    kwargs['preprocessing'] = preprocessing_ahern_1988
    return BMu_Ahern(emg,event,sub_MVIC = None,sub_MVIC_event=None, show = False, **kwargs)

def BMu1_Neblett(emg,event,sub_MVIC = None,sub_MVIC_event=None, show = False, **kwargs):
    kwargs['preprocessing'] = preprocessing_neblett_2014
    return BMu_Ahern(emg,event,sub_MVIC = None,sub_MVIC_event=None, show = False, **kwargs)


def BMu15(emg,event,sub_MVIC = None,sub_MVIC_event=None, show = False, **kwargs ):


    # preprocessing emg
    filtered_emg = preprocessing_ahern_1988(emg)
    
    #event detection
    event_line_emg = extract_event(filtered_emg,event,event_type = '')
    df_emg_subphase = event_2_segmentation(filtered_emg,event_line_emg )

    # BMu
    df_emg_subphase['value'] = df_emg_subphase['emg'].apply(lambda x: np.nanmean(x) )

    detailed_output = df_emg_subphase[['value','event','rep']]
    output_rel_list = df_emg_subphase.groupby('rep').mean().values
    output =  np.mean(output_rel_list)
    output_rel = pd.DataFrame({'Measure':[f'M{i+1}' for i in range(len(output_rel_list.flatten()))],'Value':output_rel_list.flatten()})  

    return output, detailed_output, output_rel



###################
# DU et al., 2018 #
###################



def preprocessing_du_2018(emg):
    assert not empty_value_check(emg), 'Empty values emg preprocessing'
    filtered_emg = notch_filter( butterfilter_bandpass_dual(emg,4,10,499) , f0=50 )
    return filtered_emg



def BMu_Du_2018_1(emg,event, sub_MVIC = None ,sub_MVIC_event = None, show = False,**kwargs):

    try:
        nb_BM = kwargs['nb_BM']
    except:
        raise Exception('Missing nb_BM')
    
    assert not empty_value_check(emg), 'Empty emg values'

    # Preprocessing of the signal
    filtered_emg = preprocessing_du_2018(emg)

        
    # Event detection
    event_line_emg = extract_event(filtered_emg,event,event_type = '' )

    # Normalization to the maximum value
    emg_norm = normalization_peak(filtered_emg)

    # Segmentation of the signal according to event
    df_emg_subphase = event_2_segmentation(emg_norm, event_line_emg)

    ## BM computation for each subphase => output detailed
    # median
    if nb_BM in [29,40]:
        df_emg_subphase['value'] = df_emg_subphase['emg'].apply(lambda x: median_frequency(x)) 

    # mean
    elif nb_BM in [36]:
        df_emg_subphase['value'] = df_emg_subphase['emg'].apply(lambda x: np.mean(abs(x))) 

    # mean signal power frequency
    elif nb_BM in [37,38]:
        df_emg_subphase['value'] = df_emg_subphase['emg'].apply(lambda x: mean_power_frequency(x)) 

    # signal sample entropy:
    elif nb_BM in [41]: #take to much time
        output_detailed = pd.DataFrame(data=[[0,0,0]], columns=['value','event','rep'])
        # df_emg_subphase['value'] = df_emg_subphase['emg'].apply(lambda x: sample_entropy(x)) [to save some time for the computation the Samp Ent has not been computed for each subphase]



    # ## BM computation across the whole signal => output
    # # median 
    # if nb_BM in [29,40]:
    #     output = median_frequency(emg_norm)
        
    # # mean
    # elif nb_BM in [36]:
    #     output = np.mean(abs(emg_norm))

    # #mean signal power frequency
    # elif nb_BM in [37,38]:
    #     output = mean_power_frequency(emg_norm)

    # # signal sample entropy:
    # elif nb_BM in [41]:
    #     output = sample_entropy(emg_norm)

    
    ## BMU computation for each repetition => output_rel, mean of repetition => output
    value_list = []
    df_group = df_emg_subphase.groupby('rep')

    for i in range(len(df_group)):
        df_i = df_group.get_group(i+1).reset_index()
        if len(df_i)==2:
            # concatenation of the 2 subparts of the task
            emg_b_r = np.hstack((df_i.loc[0,'emg'],df_i.loc[1,'emg']))

            # median 
            if nb_BM in [29,40]:
                value = median_frequency(emg_b_r)
            
            # mean
            elif nb_BM in [36]:
                value = np.mean(abs(emg_b_r))

            #mean signal power frequency
            elif nb_BM in [37,38]:
                value = mean_power_frequency(emg_b_r)

            # signal sample entropy
            elif nb_BM in [41]:
                value = sample_entropy(emg_b_r)

            value_list.append(value)
    
    output = np.mean(value_list)
    output_rel = pd.DataFrame({'Measure':[f'M{i+1}' for i in range(len(value_list))],'Value':value_list})
    


    if nb_BM != 41:
        output_detailed = df_emg_subphase[['value','event','rep']]

    return output, output_detailed,output_rel





def BMu_Du_2018_1_norm(emg,event, sub_MVIC = None ,sub_MVIC_event = None, show = False,**kwargs):

    try:
        nb_BM = kwargs['nb_BM']
    except:
        raise Exception('Missing nb_BM')
    
    assert not empty_value_check(emg), 'Empty emg values'

    # Preprocessing of the signal
    filtered_emg = preprocessing_du_2018(emg)

        
    # Event detection
    event_line_emg = extract_event(filtered_emg,event,event_type = '' )

    # Normalization to the maximum value subMVIC
    # emg_norm = normalization_peak(filtered_emg)

    # emg_norm = normalization_peak(filtered_emg)
    if nb_BM in [37,40,41,42,52]:task_name = 'Endurance_Ito'
    else: task_name = 'Endurance_Sorensen'

    event_line_mvc = extract_event(sub_MVIC,sub_MVIC_event,event_type = 'MVC', task_name = task_name)
    filtered_sub_MVIC = full_wave(preprocessing_du_2018(sub_MVIC[event_line_mvc == 1]))
    filtered_sub_MVIC_max = np.max(filtered_sub_MVIC)
    emg_norm = normalization(filtered_emg,filtered_sub_MVIC_max)

    # Segmentation of the signal according to event
    df_emg_subphase = event_2_segmentation(emg_norm, event_line_emg)

    ## BM computation for each subphase => output detailed
    # median
    if nb_BM in [29,40]:
        df_emg_subphase['value'] = df_emg_subphase['emg'].apply(lambda x: median_frequency(x)) 

    # mean
    elif nb_BM in [36]:
        df_emg_subphase['value'] = df_emg_subphase['emg'].apply(lambda x: np.mean(abs(x))) 

    # mean signal power frequency
    elif nb_BM in [37,38]:
        df_emg_subphase['value'] = df_emg_subphase['emg'].apply(lambda x: mean_power_frequency(x)) 

    # signal sample entropy:
    elif nb_BM in [41]: #take to much time
        output_detailed = pd.DataFrame(data=[[0,0,0]], columns=['value','event','rep'])
        # df_emg_subphase['value'] = df_emg_subphase['emg'].apply(lambda x: sample_entropy(x)) [to save some time for the computation the Samp Ent has not been computed for each subphase]

    
    ## BMU computation for each repetition => output_rel, mean of repetition => output
    value_list = []
    df_group = df_emg_subphase.groupby('rep')

    for i in range(len(df_group)):
        df_i = df_group.get_group(i+1).reset_index()
        if len(df_i)==2:
            # concatenation of the 2 subparts of the task
            emg_b_r = np.hstack((df_i.loc[0,'emg'],df_i.loc[1,'emg']))

            # median 
            if nb_BM in [29,40]:
                value = median_frequency(emg_b_r)
            
            # mean
            elif nb_BM in [36]:
                value = np.mean(abs(emg_b_r))

            #mean signal power frequency
            elif nb_BM in [37,38]:
                value = mean_power_frequency(emg_b_r)

            # signal sample entropy
            elif nb_BM in [41]:
                value = sample_entropy(emg_b_r)

            value_list.append(value)
    
    output = np.mean(value_list)
    output_rel = pd.DataFrame({'Measure':[f'M{i+1}' for i in range(len(value_list))],'Value':value_list})
    


    if nb_BM != 41:
        output_detailed = df_emg_subphase[['value','event','rep']]

    return output, output_detailed,output_rel



def BMu29(emg,event,sub_MVIC,sub_MVIC_event, show = False,**kwargs):
    if 'nb_BM' not in list(kwargs.keys()):
        kwargs['nb_BM']=29
    return BMu_Du_2018_1(emg,event,sub_MVIC,sub_MVIC_event, show = False, **kwargs)

def BMu36_Du(emg,event,sub_MVIC,sub_MVIC_event, show = False,**kwargs):
    if 'nb_BM' not in list(kwargs.keys()):
        kwargs['nb_BM']=36
    return BMu_Du_2018_1(emg,event,sub_MVIC,sub_MVIC_event, show = False, **kwargs)

def BMu37(emg,event,sub_MVIC,sub_MVIC_event, show = False,**kwargs):
    if 'nb_BM' not in list(kwargs.keys()):
        kwargs['nb_BM']=37
    return BMu_Du_2018_1(emg,event,sub_MVIC,sub_MVIC_event, show = False, **kwargs)

def BMu38(emg,event,sub_MVIC,sub_MVIC_event, show = False,**kwargs):
    if 'nb_BM' not in list(kwargs.keys()):
        kwargs['nb_BM']=38
    return BMu_Du_2018_1(emg,event,sub_MVIC,sub_MVIC_event, show = False, **kwargs)

def BMu40(emg,event,sub_MVIC,sub_MVIC_event, show = False,**kwargs):
    if 'nb_BM' not in list(kwargs.keys()):
        kwargs['nb_BM']=40
    return BMu_Du_2018_1(emg,event,sub_MVIC,sub_MVIC_event, show = False, **kwargs)

def BMu41(emg,event,sub_MVIC,sub_MVIC_event, show = False,**kwargs):
    if 'nb_BM' not in list(kwargs.keys()):
        kwargs['nb_BM']=41
    return BMu_Du_2018_1(emg,event,sub_MVIC,sub_MVIC_event, show = False, **kwargs)




def BMu29_norm(emg,event,sub_MVIC,sub_MVIC_event, show = False,**kwargs):
    if 'nb_BM' not in list(kwargs.keys()):
        kwargs['nb_BM']=29
    return BMu_Du_2018_1_norm(emg,event,sub_MVIC,sub_MVIC_event, show = False, **kwargs)

def BMu36_Du_norm(emg,event,sub_MVIC,sub_MVIC_event, show = False,**kwargs):
    if 'nb_BM' not in list(kwargs.keys()):
        kwargs['nb_BM']=36
    return BMu_Du_2018_1_norm(emg,event,sub_MVIC,sub_MVIC_event, show = False, **kwargs)

def BMu37_norm(emg,event,sub_MVIC,sub_MVIC_event, show = False,**kwargs):
    if 'nb_BM' not in list(kwargs.keys()):
        kwargs['nb_BM']=37
    return BMu_Du_2018_1_norm(emg,event,sub_MVIC,sub_MVIC_event, show = False, **kwargs)

def BMu38_norm(emg,event,sub_MVIC,sub_MVIC_event, show = False,**kwargs):
    if 'nb_BM' not in list(kwargs.keys()):
        kwargs['nb_BM']=38
    return BMu_Du_2018_1_norm(emg,event,sub_MVIC,sub_MVIC_event, show = False, **kwargs)

def BMu40_norm(emg,event,sub_MVIC,sub_MVIC_event, show = False,**kwargs):
    if 'nb_BM' not in list(kwargs.keys()):
        kwargs['nb_BM']=40
    return BMu_Du_2018_1_norm(emg,event,sub_MVIC,sub_MVIC_event, show = False, **kwargs)

def BMu41_norm(emg,event,sub_MVIC,sub_MVIC_event, show = False,**kwargs):
    if 'nb_BM' not in list(kwargs.keys()):
        kwargs['nb_BM']=41
    return BMu_Du_2018_1_norm(emg,event,sub_MVIC,sub_MVIC_event, show = False, **kwargs)


#51 52 53 34 lateral bending

def BMu_Du_2018_2(emg,event, sub_MVIC = None ,sub_MVIC_event = None, show = False,**kwargs):


    try:
        nb_BM = kwargs['nb_BM']
    except:
        raise Exception('Missing nb_BM')
    
    try:
        muscle= kwargs['muscle']
    except:
        raise Exception('Missing muscle')
    
    try:
        ini_rel= kwargs['session']
        subject= kwargs['subject']

    except:
        raise Exception('Missing informations: subject or session')


    kinematic = pd.read_csv(f"Data_NSLBP_VS_final/NSLBP-BIO-0{subject:02d}/{ini_rel}/output/Trunk_Lateral 1_kine.csv")

    #choose the correct axis: the objective is to find the frontal axis to deduce, thank to marker placed on the head of subject, on which side the subject is laterally bending
    diff = []
    for i in range(4):
        # markers placed bilateraly on the hips are chosen as references, their x,y,z coordinates are retrieved
        # the markes should have a wider distance separating them along the frontal/"left right" axis that is how the correct axis is chosen
        diff.append( np.abs(np.mean(kinematic[f'axis_ref_r{i}'][:200]) - np.mean(kinematic[f'axis_ref_l{i}'][:200])) )
    axis = np.argmax(diff)

    if len(kinematic[f'axis_{axis}']) != len(emg): # because kinematic data's sampling rate is 100 Hz whereas EMG's sampling rate is 1000 Hz, interpolation is needed
        kinematic_inter = kinematic_interpolate(kinematic[f'axis_{axis}'],emg)
    else:
        kinematic_inter = kinematic[f'axis_{axis}']


    # Preprocessing of the signal
    filtered_emg = preprocessing_du_2018(emg)

    # Event detection
    event_line_emg = extract_event(filtered_emg,event,event_type = '' )

    emg_norm = normalization_peak(filtered_emg)

    df_emg_subphase = event_2_segmentation(emg_norm, event_line_emg)
    df_kin_subphase =  event_2_segmentation(kinematic_inter, event_line_emg)
    
    ## Computation of the BMu for each subphase => detailed output
    # median
    if nb_BM in [53]:
        df_emg_subphase['value'] = df_emg_subphase['emg'].apply(lambda x: median_frequency(x)) 

    # mean signal power frequency
    elif nb_BM in [51,52]:
        df_emg_subphase['value'] = df_emg_subphase['emg'].apply(lambda x:mean_power_frequency(x)) 

    elif nb_BM in [34]:
        df_emg_subphase['value'] = df_emg_subphase['emg'].apply(lambda x: np.mean(abs(x))) 


    value_list = []
    event_list = []

    df_group = df_emg_subphase.groupby('rep')
    df_kin_group = df_kin_subphase.groupby('rep')

    # for each repetition, ie for each side bending the BMu is computed => output rel, the final BMu values is the mean of the computed parameters => output

    for i in range(len(df_group)):
        # get the groupe
        df_i = df_group.get_group(i+1).reset_index()
        df_kin_i = df_kin_group.get_group(i+1).reset_index()

        # if there are a 'bending' and a 'return'
        if len(df_i)==2:

            # merge beding and return
            emg_b_r = np.hstack((df_i.loc[0,'emg'],df_i.loc[1,'emg']))

            # compute the baseline of kinematic values + average value over bending
            avrg_baseline = df_kin_i.loc[0,'emg'][0:int(SAMPLING_RATE*1.5)].mean()
            avrg_subpart = df_kin_i.loc[0,'emg'].mean()

            # check if it is the ipsilateral side: if baseline > average then the patient is bending on the left side
            if (avrg_baseline >= avrg_subpart and muscle[0]=='L' )|(avrg_baseline <= avrg_subpart and muscle[0]=='R' ):

                event_list += ['ipsi bending','ipsi return']  

                # median
                if nb_BM in [53]:
                    value = median_frequency(emg_b_r)

                # mean signal power frequency
                elif nb_BM in [51,52]:
                    value = mean_power_frequency(emg_b_r)
                
                # elif nb_BM in [34]:
                #     value = np.nanmean(abs(emg_b_r))

                if nb_BM != 34:
                    value_list.append(value) # only value computed during ipsilateral bending are kept for the computation of the output
            
            else:
                event_list += ['contra bending','contra return']  

    if nb_BM == 34 :
        output = df_emg_subphase['value'].mean()
        # retrieve average of bending and return values from the ipsilateral side and contralateral side separately 
        ipsi_subphase = df_emg_subphase.loc[df_emg_subphase['rep']%2 ==1].groupby('rep').mean().values
        contra_subphase = df_emg_subphase.loc[df_emg_subphase['rep']%2 ==0].groupby('rep').mean().values
        # take the average of each repetition: that is to say bending and return side for both side
        max_serie = min(len(ipsi_subphase),len(contra_subphase))
        average_rep_2_side = (ipsi_subphase[:max_serie]+contra_subphase[:max_serie])/2
        output_rel = pd.DataFrame({'Measure':[f'M{i+1}' for i in range(len(average_rep_2_side))],'Value':average_rep_2_side.flatten()})


    else:
        output = np.mean(value_list)
        output_rel = pd.DataFrame({'Measure':[f'M{i+1}' for i in range(len(value_list))],'Value':value_list})
        
    output_detailed = df_emg_subphase[['value','event','rep']]        
    output_detailed['event'] = event_list

    return output,output_detailed,output_rel 




def BMu_Du_2018_2_norm(emg,event, sub_MVIC = None ,sub_MVIC_event = None, show = False,**kwargs):


    try:
        nb_BM = kwargs['nb_BM']
    except:
        raise Exception('Missing nb_BM')
    
    try:
        muscle= kwargs['muscle']
    except:
        raise Exception('Missing muscle')
    
    try:
        ini_rel= kwargs['session']
        subject= kwargs['subject']

    except:
        raise Exception('Missing informations: subject or session')


    kinematic = pd.read_csv(f"Data_NSLBP_VS_final/NSLBP-BIO-0{subject:02d}/{ini_rel}/output/Trunk_Lateral 1_kine.csv")

    #choose the correct axis: the objective is to find the frontal axis to deduce, thank to marker placed on the head of subject, on which side the subject is laterally bending
    diff = []
    for i in range(4):
        # markers placed bilateraly on the hips are chosen as references, their x,y,z coordinates are retrieved
        # the markes should have a wider distance separating them along the frontal/"left right" axis that is how the correct axis is chosen
        diff.append( np.abs(np.mean(kinematic[f'axis_ref_r{i}'][:200]) - np.mean(kinematic[f'axis_ref_l{i}'][:200])) )
    axis = np.argmax(diff)

    if len(kinematic[f'axis_{axis}']) != len(emg): # because kinematic data's sampling rate is 100 Hz whereas EMG's sampling rate is 1000 Hz, interpolation is needed
        kinematic_inter = kinematic_interpolate(kinematic[f'axis_{axis}'],emg)
    else:
        kinematic_inter = kinematic[f'axis_{axis}']


    # Preprocessing of the signal
    filtered_emg = preprocessing_du_2018(emg)

    # Event detection
    event_line_emg = extract_event(filtered_emg,event,event_type = '' )

    if nb_BM in [37,40,41,42,52]:task_name = 'Endurance_Ito'
    else: task_name = 'Endurance_Sorensen'

    event_line_mvc = extract_event(sub_MVIC,sub_MVIC_event,event_type = 'MVC', task_name = task_name)
    filtered_sub_MVIC = full_wave(preprocessing_du_2018(sub_MVIC[event_line_mvc == 1]))
    filtered_sub_MVIC_max = np.max(filtered_sub_MVIC)
    emg_norm = normalization(filtered_emg,filtered_sub_MVIC_max)

    df_emg_subphase = event_2_segmentation(emg_norm, event_line_emg)
    df_kin_subphase =  event_2_segmentation(kinematic_inter, event_line_emg)
    
    ## Computation of the BMu for each subphase => detailed output
    # median
    if nb_BM in [53]:
        df_emg_subphase['value'] = df_emg_subphase['emg'].apply(lambda x: median_frequency(x)) 

    # mean signal power frequency
    elif nb_BM in [51,52]:
        df_emg_subphase['value'] = df_emg_subphase['emg'].apply(lambda x:mean_power_frequency(x)) 

    elif nb_BM in [34]:
        df_emg_subphase['value'] = df_emg_subphase['emg'].apply(lambda x: np.mean(abs(x))) 


    value_list = []
    event_list = []

    df_group = df_emg_subphase.groupby('rep')
    df_kin_group = df_kin_subphase.groupby('rep')

    # for each repetition, ie for each side bending the BMu is computed => output rel, the final BMu values is the mean of the computed parameters => output

    for i in range(len(df_group)):
        # get the groupe
        df_i = df_group.get_group(i+1).reset_index()
        df_kin_i = df_kin_group.get_group(i+1).reset_index()

        # if there are a 'bending' and a 'return'
        if len(df_i)==2:

            # merge beding and return
            emg_b_r = np.hstack((df_i.loc[0,'emg'],df_i.loc[1,'emg']))

            # compute the baseline of kinematic values + average value over bending
            avrg_baseline = df_kin_i.loc[0,'emg'][0:int(SAMPLING_RATE*1.5)].mean()
            avrg_subpart = df_kin_i.loc[0,'emg'].mean()

            # check if it is the ipsilateral side: if baseline > average then the patient is bending on the left side
            if (avrg_baseline >= avrg_subpart and muscle[0]=='L' )|(avrg_baseline <= avrg_subpart and muscle[0]=='R' ):

                event_list += ['ipsi bending','ipsi return']  

                # median
                if nb_BM in [53]:
                    value = median_frequency(emg_b_r)

                # mean signal power frequency
                elif nb_BM in [51,52]:
                    value = mean_power_frequency(emg_b_r)
                
                # elif nb_BM in [34]:
                #     value = np.nanmean(abs(emg_b_r))

                if nb_BM != 34:
                    value_list.append(value) # only value computed during ipsilateral bending are kept for the computation of the output
            
            else:
                event_list += ['contra bending','contra return']  

    if nb_BM == 34 :
        output = df_emg_subphase['value'].mean()
        # retrieve average of bending and return values from the ipsilateral side and contralateral side separately 
        ipsi_subphase = df_emg_subphase.loc[df_emg_subphase['rep']%2 ==1].groupby('rep').mean().values
        contra_subphase = df_emg_subphase.loc[df_emg_subphase['rep']%2 ==0].groupby('rep').mean().values
        # take the average of each repetition: that is to say bending and return side for both side
        max_serie = min(len(ipsi_subphase),len(contra_subphase))
        average_rep_2_side = (ipsi_subphase[:max_serie]+contra_subphase[:max_serie])/2
        output_rel = pd.DataFrame({'Measure':[f'M{i+1}' for i in range(len(average_rep_2_side))],'Value':average_rep_2_side.flatten()})


    else:
        output = np.mean(value_list)
        output_rel = pd.DataFrame({'Measure':[f'M{i+1}' for i in range(len(value_list))],'Value':value_list})
        
    output_detailed = df_emg_subphase[['value','event','rep']]        
    output_detailed['event'] = event_list

    return output,output_detailed,output_rel 




def BMu51(emg,event,sub_MVIC,sub_MVIC_event, show = False,**kwargs):
    return BMu_Du_2018_2(emg,event,sub_MVIC,sub_MVIC_event, show = False, **kwargs)

def BMu52(emg,event,sub_MVIC,sub_MVIC_event, show = False,**kwargs):
    return BMu_Du_2018_2(emg,event,sub_MVIC,sub_MVIC_event, show = False, **kwargs)

def BMu53(emg,event,sub_MVIC,sub_MVIC_event, show = False,**kwargs):
    return BMu_Du_2018_2(emg,event,sub_MVIC,sub_MVIC_event, show = False, **kwargs)
    
def BMu34(emg,event,sub_MVIC,sub_MVIC_event, show = False,**kwargs):
    if 'nb_BM' not in list(kwargs.keys()):
        kwargs['nb_BM']=34
    return BMu_Du_2018_2(emg,event,sub_MVIC,sub_MVIC_event, show = False, **kwargs)




def BMu51_norm(emg,event,sub_MVIC,sub_MVIC_event, show = False,**kwargs):
    return BMu_Du_2018_2_norm(emg,event,sub_MVIC,sub_MVIC_event, show = False, **kwargs)

def BMu52_norm(emg,event,sub_MVIC,sub_MVIC_event, show = False,**kwargs):
    return BMu_Du_2018_2_norm(emg,event,sub_MVIC,sub_MVIC_event, show = False, **kwargs)

def BMu53_norm(emg,event,sub_MVIC,sub_MVIC_event, show = False,**kwargs):
    return BMu_Du_2018_2_norm(emg,event,sub_MVIC,sub_MVIC_event, show = False, **kwargs)
    
def BMu34_norm(emg,event,sub_MVIC,sub_MVIC_event, show = False,**kwargs):
    if 'nb_BM' not in list(kwargs.keys()):
        kwargs['nb_BM']=34
    return BMu_Du_2018_2_norm(emg,event,sub_MVIC,sub_MVIC_event, show = False, **kwargs)








#######################
# PAKZAD et al., 2016 #
#######################


def preprocessing_pakdaz_2016(emg):
    assert not empty_value_check(emg), 'Empty value for preprocessing'
    filtered_emg = butterfilter_dual( full_wave( mean_removal(  butterfilter_dual(emg, 2, 30, type ='highpass'))) , order = 1, c_f = 2.5, type = 'lowpass')
    return filtered_emg



# def resampling_pakdaz(df_emg):
def resampling_pakdaz(emg):
    """EMG data for each sub-phase of gait are re-sampled to 100 data points, using a cubic-spline interpolation.

    Args:
        df_emg  (dataframe): (columns: emg, event, repetition ) each row comprises the emg segment of the corresponding subphase

    Returns:
        df_emg  (dataframe): (columns: emg, event, repetition ) each row comprises the emg signal resampled  of the segment corresponding subphase
    """

    x_sample = np.arange(0,len(emg))
    x_new = np.arange(0,len(emg),10)
    cs = interpolate.CubicSpline(x_sample,emg,bc_type='natural')

    return cs(x_new)





def BMu_pakzad(emg,event,sub_MVIC,sub_MVIC_event, show = False,**kwargs):

    # The computation of Pakzad's BMu required indications about the analysed muscle for lateral Trunk flexion
    try :
        nb_BM = kwargs['nb_BM']
    except:
        raise Exception('Missing nb_BM')

    if nb_BM in [110,113]:
        subphase = 'all'
    elif nb_BM in [111,112]:
        subphase = 'contralateral swing'
    
    if nb_BM in [111,112]:
        try:
            muscle = kwargs['muscle']
        except:
            raise Exception('Missing muscle name')
            
    
    assert not empty_value_check(emg), 'Empty emg values'
    assert not empty_value_check(sub_MVIC), 'Empty sub_MVIC emg values'

    # Preprocessing of the signal
    filtered_emg = preprocessing_pakdaz_2016(emg)

    # Event detection emg
    event_line_emg= extract_event(filtered_emg,event,event_type='Gait')

    # # MVC computation: event extraction, preprocessing, mean
    # event_line_mvc = extract_event(sub_MVIC,sub_MVIC_event,event_type = 'MVC')
    # filtered_sub_MVIC = preprocessing_pakdaz_2016(sub_MVIC[event_line_mvc == 1])
    # filtered_sub_MVIC_mean = np.mean(filtered_sub_MVIC)

    # MVC computation with visually inspected event: event extraction, preprocessing, mean
    if nb_BM in [110,111]:
        task_mvic = 'Endurance_Ito'
    elif nb_BM in [112,113]:
        task_mvic = 'Endurance_Sorensen'
       
    event_line_mvc = extract_event(sub_MVIC,sub_MVIC_event,event_type = 'MVC', task_name = task_mvic)
    filtered_sub_MVIC = preprocessing_pakdaz_2016(sub_MVIC[event_line_mvc == 1])
    filtered_sub_MVIC_mean = np.mean(filtered_sub_MVIC)

    # Normalization
    emg_norm = normalization(filtered_emg,filtered_sub_MVIC_mean)

    # Event segmentation
    df_emg_subphase = event_gait_segmentation(emg_norm, event_line_emg )


    ## BM computation
    # Mean for each emg segments
    df_emg_subphase['resamp'] = df_emg_subphase['emg'].transform(lambda x: resampling_pakdaz(x))
    df_emg_subphase['value'] = df_emg_subphase['resamp'].transform(lambda x: np.nanmean(x))



    # BMu : mean of all subphase
    output_detailed = df_emg_subphase[['value','event','rep']]

    if subphase == 'all':
        output = output_detailed['value'].mean()
        output_rel_list = output_detailed.groupby('rep')['value'].mean().values
        output_rel = pd.DataFrame({'Measure':[f'M{i+1}' for i in range(len(output_rel_list))],'Value':output_rel_list})

    else:
        if subphase == 'contralateral swing':
            if muscle[0]=='L': val = 'RTO'  
            else : val = 'LTO'
            output = output_detailed.loc[output_detailed['event'] == val ]['value'].mean()
            output_rel_list = output_detailed.loc[output_detailed['event'] == val ]['value'].values
            output_rel = pd.DataFrame({'Measure':[f'M{i+1}' for i in range(len(output_rel_list))],'Value':output_rel_list})

            # output = output_detailed.drop('rep',axis=1)[output_detailed['event'] == val   ].groupby('event').mean().reset_index()['value'][0]

        elif subphase == 'ipsilateral swing':
            if muscle[0]=='L': val = 'LTO'  
            else : val = 'RTO'
            output = output_detailed.loc[output_detailed['event'] == val ]['value'].mean()
            output_rel_list = output_detailed.loc[output_detailed['event'] == val ]['value'].values
            output_rel = pd.DataFrame({'Measure':[f'M{i+1}' for i in range(len(output_rel_list))],'Value':output_rel_list})

        elif subphase == 'double stance':
            output = output_detailed.loc[(output_detailed['event'] == 'LHS') | (output_detailed['event'] == 'RHS') ]['value'].mean()
            output_rel_list = output_detailed.loc[(output_detailed['event'] == 'LHS') | (output_detailed['event'] == 'RHS') ]['value'].values
            output_rel = pd.DataFrame({'Measure':[f'M{i+1}' for i in range(len(output_rel_list))],'Value':output_rel_list})

        elif subphase == 'first double stance': # ! assumption all gait start with double stance in their events
            output = output_detailed.loc[(output_detailed['event'] == output_detailed['event'][0]) ]['value'][0]
            output_rel_list = output_detailed.loc[(output_detailed['event'] == output_detailed['event'][0]) ]['value'].values
            output_rel = pd.DataFrame({'Measure':[f'M{i+1}' for i in range(len(output_rel_list))],'Value':output_rel_list})

    return output, output_detailed, output_rel





def BMu110(emg,event,sub_MVIC,sub_MVIC_event, show = False,**kwargs):
    return BMu_pakzad( emg,event,sub_MVIC,sub_MVIC_event, show = False, **{'nb_BM':110})

def BMu111(emg,event,sub_MVIC,sub_MVIC_event, show = False,**kwargs):
    if 'nb_BM' not in list(kwargs.keys()):
        kwargs['nb_BM']=111
    return BMu_pakzad(emg,event,sub_MVIC,sub_MVIC_event, show = False, **kwargs)

def BMu112(emg,event,sub_MVIC,sub_MVIC_event, show = False,**kwargs):
    if 'nb_BM' not in list(kwargs.keys()):
        kwargs['nb_BM']=112
    return BMu_pakzad(emg,event,sub_MVIC,sub_MVIC_event, show = False, **kwargs)

def BMu113(emg,event,sub_MVIC,sub_MVIC_event, show = False,**kwargs):
    return BMu_pakzad(emg,event,sub_MVIC,sub_MVIC_event, show = False, **{'nb_BM':113})

















#######################
# Ansari et al., 2018 #
#######################


def preprocessing_ansari_2018(emg):
    filtered_emg = butterfilter_bandpass_dual (mean_removal(emg), order = 2, c_f_low=50, c_f_high=499)
    return filtered_emg


def BMu_Ansari(emg,event,sub_MVIC,sub_MVIC_event, show = False,**kwargs):

         
    assert not empty_value_check(emg), 'Empty emg values'
    assert not empty_value_check(sub_MVIC), 'Empty sub_MVIC emg values'

    # Preprocessing of the signal
    filtered_emg = preprocessing_ansari_2018(emg)

    # Event detection emg
    event_line_emg= extract_event(filtered_emg,event,event_type='Gait')

    # MVC computation: event extraction, preprocessing, mean
    event_line_mvc = extract_event(sub_MVIC,sub_MVIC_event,event_type = 'MVC',task_name='Endurance_Sorensen')
    filtered_sub_MVIC = preprocessing_ansari_2018(sub_MVIC[event_line_mvc == 1])
    filtered_sub_MVIC_rms = rms(filtered_sub_MVIC)

    # Event segmentation
    df_emg_subphase = event_gait_segmentation(filtered_emg, event_line_emg )


    ## BM computation
    # Mean for each emg segments
    df_emg_subphase['value'] = df_emg_subphase['emg'].transform(lambda x: rms(x)/filtered_sub_MVIC_rms)
    output_rel_list = df_emg_subphase.groupby('rep')['value'].mean().values
    output_rel = pd.DataFrame({'Measure':[f'M{i+1}' for i in range(len(output_rel_list))],'Value':output_rel_list})
    output = np.nanmean(output_rel_list)
    output_detailed = df_emg_subphase[['value','event','rep']]

    return output, output_detailed, output_rel


def BMu100_ans(emg,event,sub_MVIC,sub_MVIC_event, show = False,**kwargs):
    return BMu_Ansari(emg,event,sub_MVIC,sub_MVIC_event, show = False, **kwargs)

def BMu101_ans(emg,event,sub_MVIC,sub_MVIC_event, show = False,**kwargs):
    return BMu_Ansari(emg,event,sub_MVIC,sub_MVIC_event, show = False, **kwargs)


##########
# BMu 96 #
##########

#####################
# Lima et al., 2018 #  
#####################







##########################
#Courbalay et al., 2017 #
##########################


def preprocessing_courbalay_2017(emg):
    assert not empty_value_check(emg), 'Empty value for preprocessing'

    filtered_emg = mean_removal(butterfilter_bandpass_dual(emg,2,10,450))

    return filtered_emg




def BMu96_Courbalay(emg,event, sub_MVIC = True ,sub_MVIC_event = None, show = False,**kwargs):

    assert not empty_value_check(emg), 'Empty emg values'
    assert not empty_value_check(sub_MVIC), 'Empty sub_MVIC emg values'

    # Preprocessing of the signal
    filtered_emg = preprocessing_courbalay_2017(emg)

    # Event detection
    event_line_emg= extract_event(filtered_emg,event,event_type='')

    # Event segmentation
    df_emg_subphase = event_2_segmentation(filtered_emg, event_line_emg )

    # Compute reference RMS value
    # MVC computation: event extraction, preprocessing
    event_line_mvc = extract_event(sub_MVIC,sub_MVIC_event,event_type = 'MVC')
    filtered_sub_MVIC = preprocessing_courbalay_2017(sub_MVIC[event_line_mvc == 1])
    filtered_sub_MVIC_rms = np.nanmean(rms(filtered_sub_MVIC))


    # selection of the subparts where the patient is holding the load ie Lowering anf Lifting, not reaching for the load nor returning from lowering the load
    df_output_subphase = df_emg_subphase.loc[((df_emg_subphase['rep']%2 == 0) & (df_emg_subphase['event'] == 'bending') )| ((df_emg_subphase['rep']%2 == 1) & (df_emg_subphase['event'] == 'return')),: ].reset_index().drop('index',axis=1)


    ## BM computation
    # slinding rms
    df_output_subphase['value'] = df_output_subphase['emg'].transform(lambda x: np.nanmean(rolling_function(x,win=0.25,func= rms,unit='sec'))/filtered_sub_MVIC_rms)

    # # RMS around (250 ms window) the peak of subphase's movement for each emg segments
    # df_emg_subphase['value'] = df_emg_subphase['emg'].transform(lambda x: rms( x[np.max( [0,  np.argmax(x)-int((0.250*SAMPLING_RATE)//2)] )  :   np.min( [len(x)-1, np.argmax(x)+int((0.250*SAMPLING_RATE)//2)] ) ] ))

    # # Mormalize value
    # df_emg_subphase['value'] = df_emg_subphase['value'].transform(lambda x: x/filtered_sub_MVIC_rms)

    # Mean of rms for each subphase
    output_rel_list = df_output_subphase.loc[df_output_subphase['event'] == 'bending','value'].values
    output_rel = pd.DataFrame({'Measure':[f'M{i+1}' for i in range(len(output_rel_list))],'Value':output_rel_list})
    output = np.nanmean(output_rel_list)

    # BMu : mean of all subphase
    output_detailed = df_output_subphase[['value','event','rep']]



    return output, output_detailed,output_rel





##########################
# Larivière et al., 2002 #
##########################


def preprocessing_lariviere_2002(emg):
    assert not empty_value_check(emg), 'Empty value for preprocessing'

    filtered_emg = butterfilter_dual( full_wave( mean_removal( butterfilter_bandpass_dual(emg,4,25,499) )), order = 1, c_f = 2, type = 'lowpass' )

    return filtered_emg




def BMu96_Lariviere(emg,event, sub_MVIC = None ,sub_MVIC_event = None, show = False,**kwargs):

    assert not empty_value_check(emg), 'Empty emg values'

    # Preprocessing of the signal
    filtered_emg = preprocessing_lariviere_2002(emg)

    # Event detection emg
    event_line_emg= extract_event(filtered_emg,event,event_type='')

    # Event segmentation
    df_emg_subphase = event_2_segmentation(filtered_emg, event_line_emg )


    ## BM computation
    # selection of the subparts where the patient is holding the load ie Lowering anf Lifting, not reaching for the load nor returning from lowering the load
    df_output_subphase = df_emg_subphase.loc[((df_emg_subphase['rep']%2 == 0) & (df_emg_subphase['event'] == 'bending') )| ((df_emg_subphase['rep']%2 == 1) & (df_emg_subphase['event'] == 'return')),: ].reset_index().drop('index',axis=1)

    # Max for each emg segments
    df_output_subphase['value'] = df_output_subphase['emg'].transform(lambda x: np.nanmax(x))

    # BMu = max during lowering
    output_rel_list = df_output_subphase.loc[df_output_subphase['event'] == 'bending','value'].values
    output_rel = pd.DataFrame({'Measure':[f'M{i+1}' for i in range(len(output_rel_list))],'Value':output_rel_list})
    output = np.nanmean(output_rel_list)


    # BMu : mean of all subphase
    output_detailed = df_output_subphase[['value','event','rep']]

    # # Mean of max for each subphase
    # results_sub = df_emg_subphase.groupby('event').mean()


    return output, output_detailed, output_rel















######################
# Lamoth et al.,2006 #
######################



def preprocessing_lamoth_2006(emg):
    filtered_emg =  butterfilter_dual( hilbert_rectification(emg), order=4,c_f = 20, type='lowpass' )
    return filtered_emg


def BMu109(emg,event, sub_MVIC = None ,sub_MVIC_event = None, show = False,**kwargs):



    assert not empty_value_check(emg), 'Empty emg values'

    # Preprocessing of the signal
    filtered_emg = preprocessing_lamoth_2006(emg)
    

    # Event detection emg
    # time normalization with regard to the stride time
    df_emg_event_norm = time_emg_event_normalization(filtered_emg,event,shift=0)
    output_detailed = pd.DataFrame(data=None, columns=['event','emg','stride'])

    # for each stride compute BMu
    df_grouped = df_emg_event_norm.groupby('stride')
    for i in range(1,len(df_grouped )+1): 
        
        df = df_grouped.get_group(i)
        # normalize emg
        mean_stride = np.mean(df['emg'])
        df['emg'] = normalization(df['emg'],mean_stride)
        
        # computation of the mean EMG of each subphase
        df['event'] = df['event'].fillna(method="ffill").values
        values = df.groupby('event').mean().reset_index().drop('frame',axis=1)
        output_detailed = pd.concat([output_detailed,values])


    output_detailed = output_detailed.rename(columns={"stride": "rep", "emg": "value"})
    output_detailed = output_detailed[['value','event','rep']].reset_index().drop('index',axis=1)

    # if subphase == 'all':
    #     output = output_detailed.drop('rep',axis=1).groupby('event').mean().reset_index()['value'][0]

    # else:
    #     if subphase == 'contralateral swing':
    #         if muscle[0]=='L': val = 'RTO'  
    #         else : val = 'LTO'

    #     elif subphase == 'ipsilateral swing':
    #         if muscle[0]=='L': val = 'LTO'  
    #         else : val = 'RTO'

    output_rel_list = output_detailed.loc[(output_detailed['event'] == 'RTO') | (output_detailed['event'] == 'LTO'),:].groupby('rep').mean().values.flatten()
    output_rel = pd.DataFrame({'Measure':[f'M{i+1}' for i in range(len(output_rel_list))],'Value':output_rel_list})
    output = np.mean(output_rel['Value'])

    return output, output_detailed, output_rel


def BMu109_norm(emg,event, sub_MVIC = None ,sub_MVIC_event = None, show = False,**kwargs):


    assert not empty_value_check(emg), 'Empty emg values'

    # Preprocessing of the signal
    filtered_emg = preprocessing_lamoth_2006(emg)

    #Normalization subMVIC
    event_line_mvc = extract_event(sub_MVIC,sub_MVIC_event,event_type = 'MVC', task_name = 'Endurance_Sorensen')
    filtered_sub_MVIC = full_wave(preprocessing_lamoth_2006(sub_MVIC[event_line_mvc == 1]))
    filtered_sub_MVIC_mean = np.mean(filtered_sub_MVIC)
    

    # Event detection emg
    # time normalization with regard to the stride time
    df_emg_event_norm = time_emg_event_normalization(filtered_emg,event,shift=0)
    output_detailed = pd.DataFrame(data=None, columns=['event','emg','stride'])

    # for each stride compute BMu
    df_grouped = df_emg_event_norm.groupby('stride')
    for i in range(1,len(df_grouped )+1): 
        
        df = df_grouped.get_group(i)

        # # normalize emg
        # mean_stride = np.mean(df['emg'])
        # df['emg'] = normalization(df['emg'],mean_stride)

        # normalisation subMVIC
        df['emg'] = normalization(df['emg'],filtered_sub_MVIC_mean)

        
        
        # computation of the mean EMG of each subphase
        df['event'] = df['event'].fillna(method="ffill").values
        values = df.groupby('event').mean().reset_index().drop('frame',axis=1)
        output_detailed = pd.concat([output_detailed,values])


    output_detailed = output_detailed.rename(columns={"stride": "rep", "emg": "value"})
    output_detailed = output_detailed[['value','event','rep']].reset_index().drop('index',axis=1)

    # if subphase == 'all':
    #     output = output_detailed.drop('rep',axis=1).groupby('event').mean().reset_index()['value'][0]

    # else:
    #     if subphase == 'contralateral swing':
    #         if muscle[0]=='L': val = 'RTO'  
    #         else : val = 'LTO'

    #     elif subphase == 'ipsilateral swing':
    #         if muscle[0]=='L': val = 'LTO'  
    #         else : val = 'RTO'

    output_rel_list = output_detailed.loc[(output_detailed['event'] == 'RTO') | (output_detailed['event'] == 'LTO'),:].groupby('rep').mean().values.flatten()
    output_rel = pd.DataFrame({'Measure':[f'M{i+1}' for i in range(len(output_rel_list))],'Value':output_rel_list})
    output = np.mean(output_rel['Value'])

    return output, output_detailed, output_rel

























###############################
# Arendt-Nielsen et al., 1995 #
###############################

def preprocessing_arendt_nielsen_1995(emg):

    assert not empty_value_check(emg), 'preprocessing: empty values'
    filtered_emg = fir_filter( full_wave(mean_removal(emg)), c_f = 20, nb_taps=40, type='lowpass')
    return filtered_emg




def BMu_Arendt_Nielsen(emg,event, sub_MVIC = None ,sub_MVIC_event = None, show = False,**kwargs):

    # by default: the mean EMG of all subphases are pulled together to compute the BMu

    try :
        nb_BM = kwargs['nb_BM']
        if nb_BM in [105,107,106]:
            subphase = 'ipsilateral swing'
        elif nb_BM in [102,104,103]:
            subphase = 'contralateral swing'
        elif nb_BM in [108,100,101] :
            subphase = 'all'
        
        if nb_BM in [105,107,106,102,104,103]:
            try:
                muscle = kwargs['muscle']
            except:
                raise Exception('Missing muscle name')

    except:
        raise Exception('Missing BMu number')


    assert not empty_value_check(emg), 'Empty emg values'


    # Preprocessing of the signal
    filtered_emg = preprocessing_arendt_nielsen_1995(emg)

    ## Event detection emg
    # time normalization with regard to the stride time
    df_emg_event_norm = time_emg_event_normalization(filtered_emg,event,shift=0)
    output_detailed = pd.DataFrame(data=None, columns=['event','emg','stride'])

    
    df_grouped = df_emg_event_norm.groupby('stride')
    for i in range(1,len(df_grouped )+1): 
        df = df_grouped.get_group(i)
        #find peak
        df_ev = df.loc[:,('event')].fillna(method="ffill")
        peak = np.max(df[ (df_ev.values == 'RHS') |  (df_ev.values == 'LHS') ]['emg'])
        # normalize emg
        emg_norm = normalization(df.loc[:,('emg')],peak)
        df.loc[:,'emg'] = emg_norm

        
        if show:
            _ = extract_event_stride(df,show = show)

        df.loc[:,('event')] = df.loc[:,('event')].fillna(method="ffill").values

        #BMu = mean per subphases
        values = df.groupby('event').mean().reset_index()
        output_detailed = pd.concat([output_detailed,values])


    output_detailed = output_detailed.rename(columns={"stride": "rep", "emg": "value"})
    output_detailed = output_detailed[['value','event','rep']].reset_index().drop('index',axis=1)

    # BMu : mean of all subphase

    if subphase == 'all':
        # output = output_detailed['value'].mean()
        output_rel_list = output_detailed.groupby('rep')['value'].mean().values

    else:
        if subphase == 'contralateral swing':
            if muscle[0]=='L': val = 'RTO'  
            else : val = 'LTO'
            output_rel_list = output_detailed.loc[output_detailed['event'] == val]['value'].values

            # output = output_detailed.loc[output_detailed['event'] == val]['value'].mean()

        elif subphase == 'ipsilateral swing':
            if muscle[0]=='L': val = 'LTO'  
            else : val = 'RTO'
            output_rel_list = output_detailed.loc[output_detailed['event'] == val]['value'].values

            # output = output_detailed.loc[output_detailed['event'] == val]['value'].mean()

    output = np.nanmean(output_rel_list)
    output_rel = pd.DataFrame({'Measure':[f'M{i+1}' for i in range(len(output_rel_list))],'Value':output_rel_list})

    return output, output_detailed,output_rel


# muscle needed ! ->
def BMu105(emg,event, sub_MVIC = None ,sub_MVIC_event = None, show = False,**args):
    kwargs = {'nb_BM': 105}
    if args != {}:
        for k,v in zip(list(args.keys()),list(args.values())):
            if k not in list(kwargs.keys()):
                kwargs[k]=v
    return BMu_Arendt_Nielsen(emg,event,**kwargs)

def BMu107(emg,event, sub_MVIC = None ,sub_MVIC_event = None, show = False,**args):
    kwargs = {'nb_BM': 107}
    if args != {}:
        for k,v in zip(list(args.keys()),list(args.values())):
            if k not in list(kwargs.keys()):
                kwargs[k]=v
    return BMu_Arendt_Nielsen(emg,event,**kwargs)

def BMu106(emg,event, sub_MVIC = None ,sub_MVIC_event = None, show = False,**args):
    kwargs = {'nb_BM': 106}
    if args != {}:
        for k,v in zip(list(args.keys()),list(args.values())):
            if k not in list(kwargs.keys()):
                kwargs[k]=v
    return BMu_Arendt_Nielsen(emg,event,**kwargs)


def BMu102(emg,event, sub_MVIC = None ,sub_MVIC_event = None, show = False,**args):
    kwargs = {'nb_BM': 102}
    if args != {}:
        for k,v in zip(list(args.keys()),list(args.values())):
            if k not in list(kwargs.keys()):
                kwargs[k]=v
    return BMu_Arendt_Nielsen(emg,event,**kwargs)

def BMu103(emg,event, sub_MVIC = None ,sub_MVIC_event = None, show = False,**args):
    kwargs = {'nb_BM': 103}
    if args != {}:
        for k,v in zip(list(args.keys()),list(args.values())):
            if k not in list(kwargs.keys()):
                kwargs[k]=v
    return BMu_Arendt_Nielsen(emg,event,**kwargs)

def BMu104(emg,event, sub_MVIC = None ,sub_MVIC_event = None, show = False,**args):
    kwargs = {'nb_BM': 104}
    if args != {}:
        for k,v in zip(list(args.keys()),list(args.values())):
            if k not in list(kwargs.keys()):
                kwargs[k]=v
    return BMu_Arendt_Nielsen(emg,event,**kwargs)

# <- muscle needed 


def BMu100(emg,event, sub_MVIC = None ,sub_MVIC_event = None, show = False,**args):
    return BMu_Arendt_Nielsen(emg,event,**{'nb_BM': 100})

def BMu101(emg,event, sub_MVIC = None ,sub_MVIC_event = None, show = False,**args):
    return BMu_Arendt_Nielsen(emg,event,**{'nb_BM': 101})

def BMu108(emg,event, sub_MVIC = None ,sub_MVIC_event = None, show = False,**args):
    return BMu_Arendt_Nielsen(emg,event,**{'nb_BM': 108})



def BMu_Arendt_Nielsen_norm(emg,event, sub_MVIC = None ,sub_MVIC_event = None, show = False,**kwargs):

    # by default: the mean EMG of all subphases are pulled together to compute the BMu

    try :
        nb_BM = kwargs['nb_BM']
        if nb_BM in [105,107,106]:
            subphase = 'ipsilateral swing'
        elif nb_BM in [102,104,103]:
            subphase = 'contralateral swing'
        elif nb_BM in [108,100,101] :
            subphase = 'all'
        
        if nb_BM in [105,107,106,102,104,103]:
            try:
                muscle = kwargs['muscle']
            except:
                raise Exception('Missing muscle name')

    except:
        raise Exception('Missing BMu number')


    assert not empty_value_check(emg), 'Empty emg values'


    # Preprocessing of the signal
    filtered_emg = preprocessing_arendt_nielsen_1995(emg)

    #normalisation
    event_line_mvc = extract_event(sub_MVIC,sub_MVIC_event,event_type = 'MVC', task_name = 'Endurance_Sorensen')
    filtered_sub_MVIC = full_wave(preprocessing_arendt_nielsen_1995(sub_MVIC[event_line_mvc == 1]))
    #signal
    filtered_sub_MVIC_mean = np.mean(filtered_sub_MVIC)
    

    ## Event detection emg
    # time normalization with regard to the stride time
    df_emg_event_norm = time_emg_event_normalization(filtered_emg,event,shift=0)
    output_detailed = pd.DataFrame(data=None, columns=['event','emg','stride'])

    
    df_grouped = df_emg_event_norm.groupby('stride')
    for i in range(1,len(df_grouped )+1): 
        df = df_grouped.get_group(i)
        #find peak
        df_ev = df.loc[:,('event')].fillna(method="ffill")
        # peak = np.max(df[ (df_ev.values == 'RHS') |  (df_ev.values == 'LHS') ]['emg'])
        # normalize emg
        emg_norm = normalization(df.loc[:,('emg')],filtered_sub_MVIC_mean)
        # emg_norm = normalization(df.loc[:,('emg')],peak) # original normalisation to peak 50
        df.loc[:,'emg'] = emg_norm

        
        if show:
            _ = extract_event_stride(df,show = show)

        df.loc[:,('event')] = df.loc[:,('event')].fillna(method="ffill").values

        #BMu = mean per subphases
        values = df.groupby('event').mean().reset_index()
        output_detailed = pd.concat([output_detailed,values])


    output_detailed = output_detailed.rename(columns={"stride": "rep", "emg": "value"})
    output_detailed = output_detailed[['value','event','rep']].reset_index().drop('index',axis=1)

    # BMu : mean of all subphase

    if subphase == 'all':
        # output = output_detailed['value'].mean()
        output_rel_list = output_detailed.groupby('rep')['value'].mean().values

    else:
        if subphase == 'contralateral swing':
            if muscle[0]=='L': val = 'RTO'  
            else : val = 'LTO'
            output_rel_list = output_detailed.loc[output_detailed['event'] == val]['value'].values

            # output = output_detailed.loc[output_detailed['event'] == val]['value'].mean()

        elif subphase == 'ipsilateral swing':
            if muscle[0]=='L': val = 'LTO'  
            else : val = 'RTO'
            output_rel_list = output_detailed.loc[output_detailed['event'] == val]['value'].values

            # output = output_detailed.loc[output_detailed['event'] == val]['value'].mean()

    output = np.nanmean(output_rel_list)
    output_rel = pd.DataFrame({'Measure':[f'M{i+1}' for i in range(len(output_rel_list))],'Value':output_rel_list})

    return output, output_detailed,output_rel


def BMu102_norm(emg,event,sub_MVIC = None ,sub_MVIC_event = None, show = False,**args):
    kwargs = {'nb_BM': 102}
    if args != {}:
        for k,v in zip(list(args.keys()),list(args.values())):
            if k not in list(kwargs.keys()):
                kwargs[k]=v

    return BMu_Arendt_Nielsen_norm(emg,event,sub_MVIC ,sub_MVIC_event ,**kwargs)


def BMu108_norm(emg,event,sub_MVIC = None ,sub_MVIC_event = None, show = False,**args):
    kwargs = {'nb_BM': 108}
    if args != {}:
        for k,v in zip(list(args.keys()),list(args.values())):
            if k not in list(kwargs.keys()):
                kwargs[k]=v

    return BMu_Arendt_Nielsen_norm(emg,event,sub_MVIC ,sub_MVIC_event ,**kwargs)

##############################
# Van der Hulst et al., 2010 #
##############################


def preprocessing_van_der_hulst_2010(emg):
    filtered_emg = butterfilter( full_wave( mean_removal( butterfilter (emg, order = 3, c_f=20, type='highpass' )) ), order = 3, c_f = 25, type = 'lowpass')
    return filtered_emg




def BMu_van_der_hulst(emg,event, sub_MVIC = None ,sub_MVIC_event = None, show = False,**kwargs):

    # by default: the mean EMG of all subphases are pulled together to compute the BMu
    try:
        muscle = kwargs['muscle']
    except:
        raise Exception( 'muscle missing' )

    try :
        nb_BM = kwargs['nb_BM']
        if nb_BM == 114:
            # BMu114 3 : ratio
            # subphase = 'double stance' # BMu114 4
            subphase = 'first double stance' # BMu114 5
        elif nb_BM == 115:
            subphase = 'all'
    except:
        pass


    assert not empty_value_check(emg), 'Empty emg values'
    assert subphase in ['contralateral swing','ipsilateral swing','all','first double stance','double stance','ratio'], 'Unknown subphase'

    # Preprocessing of the signal
    filtered_emg = preprocessing_van_der_hulst_2010(emg)

    # Event detection emg
    # time normalization with regard to the stride time
    df_emg_event_norm = time_emg_event_normalization(filtered_emg,event,shift=0)
    output_detailed = pd.DataFrame(data=None, columns=['event','emg','stride'])

    # for each stride compute BMu
    df_grouped = df_emg_event_norm.groupby('stride')
    for i in range(1,len(df_grouped )+1): 
        df = df_grouped.get_group(i)     
        
        # computation of the mean EMG of each subphase
        df['event'] = df['event'].fillna(method="ffill").values
        values = df.groupby('event').mean().reset_index()
        output_detailed = pd.concat([output_detailed,values])


    output_detailed = output_detailed.rename(columns={"stride": "rep", "emg": "value"})
    output_detailed = output_detailed[['value','event','rep']].reset_index().drop('index',axis=1)

    if subphase == 'all':
        output = output_detailed['value'].mean()
        output_rel_list = output_detailed.groupby('rep').mean().values.flatten()
        output_rel = pd.DataFrame({'Measure':[f'M{i+1}' for i in range(len(output_rel_list))],'Value':output_rel_list})

    else:
        if subphase == 'contralateral swing':
            if muscle[0]=='L': val = 'RTO'  
            else : val = 'LTO'
            output_rel_list =output_detailed.loc[output_detailed['event'] == val, 'value'].values.flatten()
            output_rel = pd.DataFrame({'Measure':[f'M{i+1}' for i in range(len(output_rel_list))],'Value':output_rel_list})
            output = np.nanmean(output_rel_list)

        elif subphase == 'ipsilateral swing':
            if muscle[0]=='L': val = 'LTO'  
            else : val = 'RTO'
            output_rel_list = output_detailed.loc[output_detailed['event'] == val, 'value'].values.flatten()
            output_rel = pd.DataFrame({'Measure':[f'M{i+1}' for i in range(len(output_rel_list))],'Value':output_rel_list})
            output = np.nanmean(output_rel_list)

        elif subphase == 'double stance':
            output_rel_list = np.mean([output_detailed.loc[output_detailed['event'] == 'LHS','value'].values.flatten(), output_detailed.loc[output_detailed['event'] == 'RHS','value'].values.flatten()],axis=0)
            output_rel = pd.DataFrame({'Measure':[f'M{i+1}' for i in range(len(output_rel_list))],'Value':output_rel_list})
            output = np.nanmean(output_rel_list)

        elif subphase == 'first double stance':
            if muscle[0]=='R': val = 'LHS' # right muscle a stride begins with left heel strike
            else: val = 'RHS'
            output_rel_list = output_detailed.loc[output_detailed['event'] == val,'value'].values.flatten()
            output_rel = pd.DataFrame({'Measure':[f'M{i+1}' for i in range(len(output_rel_list))],'Value':output_rel_list})
            output = np.nanmean(output_rel_list)

        elif subphase == 'ratio':

            if muscle[0]=='R': swi,ds = 'LTO','RHS'  
            else : swi,ds = 'RTO','LHS'

            df_swi = output_detailed.loc[output_detailed['event'] == swi, 'value'].values
            df_ds = output_detailed.loc[output_detailed['event'] == ds, 'value'].values
            ratio_rel = df_swi/df_ds
            output_rel = pd.DataFrame({'Measure':[f'M{i+1}' for i in range(len(ratio_rel))],'Value':ratio_rel})
            output = ratio_rel.mean()


    return output, output_detailed, output_rel





def BMu115(emg,event, sub_MVIC = None ,sub_MVIC_event = None, show = False,**kwargs):
    kwargs['nb_BM'] = 115
    return BMu_van_der_hulst(emg,event,**kwargs)


def BMu114(emg,event, sub_MVIC = None ,sub_MVIC_event = None, show = False,**kwargs):
    kwargs['nb_BM'] = 114
    return BMu_van_der_hulst(emg,event,**kwargs)





















###########################
# Dankaerts et al., 2006a #
###########################

def preprocessing_dankaerts_2006(emg):
    emg_butter = butterfilter_dual(emg, order=2, c_f=30, type='highpass')
    emg = full_wave( mean_removal(emg_butter))
    filtered_emg = butterfilter_bandpass_dual(emg,order=2,c_f_low= 4,c_f_high=400)
    env = rolling_function(filtered_emg,func=lambda x: np.sqrt(np.mean(x**2)), win =150,unit='frame')
    return env

def preprocessing_dankaerts_2006_invfw(emg):
    emg_butter = butterfilter_dual(emg, order=2, c_f=30, type='highpass')
    filtered_emg = butterfilter_bandpass_dual(emg,order=2,c_f_low= 4,c_f_high=400)
    emg = full_wave( mean_removal(emg_butter))
    env = rolling_function(filtered_emg,func=lambda x: np.sqrt(np.mean(x**2)), win =150,unit='frame')
    return env

def preprocessing_dankaerts_2006_abw(emg):
    emg_butter = hampel_adaptative_bw(emg)
    emg = full_wave( mean_removal(emg_butter))
    filtered_emg = butterfilter_bandpass_dual(emg,order=2,c_f_low= 4,c_f_high=400)
    env = rolling_function(filtered_emg,func=lambda x: np.sqrt(np.mean(x**2)), win =150,unit='frame')
    return env

def preprocessing_dankaerts_2006_fas(emg): #not working for subMIVC sig ?
    emg_fas = fas_filter(emg,L=0.005,nq_min=5,k=5)
    emg = full_wave(mean_removal(emg))
    filtered_emg = butterfilter_bandpass_dual(full_wave( mean_removal( emg_fas )),order=2,c_f_low= 4,c_f_high=400)
    env = rolling_function(filtered_emg,func=lambda x: np.sqrt(np.mean(x**2)), win =150,unit='frame')
    return env


def BMu_Dankaerts_2006(emg,event, sub_MVIC, sub_MVIC_event, show = False,preprocessing_dankaerts=preprocessing_dankaerts_2006,**kwargs):

    # preprocessing of the signal
    filtered_emg = preprocessing_dankaerts(emg)
        
    # MVC computation: event extraction, preprocessing, mean
    event_line_mvc = extract_event(sub_MVIC,sub_MVIC_event,event_type = 'MVC',task_name='Endurance_Sorensen')
    filtered_sub_MVIC = preprocessing_dankaerts(sub_MVIC[event_line_mvc == 1])
    filtered_sub_MVIC_mean = np.nanmean(filtered_sub_MVIC)

    # Normalization
    emg_norm = normalization(filtered_emg,filtered_sub_MVIC_mean)
    output_rel_list = [np.nanmean(emg_norm[i * (len(emg_norm)//3) : (i+1) * (len(emg_norm)//3)]) for i in range(3) ]

    output = np.nanmean(output_rel_list)
    detailed_output = pd.DataFrame( [[np.nanmean(emg_norm),'total',1],
                                     [np.nanmean(emg_norm[:len(emg_norm)//3]),'start',1],
                                     [np.nanmean(emg_norm[len(emg_norm)//3:len(emg_norm)//3*2]),'middle',1],
                                     [np.nanmean(emg_norm[len(emg_norm)//3*2:]),'end',1]], columns=['value','event','rep'])

    output_rel = pd.DataFrame({'Measure':[f'M{i+1}' for i in range(len(output_rel_list))],'Value':output_rel_list})

    return output, detailed_output,output_rel




def BMu67(emg,event, sub_MVIC, sub_MVIC_event, show = False,**kwargs):
    return BMu_Dankaerts_2006(emg,event, sub_MVIC, sub_MVIC_event, show = False,**kwargs)

def BMu68(emg,event, sub_MVIC, sub_MVIC_event, show = False,**kwargs):
    return BMu_Dankaerts_2006(emg,event, sub_MVIC, sub_MVIC_event, show = False,**kwargs)


def BMu68_invfw(emg,event, sub_MVIC, sub_MVIC_event, show = False,**kwargs):
    return BMu_Dankaerts_2006(emg,event, sub_MVIC, sub_MVIC_event, show = False,preprocessing_dankaerts=preprocessing_dankaerts_2006_invfw,**kwargs)


def BMu67_abw(emg,event, sub_MVIC, sub_MVIC_event, show = False,**kwargs):
    return BMu_Dankaerts_2006(emg,event, sub_MVIC, sub_MVIC_event, show = False,preprocessing_dankaerts=preprocessing_dankaerts_2006_abw,**kwargs)

def BMu67_invfw(emg,event, sub_MVIC, sub_MVIC_event, show = False,**kwargs):
    return BMu_Dankaerts_2006(emg,event, sub_MVIC, sub_MVIC_event, show = False,preprocessing_dankaerts=preprocessing_dankaerts_2006_invfw,**kwargs)






###########################
# Dankaerts et al., 2009 #
###########################

def preprocessing_dankaerts_2009(emg):
    emg_butter = butterfilter_dual(emg, order=2, c_f=30, type='highpass')
    filtered_emg = butterfilter_bandpass_dual(full_wave( mean_removal( emg_butter )),order=2,c_f_low= 4,c_f_high=400)
    env = rolling_function(filtered_emg,func=lambda x: np.sqrt(np.nanmean(x**2)), win =150,unit='frame')
    return env

def preprocessing_dankaerts_2009_invfw(emg):
    emg_butter = butterfilter_dual(emg, order=2, c_f=30, type='highpass')
    filtered_emg = butterfilter_bandpass_dual(emg_butter,order=2,c_f_low= 4,c_f_high=400)
    filtered_emg = full_wave(mean_removal(filtered_emg))
    env = rolling_function(filtered_emg,func=lambda x: np.sqrt(np.nanmean(x**2)), win =150,unit='frame')
    return env


def BMu_Dankaerts_2009(emg,event, sub_MVIC, sub_MVIC_event, show = False,prepro = preprocessing_dankaerts_2009,**kwargs):


    filtered_emg = prepro(emg)

    # MVC computation: event extraction, preprocessing, mean
    event_line_mvc = extract_event(sub_MVIC,sub_MVIC_event,event_type='MVC',task_name= 'Endurance_Sorensen',show=False)
    filtered_sub_MVIC = preprocessing_dankaerts_2009(sub_MVIC[event_line_mvc == 1])
    filtered_sub_MVIC_mean = np.nanmean(filtered_sub_MVIC)

    # Normalization
    emg_norm = normalization(filtered_emg,filtered_sub_MVIC_mean)

    # extraction of the events start/stop_standing_flexion
    df_extracted_event = event_extraction_flex_return(emg_norm,event)
    # segmentation of the emg according to the previously computed event
    df_emg_subphases = event_segmentation_from_df_time(emg_norm,df_extracted_event)

    # BMu computation for each suphases and quartiles of bending and return subphases
    detailed_output = []
    for i in range(df_emg_subphases.shape[0]):
        ev = df_emg_subphases.loc[i,'event']
        emg_sub = df_emg_subphases.loc[i,'emg']
        rep_sub = df_emg_subphases.loc[i,'rep']
        if ev == 'start_standing' or ev == 'start_flexion': # standing of MVF period
            detailed_output += [[np.nanmean(emg_sub),f'{"standing" if ev == "start_standing" else "MVF"}',rep_sub]]
        else: # flexion amd return are divided into quartiles
            detailed_output += [[np.nanmean(emg_sub[i * int(len(emg_sub)/4):(i + 1) * int(len(emg_sub)/4)]),f"{'bending' if ev=='stop_standing' else 'return'}_Q{i+1}",rep_sub] for i in range(4) ]


    df_detailed_output = pd.DataFrame(detailed_output,columns=['value','event','rep'])
    # detailed_output2 = []
    # for ev in df_detailed_output['event'].unique():
    #     detailed_output2 += [[np.mean(df_detailed_output.loc[df_detailed_output['event']==ev,'value']),ev]]
    # df_detailed_output2 = pd.DataFrame(detailed_output2,columns = ['value','event'])
    output_rel_list = df_detailed_output.loc[df_detailed_output['event']=='MVF','value'].values
    output_rel = pd.DataFrame({'Measure':[f'M{i+1}' for i in range(len(output_rel_list))],'Value':output_rel_list})
    output = np.nanmean(output_rel_list)

    return output, df_detailed_output, output_rel




def BMu36(emg,event, sub_MVIC, sub_MVIC_event, show = False,**kwargs):
    return BMu_Dankaerts_2009(emg,event, sub_MVIC, sub_MVIC_event, show = False,**kwargs)

def BMu36_invfw(emg,event, sub_MVIC, sub_MVIC_event, show = False,**kwargs):
    return BMu_Dankaerts_2009(emg,event, sub_MVIC, sub_MVIC_event,prepro=preprocessing_dankaerts_2006_invfw, show = False,**kwargs)

# def BMu67(emg,event, sub_MVIC, sub_MVIC_event, show = False,**kwargs):
#     return BMu_Dankaerts_2009(emg,event, sub_MVIC, sub_MVIC_event, show = False,**kwargs)

# def BMu68(emg,event, sub_MVIC, sub_MVIC_event, show = False,**kwargs):
#     return BMu_Dankaerts_2009(emg,event, sub_MVIC, sub_MVIC_event, show = False,**kwargs)













    





















######################
# MEHTA et al., 2000 #
######################

# preprocessing that seem better: preprocessing_mehta_2000 then preprocessing_mehta_2000_fas

def preprocessing_mehta_2000(emg):
    emg_bw = butterfilter_dual(emg,order =2, c_f=30, type='highpass')
    filtered_emg = butterfilter_dual(full_wave( mean_removal(emg_bw)), order = 1, c_f=10,type='lowpass')
    return filtered_emg

def preprocessing_mehta_2000_ABW(emg):
    emg_bw = adaptive_butterfilter(emg, cf=35)
    filtered_emg = butterfilter_dual(full_wave( mean_removal(emg_bw)), order = 1, c_f=10,type='lowpass')
    return filtered_emg


def preprocessing_mehta_2000_bwinv(emg):
    emg_bw = butterfilter_dual(emg,order =2, c_f=30, type='highpass')
    filtered_emg = butterfilter_dual(emg_bw, order = 1, c_f=10,type='lowpass')
    filtered_emg = full_wave( mean_removal(filtered_emg))
    return filtered_emg

def preprocessing_mehta_2000_bwinv2(emg):
    emg_bw = butterfilter_dual(emg,order =2, c_f=30, type='highpass')
    filtered_emg = full_wave( mean_removal(emg_bw))
    filtered_emg = butterfilter_dual(filtered_emg, order = 1, c_f=10,type='lowpass')
    filtered_emg = full_wave( mean_removal(filtered_emg))
    return filtered_emg

def preprocessing_mehta_2000_bwinvclip(emg):
    emg_bw = butterfilter_dual(emg,order =2, c_f=30, type='highpass')
    filtered_emg = full_wave( mean_removal(emg_bw))
    filtered_emg = butterfilter_dual(filtered_emg, order = 1, c_f=10,type='lowpass')
    #half wave rectification
    filtered_emg = np.clip( mean_removal(filtered_emg),0,np.max(filtered_emg))
    return filtered_emg

def preprocessing_mehta_2000_fas(emg):
    emg_fas = fas_filter(emg.copy(),L=0.1,nq_min=10,k=2)
    filtered_emg = butterfilter_dual(full_wave( mean_removal(emg_fas)), order = 1, c_f=10,type='lowpass')
    return filtered_emg

def preprocessing_mehta_2000_fas_bwinv(emg):
    emg_fas = fas_filter(emg.copy(),L=0.1,nq_min=10,k=2)
    filtered_emg = butterfilter_dual(emg_fas, order = 1, c_f=10,type='lowpass')
    filtered_emg = full_wave( mean_removal(filtered_emg))
    return filtered_emg

def preprocessing_mehta_2000_fas_bwinv2(emg):
    emg_fas = fas_filter(emg.copy(),L=0.1,nq_min=10,k=2)
    filtered_emg = full_wave( mean_removal(emg_fas))
    filtered_emg = butterfilter_dual(filtered_emg, order = 1, c_f=10,type='lowpass')
    filtered_emg = full_wave( mean_removal(filtered_emg))
    return filtered_emg



def BMu_mehta(emg,event=None, sub_MVIC = None ,sub_MVIC_event = None, show = False, preprocessing = preprocessing_mehta_2000,**kwargs):
    
    # preprocessing of the signal
    filtered_emg = preprocessing(emg)

    # detection of contractions
    nb_BM = kwargs['nb_BM']
    if nb_BM == 86:
        output, state_contraction, _, _ = double_threshold(filtered_emg, win = 0.15, time_win = 0.15, number_mean = 2, number_std = 0) 
        # fas p2 filtered_emg, win = 0.15, time_win = 0.15, number_mean = 1.5, number_std = 0
    elif nb_BM == 88:
        output, state_contraction, _, _ = double_threshold(filtered_emg, win = 0.1, time_win = 0.2, number_mean = 1, number_std = 2) 

    elif nb_BM == 87:
        output, state_contraction, _, _ = double_threshold(filtered_emg, win = 0.1, time_win = 0.2, number_mean = 1, number_std = 2) 


    ## BMu computation
    # values is a list that will store the duration of each activation burst
    values = []
    # if some contractions have been detected
    if 1 in state_contraction:
        values = []
        # takes the segment were the contraction has been detected, ie where there are consecutive 1 in state_contraction
        for key, group in groupby(state_contraction):
            
            if key == 1:
                gr = list(group)
                length = len( gr)
                values += [length/SAMPLING_RATE]
            
        output = np.mean(values)
        output_detailed = pd.DataFrame({'value':values,'event':np.ones_like(values),'rep':np.arange(1,len(values)+1)})
        output_rel_list = values
        output_rel = pd.DataFrame({'Measure':[f'M{i+1}' for i in range(len(output_rel_list))],'Value':output_rel_list})

    else:
        output = None
        output_rel_list = [np.NAN,np.NAN,np.NAN]
        output_rel = pd.DataFrame({'Measure':[f'M{i+1}' for i in range(len(output_rel_list))],'Value':output_rel_list})
        output_detailed = pd.DataFrame({'value':None,'event':1,'rep':1},index=[0])

    return output,output_detailed,output_rel




def BMu86(emg,event=None, sub_MVIC = None ,sub_MVIC_event = None, show = False,**kwargs):
    return BMu_mehta(emg,preprocessing = preprocessing_mehta_2000,**{'nb_BM':86})

def BMu88(emg,event=None, sub_MVIC = None ,sub_MVIC_event = None, show = False,**kwargs):
    return BMu_mehta(emg,preprocessing = preprocessing_mehta_2000,**{'nb_BM':88})

def BMu87(emg,event=None, sub_MVIC = None ,sub_MVIC_event = None, show = False,**kwargs):
    return BMu_mehta(emg,preprocessing = preprocessing_mehta_2000,**{'nb_BM':87})




def BMu86_abw(emg,event=None, sub_MVIC = None ,sub_MVIC_event = None, show = False,**kwargs):
    return BMu_mehta(emg,preprocessing =preprocessing_mehta_2000_ABW,**{'nb_BM':86})

def BMu86_fas(emg,event=None, sub_MVIC = None ,sub_MVIC_event = None, show = False,**kwargs):
    return BMu_mehta(emg,preprocessing =preprocessing_mehta_2000_fas,**{'nb_BM':86})

def BMu88_fas(emg,event=None, sub_MVIC = None ,sub_MVIC_event = None, show = False,**kwargs):
    return BMu_mehta(emg,preprocessing =preprocessing_mehta_2000_fas,**{'nb_BM':88})

def BMu87_fas(emg,event=None, sub_MVIC = None ,sub_MVIC_event = None, show = False,**kwargs):
    return BMu_mehta(emg,preprocessing =preprocessing_mehta_2000_fas,**{'nb_BM':87})



def BMu86_bwinv(emg,event=None, sub_MVIC = None ,sub_MVIC_event = None, show = False,**kwargs):
    return BMu_mehta(emg,preprocessing = preprocessing_mehta_2000_bwinv,**{'nb_BM':86})

def BMu88_bwinv(emg,event=None, sub_MVIC = None ,sub_MVIC_event = None, show = False,**kwargs):
    return BMu_mehta(emg,preprocessing = preprocessing_mehta_2000_bwinv,**{'nb_BM':88})

def BMu87_bwinv(emg,event=None, sub_MVIC = None ,sub_MVIC_event = None, show = False,**kwargs):
    return BMu_mehta(emg,preprocessing = preprocessing_mehta_2000_bwinv,**{'nb_BM':87})



def BMu86_fas_bwinv(emg,event=None, sub_MVIC = None ,sub_MVIC_event = None, show = False,**kwargs):
    return BMu_mehta(emg,preprocessing = preprocessing_mehta_2000_fas_bwinv,**{'nb_BM':86})

def BMu88_fas_bwinv(emg,event=None, sub_MVIC = None ,sub_MVIC_event = None, show = False,**kwargs):
    return BMu_mehta(emg,preprocessing = preprocessing_mehta_2000_fas_bwinv,**{'nb_BM':88})

def BMu87_fas_bwinv(emg,event=None, sub_MVIC = None ,sub_MVIC_event = None, show = False,**kwargs):
    return BMu_mehta(emg,preprocessing = preprocessing_mehta_2000_fas_bwinv,**{'nb_BM':87})



########################
# Pääsuke et al., 2002 #
########################

def preprocessing_paasuke_2002(emg):
    return mean_removal(emg)

def BMu117(emg,event=None, sub_MVIC = None ,sub_MVIC_event = None, show = False,**kwargs):

    emg = preprocessing_paasuke_2002(emg)
    # with event
    event_line = extract_event(emg,event,task_name='Endurance_Sorensen', show=False,event_type='Endu')
    emg = emg[event_line==1]
     
    # rolling mean power frequency
    if type(emg) != pd.core.series.Series:
            emg = pd.Series(emg)
        
    win = 1
    emg_rolling = emg.rolling(win*SAMPLING_RATE, min_periods = 1,center = True,closed = 'neither',step = 500).apply(lambda x: mean_power_frequency(x,type='fft'))

    output_rel_list_start = [np.nanmean(emg_rolling[10*i:10*(i+1)+1]) for i in range (3)]
    output_rel_list_end = [np.nanmean(emg_rolling[-10*(i+1):-10*(i) if i!=0 else len(emg_rolling)]) for i in range (3)]

    dict_detailed = {}
    dict_detailed['value'] = output_rel_list_start+output_rel_list_end
    dict_detailed['event'] = ['start']*3 + ['stop']*3
    dict_detailed['rep'] = np.concatenate([np.arange(1,4) ,np.arange(1,4)])

    output_rel = pd.DataFrame({'Measure':[f'M{i+1}' for i in range(len(output_rel_list_start))],'Value':output_rel_list_start})
    output = np.nanmean(output_rel_list_start)
    output_detailed = pd.DataFrame(dict_detailed)
    output,output_rel,output_detailed

    return output, output_detailed, output_rel




def BMu119(emg,event=None, sub_MVIC = None ,sub_MVIC_event = None, show = False,**kwargs):

    emg = preprocessing_paasuke_2002(emg)
    # with event
    event_line = extract_event(emg,event,task_name='Endurance_Sorensen', show=False,event_type='Endu')
    emg = emg[event_line==1]

     
    # rolling mean power frequency
    if type(emg) != pd.core.series.Series:
            emg = pd.Series(emg)
        
    overlap = 0.5
    emg_mean_win = emg.rolling(1*SAMPLING_RATE, min_periods = 1 ,center = True,closed = 'neither',step = int((SAMPLING_RATE)*overlap)).apply(mean_power_frequency)

    t_all =  np.arange(0,len(emg_mean_win)*500,500)
    MPF_start = emg_mean_win[:int(5*1/overlap)+1]

    t_start = np.arange(0,len(MPF_start)*500,500)
    MPF_end = emg_mean_win[int(-5.5*1/overlap):]
    t_end = np.arange(0,len(MPF_end )*500,500)

    res_start =  linregress(t_start, MPF_start)
    res_stop =  linregress(t_end, MPF_end)
    res_all =  linregress(t_all, emg_mean_win)

    # MF slope as the percent change from the intial value ( %/min )
    MF_slope = 100 - ( ( np.mean(MPF_end) * 100 ) / np.mean(MPF_start) ) # decrease of the slope during the task in %
    MF_slope_min = MF_slope * 60/ (len(emg)/SAMPLING_RATE) #decrease as % per min


    output_detailed = pd.DataFrame([[res_start.slope,'slope start',1],[res_stop.slope,'slope end',1],[res_all.slope,'slope all',1],[MF_slope_min,'percent',1],[np.mean(MPF_end),'MF end',1],[np.mean(MPF_start),'MF start',1]],columns=['value','event','rep']) 
    output_rel_list = [None,None,None]
    output_rel = pd.DataFrame({'Measure':[f'M{i+1}' for i in range(len(output_rel_list))],'Value':output_rel_list})

    return MF_slope_min, output_detailed, output_rel





#######################
# Suuden et al., 2014 #
#######################

def preprocessing_suuden_2014(emg):
    return mean_removal(emg)

def BMu118(emg,event=None, sub_MVIC = None ,sub_MVIC_event = None, show = False,**kwargs):

    emg = preprocessing_suuden_2014(emg)
    # with event
    event_line = extract_event(emg,event,task_name='Endurance_Sorensen', show=False,event_type='Endu')
    emg = emg[event_line==1]
        
    # rolling mean power frequency
    if type(emg) != pd.core.series.Series:
            emg = pd.Series(emg)
        
   
    win = 1
    overlap = (win*SAMPLING_RATE)//2
    emg_rolling = emg.rolling(win*SAMPLING_RATE, min_periods = 1,center = True,closed = 'neither',step = overlap).apply(lambda x: median_frequency(x,type='fft'))

    output_rel_list = [np.nanmean(emg_rolling[10*i:10*(i+1)+1]) for i in range (3)]
    list_output_end = [np.nanmean(emg_rolling[-10*(i+1):-10*(i) if i!=0 else len(emg_rolling)]) for i in range (3)]

    dict_detailed = {}
    dict_detailed['value'] = output_rel_list+list_output_end
    dict_detailed['event'] = ['start']*3 + ['stop']*3
    dict_detailed['rep'] = np.concatenate([np.arange(1,4) ,np.arange(1,4)])

    output_rel = pd.DataFrame({'Measure':[f'M{i+1}' for i in range(len(output_rel_list))],'Value':output_rel_list})
    output = np.nanmean(output_rel_list)
    output_detailed = pd.DataFrame(dict_detailed)
    output,output_rel,output_detailed

    return output, output_detailed, output_rel







############################
# Suter and Lindsay., 2001 #
############################

def preprocessing_suter_2001(emg):
     return mean_removal(emg)

def BMu118_suter(emg,event=None, sub_MVIC = None ,sub_MVIC_event = None, show = False,**kwargs):
    emg = preprocessing_suter_2001(emg)
    # with event
    event_line = extract_event(emg,event,task_name='Endurance_Sorensen', show=False,event_type='Endu')
    emg = emg[event_line==1]
        
    # rolling mean power frequency
    if type(emg) != pd.core.series.Series:
            emg = pd.Series(emg)
        

    win = 5 # 5sec window
    overlap = 1*SAMPLING_RATE
    emg_rolling = emg.rolling(win*SAMPLING_RATE, min_periods = 5*SAMPLING_RATE,center = True,closed = 'both',step = overlap).apply(lambda x: median_frequency(x,type='fft'))

    emg_rolling = emg_rolling.dropna()
    t_all = np.arange(emg_rolling.shape[0])
    res_all =  linregress(t_all, emg_rolling.values)

    output_detailed = pd.DataFrame([[res_all.slope,'slope',1],[res_all.intercept,'intercept',1]],columns=['value','event','rep']) 
    output_rel_list = [None,None,None]
    output_rel = pd.DataFrame({'Measure':[f'M{i+1}' for i in range(len(output_rel_list))],'Value':output_rel_list})

    return res_all.intercept, output_detailed, output_rel





    # if show:
    #     emg_on_rep,emg_on, state_contraction, threshold_amp,event_emg = contraction_detection_lima_2018_0(filtered_emg)

    #     fig, (ax1,ax2,ax3) = plt.subplots(1,3,figsize=(10,3))
    #     t1 = np.arange(len(event_emg))/SAMPLING_RATE
    #     ax1.plot(t1, state_contraction*threshold_amp,alpha = 0.7,color = 'green')
    #     ax1.plot(t1,np.ones_like(t1)*threshold_amp, alpha = 0.5, linestyle='--')
    #     ax1.plot(t1, event_emg,alpha = 0.5)
    #     ax1.set(xlabel='time (s)', ylabel='amplitude (V)', title ='Muscle onset and offset detection')


    #     ax2.plot(np.array(emg_on.index)/SAMPLING_RATE,emg_on.emg, color = 'blue', label= 'flexion')
    #     ax2.plot(np.arange(len(filtered_emg))/SAMPLING_RATE, filtered_emg,alpha = 0.7,color = 'green', label = 'whole task')
    #     ax2.set(xlabel='time (s)', ylabel='amplitude (V)', title ='EMG activity during bending and return')
    #     ax2.legend()
        
    #     t3 = np.arange(len(emg))/SAMPLING_RATE
    #     ax3.plot(t3, emg, label='before')
    #     ax3.plot(t3, filtered_emg, alpha = 0.7,label='after')
    #     # ax3.plot(t3,event * 0.1)

    #     ax3.set(xlabel='time (s)', ylabel='amplitude (V)', title ='EMG before and after processing')
    #     ax3.legend()

    #     if title:
    #         fig.suptitle(title)

    #     fig.tight_layout()



# def show_evolution(emg, filtered_emg, event=None,event_type=None,filtered_emg_contraction=None,state_contraction=None,threshold_amp=None,emg_on = None,plot_event = False,contraction = False):

#     nb_ax = 1
#     if plot_event: nb_ax += 1
#     if contraction: nb_ax +=2
#     fig, ax  = plt.subplots(1,4,figsize=(5*nb_ax,3))

#     def plot_event(ax):
#         if event_type == 'Gait' or event_type != 'MVC':

#             for i in range(1,len(event['time'])):
#                 x,next_x,current_event = event['time'][i-1],event['time'][i], event['event'][i-1]
#                 p1 = plt.axvline(x=x,color='white')
#                 if event_type == 'Gait':
#                     ax.axvspan(x, next_x, color='red' if current_event == 'RHS' else 'orange' if current_event == 'RTO' else 'yellow' if current_event == 'LHS' else 'green', alpha=0.3)
        
#                 elif event_type != 'MVC':
                    
#                     ax.axvspan(x, next_x, color= 'green' if current_event == 'stopMotion' else 'orange' , alpha=0.3)


#         else:
#             start_mvc = event.loc[event['event']  == 'startMVC']['time'].values[0]
#             stop_mvc = event.loc[event['event'] == 'stopMVC']['time'].values[0]
#             start_endu = event.loc[event['event'] == 'startEndurance']['time'].values[0]
#             stop_endu = event.loc[event['event'] == 'stopEndurance']['time'].values[0]

#             ax.axvspan(start_mvc, stop_mvc, color='cyan', alpha = 0.3,label =  'mvc' )
#             ax.axvspan(start_endu, stop_endu, color='green', alpha = 0.3 ,label =  'endurance' )
#             ax.legend()

        
#         t = np.arange(0,len(emg))/SAMPLING_RATE
#         ax.set(xlabel='time (s)', ylabel='amplitude (V)', title = 'Original EMG and its events')
#         ax.plot(t,emg)

#     def plot_filtering(ax):
#         t2 = np.arange(len(emg))/SAMPLING_RATE
#         ax.plot(t2, emg, label='before')
#         ax.plot(t2, filtered_emg, alpha = 0.7,label='after')

#         ax.set(xlabel='time (s)', ylabel='amplitude (V)', title ='EMG before and after processing')
#         ax.legend()
    

#     def plot_contraction(ax1,ax2):
#         t1 = np.arange(len(event))/SAMPLING_RATE
#         ax1.plot(t1, state_contraction*threshold_amp,alpha = 0.7,color = 'green')
#         ax1.plot(t1,np.ones_like(t1)*threshold_amp, alpha = 0.5, linestyle='--')
#         ax1.plot(t1, filtered_emg_contraction,alpha = 0.5)
#         ax1.set(xlabel='time (s)', ylabel='amplitude (V)', title ='Muscle onset and offset detection')

#         ax2.plot(np.array(emg_on.index)/SAMPLING_RATE,emg_on.emg, color = 'blue', label= 'flexion')
#         ax2.plot(np.arange(len(filtered_emg))/SAMPLING_RATE, filtered_emg,alpha = 0.7,color = 'green', label = 'whole task')
#         ax2.set(xlabel='time (s)', ylabel='amplitude (V)', title ='EMG activity during bending and return')
#         ax2.legend()

#     plot_filtering(ax[0])

#     if plot_event: plot_event(ax[1])
#     if contraction: plot_contraction(ax[2],ax[3])
        
#     fig.tight_layout()


#     show_evolution(emg, filtered_emg, event=event,event_type='Gait',filtered_emg_contraction=None,state_contraction=None,threshold_amp=None,emg_on = None,plot_event = True,contraction = False)