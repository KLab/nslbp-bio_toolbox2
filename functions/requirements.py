from ezc3d import c3d

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

import os
from os.path import exists
from glob import glob
import fnmatch
from itertools import permutations,groupby


from scipy import signal,interpolate,stats
from scipy.signal import argrelextrema, firwin, kaiserord, hilbert, argrelmin
from scipy.stats import shapiro, mannwhitneyu, linregress,ttest_ind,t,ttest_rel,wilcoxon #as student
from scipy.fft import fft, rfft,ifft
from scipy.fft import fftfreq, rfftfreq
import pywt
import random
from webbrowser import open_new_tab

import shutil
from time import time as timer
import json

FIG_SIZE = (4,2)
SAMPLING_RATE = 1000
SUBJECT = np.arange(1,69)
SUBJECT = [s for s in SUBJECT if s not in [4,15,18,19,20,31,37,59]] # eigth excluded: one without INI and REL (59), seven without REL,  20 is a control, 4,15,18,19,31,37 are patients

# INI_SUBJECT = np.delete(SUBJECT, np.where(SUBJECT == 59))
# REL_SUBJECT = np.delete(SUBJECT, np.where(SUBJECT in [4,15,18,19,20,31,37,59]))

SUBJECT_C = [1, 2, 3, 5, 6, 7, 8, 13, 16, 21, 22, 23, 25, 35, 38, 39, 40, 44, 46, 48, 49, 52, 55, 56, 57, 60, 64, 66, 67, 68]
SUBJECT_P = [9,  10, 11, 12, 14, 17, 24, 26, 27, 28, 29, 30, 32, 33, 34, 36, 41, 42, 43, 45, 47, 50, 51, 53, 54, 58, 61, 62, 63, 65]
SUBJECT_LLBP = [10, 12, 14, 26, 28, 29, 30, 32, 41, 42, 45, 50, 53, 54]
SUBJECT_HLBP = [ 9, 11, 17, 24, 27, 33, 34, 36, 43, 47, 51, 58, 61, 62, 63, 65]

CHANNEL = ['R_RA', 'L_RA', 'R_EO', 'L_EO', 'R_LES', 'L_LES', 'R_ESI', 'L_ESI', 'R_MTF', 'L_MTF', 'R_GMED', 'L_GMED', 'R_RF', 'L_RF', 'R_SM', 'L_SM']
CHANNEL= sorted(CHANNEL)
SESSION = ['INI','REL']
EVENT_GAIT = {0 : 'RHS', 1:'RTO', 2: 'LHS', 3:'LTO',-1:''}
EVENT_GAIT_4 = {0 : 'RHS', 1:'RTO', 2: 'LHS', 3:'LTO'}
EVENT_BEND = {1: 'bending', 0:'return',-1:''}
EVENT_BEND_2 = {1: 'bending', 0:'return'}
BM = ['BMu22', 'BMu23', 'BMu24', 'BMu81', 'BMu82', 'BMu97 ', 'BMu98 ',
       'BMu110', 'BMu111 ', 'BMu112 ', 'BMu113', 'BMu29', 'BMu34',
       'BMu37', 'BMu38', 'BMu40', 'BMu41', 'BMu51', 'BMu52', 'BMu53',
       'BMu67', 'BMu68', 'BMu102', 'BMu105', 'BMu108', 'BMu104', 'BMu107',
       'BMu103', 'BMu106', 'BMu100', 'BMu101', 'BMu1', 'BMu15', 'BMu69',
       'BMu35', 'BMu115', 'BMu114', 'BMu118', 'BMu44', 'BMu43', 'BMu91',
       'BMu42', 'BMu86', 'BMu88', 'BMu87', 'BMu117', 'BMu119', 'BMu109 ',
       'BMu36', 'BMu96']

# PATH_REPO = """C:/Users/Anais/Documents/NSLBP/Data/"""
PATH_REPO = """C:\\Users\\Anais\\Documents\\NSLBP\\Data\\""" # used to extract c3d files


TASKS_NAME = ['Endurance_Ito', 'Endurance_Sorensen', 'Gait_Fast', 'Gait_Normal', 'Gait_Slow' ,'Perturbation_L_Shoulder',
 'Perturbation_R_Shoulder', 'Posture_Sitting', 'Posture_Standing',
 'S2S_Constrained', 'S2S_Unconstrained', 'Swing_L_Leg',
 'Swing_R_Leg', 'Trunk_Forward', 'Trunk_Lateral', 'Trunk_Rotation',
 'Weight_Constrained', 'Weight_Unconstrained', 'sMVC_L_Gmed', 'sMVC_L_Rf',
 'sMVC_L_Semiten', 'sMVC_R_Gmed', 'sMVC_R_Rf', 'sMVC_R_Semiten']

TASKS_NAME_2 = ['Endurance_Ito', 'Endurance_Sorensen', 'Gait_Fast', 'Gait_Normal', 'Gait_Slow' ,'Perturbation_L_Shoulder',
 'Perturbation_R_Shoulder', 'Posture_Sitting', 'Posture_Standing',
 'S2S_Constrained', 'S2S_Unconstrained', 'Swing_L_Leg',
 'Swing_R_Leg', 'Trunk_Forward', 'Trunk_Lateral', 'Trunk_Rotation',
 'Weight_Constrained', 'Weight_Unconstrained']

TASKS_NAME_EVENT = ['Gait_Fast', 'Gait_Normal', 'Gait_Slow' ,
 'S2S_Constrained', 'S2S_Unconstrained', 'Trunk_Forward', 'Trunk_Lateral', 'Trunk_Rotation',
 'Weight_Constrained', 'Weight_Unconstrained']

TASK_STUDIED_DYNAMIC = [ 'Gait_Normal', 'S2S_Unconstrained', 'Trunk_Forward', 'Trunk_Lateral', 'Weight_Unconstrained']
TASK_STUDIED = [ 'Gait_Normal','Perturbation_R_Shoulder', 'S2S_Unconstrained', 'Trunk_Forward', 'Trunk_Lateral', 'Weight_Unconstrained','Posture_Sitting', 'Posture_Standing','Endurance_Ito', 'Endurance_Sorensen']
TASKS_NAME_EVENT_MVIC = ['Endurance_Ito', 'Endurance_Sorensen']
TASK_SYM =  [ 'Gait_Normal', 'S2S_Unconstrained', 'Trunk_Forward', 'Weight_Unconstrained','Posture_Sitting', 'Posture_Standing','Endurance_Ito', 'Endurance_Sorensen'] #w/o  'Trunk_Lateral' w/o 'Perturbation_R_Shoulder',

COLOR_P = sns.color_palette("RdYlBu", 14).as_hex()
COLOR = {'P':COLOR_P[3],'PR':COLOR_P[2],'PL':COLOR_P[4],'C':COLOR_P[-4],'CR':COLOR_P[-3],'CL':COLOR_P[-5]}

""" 
EDA:

REL, subject 4: not found
REL, subject 15: not found
REL, subject 18: not found
REL, subject 19: not found
REL, subject 20: not found
REL, subject 31: not found
REL, subject 37: not found
INI, subject 59: not found
REL, subject 59: not found

"""

