
from functions import *

import sys

# def create_summary_BM(BMu_nb,save_html=True,replace_html=True,ini_only=True,group_muscle = True,save_png = False, trial = None):
#     plt.close('all')
#     BMu_function, preprocessing,muscles,task, mvc_computation,mvc_task,contraction_computation,event_computation,event_type,kwargs = BMu_dict_final['BMu'+str(BMu_nb)]
#     try:
#         author=kwargs['desc']
#     except:
#         author=''
#     exists_html = False
#     repo_html = "summary_sides"
#     if save_html:
#         repo_img = f'C:\\Users\\Anais\\Documents\\images_rapports_python\\{repo_html}\\Report BMu{BMu_nb}{f"{author}" if author != "" else ""}{f"_{trial}" if trial != None else ""}_files\\' 
#         if not exists(repo_img):
#             os.mkdir(repo_img)
    
#     if exists(f'C:\\Users\\Anais\\Documents\\images_rapports_python\\{repo_html}\\Report BMu {BMu_nb}{f"{author}" if author != "" else ""}{f"_{trial}" if trial != None else ""} .html'):
#         if replace_html == False:
#             print('exit')
#             return 0
#         else: 
#             exists_html = True
        

#     else:
#         exists_html = False
#     df_units = pd.read_csv('../support doc/Units.csv',header=None)
#     df_units = df_units.rename(columns = {0:'BMu',1:'Unit'})
#     unit = df_units.loc[df_units['BMu']==f'BMu{BMu_nb}','Unit'].values[0]

#     # ---------------------------------------------------
#     # # Enter the number of the BMu and run all 
#     # -----------------------------------------------------
#     BMu_values,BMu_detailed,BMu_error  = load_results_presentation(BMu_nb,trial=trial,author=author)
#     text_table_shape = f'The shape of the table for BMu values is {BMu_values.shape} \nThe shape of the table for the detailed values for each subphase is {BMu_detailed.shape} \nThe shape of the table containing the error that occured while computing the BMu is {BMu_error.shape}'

#     #one subject test
#     subject = 3
#     session = 'INI'
#     trial_sub = 1

#     emg = load_file_subject_mvt_session2(subject,task,session,n=trial_sub)[muscles[0]]
#     kwargs = {'nb_BM': BMu_nb,'muscle':muscles[0], 'session':session,'subject':subject}


#     if event_computation: event = load_event_subject_mvt_session2(subject,task,session,n=trial_sub)
#     else: event = None

#     if mvc_computation:
#         sub_MVIC = load_file_subject_mvt_session2(subject,mvc_task,session,n=trial_sub)[muscles[0]]

#         try:
#             sub_MVIC_event = load_event_subject_mvt_session2(subject,mvc_task,session,n=trial_sub)
#         except Exception as e:
#             sub_MVIC_event = pd.DataFrame(data=None,columns=['event','time_sec','time_min'])
#             print('no subMVIC')

#     else : sub_MVIC,sub_MVIC_event = None,None

#     output, detailed_output,output_rel = BMu_function(emg,event,sub_MVIC,sub_MVIC_event,**kwargs)
#     try:
#         text_computed_BM = f'The value of the computed Biomarker is {output:0.6f} {unit} '
#     except TypeError:
#         text_computed_BM = f'The value of the computed Biomarker is None '
#     try:
#         text_current_BM = f"The current saved value of BM is: {BMu_values.loc[list((BMu_values['subject']==subject) & (BMu_values['session']==session)& (BMu_values['channel']==muscles[0]) ).index(True),:]['value']:0.6f} {unit}"
#     except:
#         text_current_BM = 'None'
#         pass
#     if task == 'Gait_Normal':
        
#         # Grouping columns and applying agg function
#         group = BMu_values.groupby(['BM','subject','P_C','session','task','channel'], as_index=False)
#         BMu_values = group.agg({'value':np.mean})

#         # Joining res column and values
#         BMu_values.columns = list(map(''.join, BMu_values.columns.values))
#         text_table_shape = f'The shape of the table for BMu values is {BMu_values.shape} \nThe shape of the table for the detailed values for each subphase is {BMu_detailed.shape} \nThe shape of the table containing the error that occured while computing the BMu is {BMu_error.shape}'

#     ## Display results
#     BMu_P,BMu_C, std_p,std_c, mean_p,mean_c,mean_diff = extract_values_BMu_PC(BMu_values,outlier_present=True)
#     text_mean_w_outliers =  f'With outliers: \nThe mean value for patients is {mean_p:0.5f} +/- {std_p:0.5f} {unit},\nThe mean value for controls is {mean_c:0.5f} +/- {std_c:0.5f} {unit}.\nThus the difference between means is {mean_diff:0.5f} {unit}'


#     BMu_P1,BMu_C1, std_p1,std_c1, mean_p1,mean_c1,mean_diff1 = extract_values_BMu_PC(BMu_values,outlier_present=False)
#     text_mean_wo_outliers = f'\nWithout outliers:\nThe mean value for patients is {mean_p1:0.5f} +/- {std_p1:0.5f} {unit},\nThe mean value for controls is {mean_c1:0.5f} +/- {std_c1:0.5f} {unit}. \nThus the difference between means is {mean_diff1:0.5f} {unit}'

#     text_mean_w_outliers_rl = ''
#     for lr in [True,False]:
#         BMu_Pr,BMu_Cr, std_pr,std_cr, mean_pr,mean_cr,mean_diffr = extract_values_BMu_PC_RL(BMu_values,outlier_present=True,left=lr)
#         BMu_Pro,BMu_Cro, std_pro,std_cro, mean_pro,mean_cro,mean_diffro = extract_values_BMu_PC_RL(BMu_values,outlier_present=False,left=lr)

#         text_mean_w_outliers_rl +=  f'{"LEFT" if lr else "<br><br>RIGHT"} with outliers (without): <br>The mean value for patients is {mean_pr:0.5f} +/- {std_pr:0.5f} {unit} ({mean_pro:0.5f} +/- {std_pro:0.5f} {unit}),<br>The mean value for controls is {mean_cr:0.5f} +/- {std_cr:0.5f} {unit} ({mean_cro:0.5f} +/- {std_cro:0.5f} {unit} ).<br>Thus the difference between means is {mean_diffr:0.5f} {unit} ({mean_diffro:0.5f} {unit})'

#     # try:
#     #     df_expected = pd.read_csv('../support doc/Expected_value.csv')
#     #     _, pP = mannwhitneyu(BMu_values.loc[BMu_values['P_C']=='P','value'], [df_expected.loc[df_expected['BM']==f'BMu{BMu_nb}','patient'].values[0]])
#     #     _, pC = mannwhitneyu(BMu_values.loc[BMu_values['P_C']=='C','value'], [df_expected.loc[df_expected['BM']==f'BMu{BMu_nb}','control'].values[0]])
#     #     print(f"The expected mean value for patient was {df_expected.loc[df_expected['BM']==f'BMu{BMu_nb}','patient'].values[0]} and {df_expected.loc[df_expected['BM']==f'BMu{BMu_nb}','control'].values[0]} for controls")
#     #     for group,p in zip(['PATIENTS','CONTROLS'],[pP,pC]):
#     #         print(f"Mean for {group} IS {'NOT' if p<0.05 else ''} (p = {p:0.2f} {'<=' if p<0.05 else '>' } 0.05) significantly different from the original value")
#     # except Exception as e :
#     #     print(e)

#     text_nb_missing_value = f'The number of missing values is: {np.sum(pd.isna(BMu_values["value"]))} it corresponds to {np.sum(pd.isna(BMu_values["value"])) / BMu_values.shape[0]*100:3.4}% of the values'

#     ## BMu Values
#     ### With outliers
#     text_nb_outliers = f'The number of outliers is {np.sum(BMu_remove_outlier_PC(BMu_values))}, it corresponds to {np.sum(BMu_remove_outlier_PC(BMu_values)) / BMu_values.shape[0]*100:3.4}% of the values'

#     box_values_BMu(BMu_values,outlier_present = True,save=save_html,name='boxplot_w_outlier',repo=repo_img,unit=unit) # if not used,comment the line to save some time
#     ### Without outliers
#     box_values_BMu(BMu_values,outlier_present = False,save=save_html,name='boxplot_wo_outlier',repo=repo_img,unit=unit)

#     if ini_only: BMu_values_val = BMu_values.loc[BMu_values['session']=='INI',:].copy(deep=True)
#     else: BMu_values_val = BMu_values


#     if BMu_nb not in [44,43,91,42,35,15]:
#         RL_bool = True  
#     else:
#         RL_bool = False
#     normality = check_normal_distribution_P_C(BMu_values_val,outlier_present=True,show=False) 
#     if False in normality :
#         pPCo,pRLo,pRLCo,pRLPo,pPCRo,pPCLo,do = comparison_PC_RL(BMu_values_val,test="mannwhitneyu", outlier_present=True, group_muscle=group_muscle,show =False, RL = RL_bool)
#     else:
#         pPCo,pRLo,pRLCo,pRLPo,pPCRo,pPCLo,do = comparison_PC_RL(BMu_values_val, test='ttest', outlier_present=True, group_muscle=group_muscle,show =False, RL = RL_bool)
#     if False in normality :
#         pPC,pRL,pRLC,pRLP,pPCR,pPCL,d = comparison_PC_RL(BMu_values_val,test="mannwhitneyu", outlier_present=False,group_muscle=group_muscle, show =False, RL = RL_bool)
#     else:
#         pPC,pRL,pRLC,pRLP,pPCR,pPCL,d = comparison_PC_RL(BMu_values_val, test='ttest', outlier_present=False, group_muscle=group_muscle,show =False, RL = RL_bool)


#     ## Missing values
#     bool_missing = pd.isna(BMu_values["value"])
#     df_missing = BMu_values.loc[bool_missing,:]
#     ## BMu outliers
#     df_outliers_BM = BMu_values[BMu_remove_outlier_PC(BMu_values)].reset_index().drop('index',axis=1)
 
#     def save_png_outliers(BMu_values,BMu_nb,filt = True):
#         df_outliers = BMu_values[BMu_remove_outlier_PC(BMu_values)].reset_index().drop('index',axis=1)
#         task = df_outliers.loc[0,'session']
#         mus = df_outliers.loc[0,'channel'][2:]

#         for i,row in df_outliers.iterrows():
#             if filt:
#                 fig,ax = plt.subplots(2,1,figsize=((8,6)))
#             else:
#                 fig,ax = plt.subplots(figsize=((8,3)))
#                 ax = [ax]

#             subject = row['subject']
#             group = row['P_C']
#             session = row['session']
#             channel = row['channel']
#             val = row['value']
#             task = row['task']
#             trial = row['repetition']

#             emg = load_file_subject_mvt_session2(subject,task,session,n=trial)[channel]

#             ax[0].plot(np.arange(len(emg))/SAMPLING_RATE,emg)
#             ax[0].set(title = f'S: {subject}, session: {session}, channel: {channel} BM = {val:3.5f}',xlabel='time (s)',ylabel=f'amplitude ({unit})')
#             ax[1].set(title = f'Filtered EMG',xlabel='time (s)',ylabel=f'amplitude ({unit})')


#             if filt:
#                 filter_emg = preprocessing(emg)
#                 ax[1].plot(np.arange(len(filter_emg))/SAMPLING_RATE,filter_emg)

#             fig.tight_layout()

#             path_folder = f"C:\\Users\\Anais\\Documents\\images_rapports_python\\outliers\\{task}_{mus}_{BMu_nb}{author}\\"
#             if not exists(path_folder):
#                 os.mkdir(path_folder)
                
#             fig.savefig(path_folder+f"{subject}{group}_{session}_{channel}.png")
#             plt.close('all')

#     if save_png:
#         save_png_outliers(BMu_values,BMu_nb)

#     ## BMu detailed values
#     def box_detailed_values_BMu(BMu_detailed,outliers = False):
#         fig1,ax1 = plt.subplots(figsize = (7,4))
#         sns.boxplot(x="event", y="value", hue="P_C", data=BMu_detailed, showfliers=outliers,ax=ax1)  
#         ax1.set(ylabel = 'value')
#         fig1.tight_layout()
#     box_detailed_values_BMu(BMu_detailed) #by default without outliers

#     def text_sig_from_p(pPC,pRL,pRLC,pRLP,pPCR,pPCL,d):
#         text = ""
#         for p,t in zip([pPC,pPCR,pPCL,pRL,pRLC,pRLP,],['control and lbp','right side of control and lbp','left side of control and lbp','right and left of all','right and left of control','right and left of lbp']):
#                     if p != None and p <= 0.05 :
#                         text += f"<br>The difference between {t.upper()} patients IS significant, p = {p:0.4f} <= 0.05{f', Cohen s d = {d:0.2f}' if t == 'control and lbp' else ''  } "
#                     elif p != None and p > 0.05:
#                         text += f"<br>The difference between {t.upper()} patients IS NOT significant, p = {p:0.4f} > 0.05{f', Cohen s d = {d:0.2f}' if t == 'control and lbp' else ''  } "
#                     else:
#                         pass

#         return text
    

#     def one_sample_per_subphase(BMu_detailed):
#         output = {'BMu':[],'subject':[],'P_C':[],'session':[],'task':[],'channel':[],'value':[],'event':[]}
#         task = BMu_detailed.loc[0,'task']
#         BMu_number = BMu_detailed.loc[0,'BMu']
#         for sub in BMu_detailed['subject'].unique():
#             BMu_sub = BMu_detailed.loc[BMu_detailed['subject']==sub,:]
#             for ses in BMu_sub['session'].unique():
#                 BMu_grouped_ses = BMu_sub.loc[BMu_sub['session']==ses,:]
#                 for ch in BMu_grouped_ses['channel'].unique():
#                     for ev in BMu_grouped_ses['event'].unique():
#                         output['value'] += [BMu_grouped_ses.loc[(BMu_grouped_ses['channel'] == ch) & (BMu_grouped_ses['event'] == ev),'value'].mean()]
#                         output['event'] += [ev]
#                         output ['channel'] += [ch]
#                         output['task'] += [task]
#                         output['session'] += [ses]
#                         output['P_C'] += [BMu_sub.loc[:,'P_C'].values[0]]
#                         output['subject'] += [sub]
#                         output['BMu'] += [BMu_number]

#         df_ouput = pd.DataFrame(output)
#         return df_ouput
    

#     def function_text_detailed_sig(BMu_detailed,ini_only=True,group_muscle=True,group_subphase=True):
#         if group_subphase:
#             BMu_detailed_ = one_sample_per_subphase(BMu_detailed.copy(deep=True))
#         else:
#             BMu_detailed_ = BMu_detailed

#         if ini_only: BMu_detailed_val = BMu_detailed_.loc[BMu_detailed_['session']=='INI',:].copy(deep=True)
#         else: BMu_detailed_val = BMu_detailed_

#         if len(list(BMu_detailed_val['event'].unique())) >1 : #ie if there are event to look at
#             text_detailed_sig = ''
#             for out in [True,False]:
#                 text_detailed_sig += f'<br>* WITH{"OUT" if not out else ""} OUTLIERS *<br>'
#                 for ev in list(BMu_detailed_val['event'].unique()):
#                     BMu_df = BMu_detailed_val.loc[BMu_detailed_val['event'] == ev,:]
#                     normalityd = check_normal_distribution_P_C(BMu_df,outlier_present=True,show=False) 

#                     if False in normalityd:
#                         pPCd,pRLd,pRLCd,pRLPd,pPCRd,pPCLd,dd = comparison_PC_RL(BMu_df,test="mannwhitneyu", outlier_present=out, show =False,group_muscle=group_muscle, RL = True if BMu_nb not in [44,43,91,42,35,15]  else False )
#                     else:
#                         pPCd,pRLd,pRLCd,pRLPd,pPCRd,pPCLd,dd = comparison_PC_RL(BMu_df, test='ttest', outlier_present=out, show =False,group_muscle=group_muscle, RL = True if BMu_nb not in [44,43,91,42,35,15]  else False)
#                     text_detailed_sig += f'<br>{ev.upper()} <br>{text_sig_from_p(pPCd,pRLd,pRLCd,pRLPd,pPCRd,pPCLd,dd)}<br>'
#         else:
#             text_detailed_sig = 'no events'

#         return text_detailed_sig
    
#     text_detailed_sig = function_text_detailed_sig(BMu_detailed,group_subphase = False)
#     BMu_detailed["P_C_channel"] = BMu_detailed['P_C'] + '_' + BMu_detailed["channel"]
#     fig,ax = plt.subplots(figsize =(12,3))
#     sns.boxplot(x="event", y="value", hue="P_C_channel"  , data=BMu_detailed,showfliers=False, palette =[COLOR['CL'],COLOR['PL'],COLOR['CR'],COLOR['PR']],  hue_order=[f'C_{muscles[0]}',f'P_{muscles[0]}',f'C_{muscles[1]}',f'P_{muscles[1]}'])  
#     ax.set(ylabel =f'Values ({unit})')
#     sns.move_legend(ax, "upper left", bbox_to_anchor=(1, 1))

#     fig.tight_layout()
#     if save_html:
#         fig.savefig(f'{repo_img}boxplot_detailed_wo_outliers.png')
   
#     try:
#         all_files_detailed_rel = glob(f'results\\detailed_rel\\BMu{str(BMu_nb)}{author}_{get_last_nb(BMu_nb,author)}*.csv')
#         BMu_detailed_rel_csv = all_files_detailed_rel[-1]
#         BMu_detailed_rel =pd.read_csv(BMu_detailed_rel_csv)
#     except Exception as e:
#         raise Exception('no output_rel')

#     # 1. Set up multiple variables to store the titles, text within the report
#     page_title_text=f'Report BMu{BMu_nb}{f"{author}" if author != "" else ""}{f"_{trial}" if trial != None else ""}'
#     title_text = f'BMu {BMu_nb}{f"{author}" if author != "" else ""}{f"_{trial}" if trial != None else ""} '
#     text = f'SUMMARY :<br>Preprocessing :{preprocessing.__name__} <br>Muscles :{muscles} <br>Task :{task} <br>MVIC : {mvc_computation,mvc_task} <br>CONTRACTION :{contraction_computation} <br>EVENT :{event_computation,event_type}'
#     #table_shape 
#     text_significance_w_outliers = text_sig_from_p(pPCo,pRLo,pRLCo,pRLPo,pPCRo,pPCLo,do)
#     text_significance_wo_outliers = text_sig_from_p(pPC,pRL,pRLC,pRLP,pPCR,pPCL,d)

#                 # {df_test.to_html()}


#     # 2. Combine them together using a long f-string
#     html = f'''
#         <html>
#             <head>
#                 <title>{page_title_text}</title>
#             </head>
#             <body>
#                 <h1>{title_text}</h1>
#                 <p>{text}</p>
#                 <p>{text_current_BM}{text_computed_BM} <br><p>
#                 <p>{text_table_shape}<br></p>
#                 <p>{text_nb_missing_value}</p>
#                 <p>{text_nb_outliers}</p>
#                 <p>{text_mean_w_outliers}</p>
#                  <p>{text_mean_wo_outliers}</p>
#                 <h2> For each side </h2>
#                 <p>{text_mean_w_outliers_rl}</p>
#                 <br>
#                 <h2>With outliers</h2>

#                 <p>{text_significance_w_outliers}</p>
#                 <h2>Without outliers</h2>

#                 <p>{text_significance_wo_outliers}</p>
#                 <h2>With outliers</h2>
#                 <img src="{repo_img}boxplot_w_outlier.png" alt="w out">
                
#                 <h2>Without outliers</h2>
#                 <img src="{repo_img}boxplot_wo_outlier.png" alt="wo out">
#                 <br>
#                 <p>{text_detailed_sig}</p>
#                 <br>

#                 <p> With grouped subphases <br> {function_text_detailed_sig(BMu_detailed,group_subphase = True)}</p>
#                 <img src="{repo_img}boxplot_detailed_wo_outliers.png" alt="w out">
#                 <br>
#                 {BMu_values.head(15).to_html()}
#                 <br>
#                 {BMu_detailed.head(15).to_html()}
#                 <br>
#                 {BMu_error.to_html()}
#                 <br>
#                 OUTLIERS<br>
#                 {df_outliers_BM.to_html()}
#                 <br>
#                 {BMu_detailed_rel.to_html()}
#             </body>
#         </html>
#         '''

#     if save_html and (replace_html | (not exists_html)) :
#         path = f"C:\\Users\\Anais\\Documents\\images_rapports_python\\{repo_html}\\"
#         with open(path + f'report {title_text}.html', 'w') as f:
#             f.write(html)

#     # 3. Write the html string as an HTML file
#     with open('html_report.html', 'w') as f:
#         f.write(html)



#     # filename = 'C:\\Users\\Anais\\Studio Code\\Project 1\\nslbp-bio_toolbox2\\html_report.html'
#     # open_new_tab(filename)




def create_summary_BM(BMu_nb,repo_html,dict_final_bool=True,factor=3,save_html=True,replace_html=False,ini_only=True,save_png=False,trial=None):

 
    plt.close('all')
    BMu_function, preprocessing,muscles,task, mvc_computation,mvc_task,contraction_computation,event_computation,event_type,kwargs = BMu_dict_final['BMu'+str(BMu_nb)]
    try:
        author=kwargs['desc']
    except:
        author=''
    
    # initialization
    exists_html = False

    if save_html:
        repo_img = f'C:\\Users\\Anais\\Documents\\images_rapports_python\\{repo_html}\\Report BMu{BMu_nb}{f"{author}" if author != "" else ""}{f"_{trial}" if trial != None else ""}_files\\' 
        if not exists(repo_img):
            os.mkdir(repo_img)
    
    if exists(f'C:\\Users\\Anais\\Documents\\images_rapports_python\\{repo_html}\\Report BMu {BMu_nb}{f"{author}" if author != "" else ""}{f"_{trial}" if trial != None else ""} .html'):
        if replace_html == False:
            print('exit')
            return 0
        else: 
            exists_html = True

    else:
        exists_html = False

    df_units = pd.read_csv('../support doc/Units.csv',header=None)
    df_units = df_units.rename(columns = {0:'BMu',1:'Unit'})
    unit = df_units.loc[df_units['BMu']==f'BMu{BMu_nb}','Unit'].values[0]



    # ---------------------------------------------------

    #load file
    BMu_values,BMu_detailed,BMu_error  = load_results_presentation(BMu_nb,trial=trial,author=author)
    text_table_shape = f'The shape of the table for BMu values is {BMu_values.shape} \nThe shape of the table for the detailed values for each subphase is {BMu_detailed.shape} \nThe shape of the table containing the error that occured while computing the BMu is {BMu_error.shape}'

    ## one subject test
    subject = 1
    session = 'INI'
    trial_sub = 1
    
    # load file
    emg = load_file_subject_mvt_session2(subject,task,session,n=trial_sub)[muscles[0]]
    kwargs = {'nb_BM': BMu_nb,'muscle':muscles[0], 'session':session,'subject':subject}

    if event_computation: event = load_event_subject_mvt_session2(subject,task,session,n=trial_sub)
    else: event = None

    if mvc_computation:
        sub_MVIC = load_file_subject_mvt_session2(subject,mvc_task,session,n=trial_sub)[muscles[0]]

        try:
            sub_MVIC_event = load_event_subject_mvt_session2(subject,mvc_task,session,n=trial_sub)
        except Exception as e:
            print(e)
            sub_MVIC_event = pd.DataFrame(data=None,columns=['event','time_sec','time_min'])

    else : sub_MVIC,sub_MVIC_event = None,None

    # compute Bmu
    output, detailed_output,output_rel = BMu_function(emg,event,sub_MVIC,sub_MVIC_event,**kwargs)

    try:
        text_computed_BM = f'The value of the computed Biomarker is {output:0.6f} {unit} '
    except TypeError:
        text_computed_BM = f'The value of the computed Biomarker is None '

    # get the saved value of the Bmu to check if the last processing and the one last used match
    try:
        text_current_BM = f"The current saved value of BM is: {BMu_values.loc[list((BMu_values['subject']==subject) & (BMu_values['session']==session)& (BMu_values['channel']==muscles[0]) ).index(True),:]['value']:0.6f} {unit}"
    except:
        text_current_BM = 'None'
        pass

    if task == 'Gait_Normal':
        
        # Grouping columns and applying agg function
        group = BMu_values.groupby(['BM','subject','P_C','session','task','channel'], as_index=False)
        BMu_values = group.agg({'value':np.mean})

        # Joining res column and values
        BMu_values.columns = list(map(''.join, BMu_values.columns.values))
        text_table_shape = f'The shape of the table for BMu values is {BMu_values.shape} \nThe shape of the table for the detailed values for each subphase is {BMu_detailed.shape} \nThe shape of the table containing the error that occured while computing the BMu is {BMu_error.shape}'

    ## BMu results for all participants
    BMu_P,BMu_C, std_p,std_c, mean_p,mean_c,mean_diff = extract_values_BMu_PC_RL(BMu_values,outlier_present=True,desc=author,factor=factor,both = True)
    text_mean_w_outliers =  f'With outliers: \nThe mean value for patients is {mean_p:0.5f} +/- {std_p:0.5f} {unit},\nThe mean value for controls is {mean_c:0.5f} +/- {std_c:0.5f} {unit}.\nThus the difference between means is {abs(mean_diff):0.5f} {unit}'

    BMu_P1,BMu_C1, std_p1,std_c1, mean_p1,mean_c1,mean_diff1 = extract_values_BMu_PC_RL(BMu_values,outlier_present=False,desc=author,factor=factor,both = True)
    text_mean_wo_outliers = f'\nWithout outliers:\nThe mean value for patients is {mean_p1:0.5f} +/- {std_p1:0.5f} {unit},\nThe mean value for controls is {mean_c1:0.5f} +/- {std_c1:0.5f} {unit}. \nThus the difference between means is {abs(mean_diff1):0.5f} {unit}'

    text_mean_w_outliers_rl = ''

    for lr in [True,False]:
        BMu_Pr,BMu_Cr, std_pr,std_cr, mean_pr,mean_cr,mean_diffr = extract_values_BMu_PC_RL(BMu_values,outlier_present=True,left=lr,desc=author,factor=factor)
        BMu_Pro,BMu_Cro, std_pro,std_cro, mean_pro,mean_cro,mean_diffro = extract_values_BMu_PC_RL(BMu_values,outlier_present=False,left=lr,desc=author,factor=factor)

        text_mean_w_outliers_rl +=  f'{"LEFT" if lr else "<br><br>RIGHT"} with outliers (without): <br>The mean value for patients is {mean_pr:0.5f} +/- {std_pr:0.5f} {unit} ({mean_pro:0.5f} +/- {std_pro:0.5f} {unit}),<br>The mean value for controls is {mean_cr:0.5f} +/- {std_cr:0.5f} {unit} ({mean_cro:0.5f} +/- {std_cro:0.5f} {unit} ).<br>Thus the difference between means is {abs(mean_diffr):0.5f} {unit} ({abs(mean_diffro):0.5f} {unit})'

    text_nb_missing_value = f'The number of missing values is: {np.sum(pd.isna(BMu_values["value"]))} it corresponds to {np.sum(pd.isna(BMu_values["value"])) / BMu_values.shape[0]*100:3.4}% of the values'

    ## Boxplot 
    # With outliers
    text_nb_outliers = f'The number of outliers is {np.sum(BMu_remove_outlier_PC(BMu_values,factor=3))}, it corresponds to {np.sum(BMu_remove_outlier_PC(BMu_values,factor=factor)) / BMu_values.shape[0]*100:3.4}% of the values'
    box_values_BMu(BMu_values,outlier_present = True,save=(save_html & replace_html),name='boxplot_w_outlier',repo=repo_img,unit=unit,factor=factor) # if not used,comment the line to save some time
    box_values_BMu_llbp_hlbp(BMu_values,outlier_present=True,save=(save_html & replace_html),repo=repo_img,name='boxplot_w_outlier_group',unit=unit,factor=factor)

    # Without outliers
    box_values_BMu(BMu_values,outlier_present = False,save=(save_html & replace_html),name='boxplot_wo_outlier',repo=repo_img,unit=unit,factor=factor)
    box_values_BMu_llbp_hlbp(BMu_values,outlier_present=False,save=(save_html & replace_html),repo=repo_img,name='boxplot_wo_outlier_group',unit=unit,factor=factor)

    if ini_only: BMu_values_val = BMu_values.loc[BMu_values['session']=='INI',:].copy(deep=True)
    else: BMu_values_val = BMu_values

    ## Validity

    if (BMu_nb not in [44,43,91,42,35,15]) or (author=='_rl'):
        RL_bool = True  
    else:
        RL_bool = False


    normality = check_normal_distribution_P_C(BMu_values_val,outlier_present=True,factor=factor) 
    # if False in normality :
    #     dict_sig = comparison_PC_RL_HLLBP(BMu_values_val,test="mannwhitneyu", outlier_present=True,factor=factor, show =True, RL = RL_bool)
    # else:
    #     dict_sig = comparison_PC_RL_HLLBP(BMu_values_val, test='ttest', outlier_present=True,factor=factor, show =True, RL = RL_bool)

    # pPCo,pRLo,pRLCo,pRLPo,pPCRo,pPCLo = dict_sig[('LBP','C','RL')],dict_sig[('R','L','PC')],dict_sig[('R','L','C')],dict_sig[('R','L','P')],dict_sig[('LBP','C','R')],dict_sig[('LBP','C','L')]
    # if False in normality :
    #     dict_sig = comparison_PC_RL_HLLBP(BMu_values_val,test="mannwhitneyu", outlier_present=False,factor=factor, show =True, RL = RL_bool)
    # else:
    #     dict_sig = comparison_PC_RL_HLLBP(BMu_values_val, test='ttest', outlier_present=False,factor=factor, show =True, RL = RL_bool)

    # pPC,pRL,pRLC,pRLP,pPCR,pPCL = dict_sig[('LBP','C','RL')],dict_sig[('R','L','PC')],dict_sig[('R','L','C')],dict_sig[('R','L','P')],dict_sig[('LBP','C','R')],dict_sig[('LBP','C','L')]
    
    
    text_dict_sig_w=comparison_PC_RL_HLLBP(BMu_values_val,show=False,test='mannwhitneyu' if False in normality else 'ttest',RL=RL_bool,outlier_present=True,factor=factor,text=True)
    text_dict_sig_wo=comparison_PC_RL_HLLBP(BMu_values_val,show=False,test='mannwhitneyu'if False in normality else 'ttest',RL=RL_bool,outlier_present=False,factor=factor,text=True)


    ## Missing values
    bool_missing = pd.isna(BMu_values["value"])
    df_missing = BMu_values.loc[bool_missing,:]

    ## BMu outliers
    df_outliers_BM = BMu_values[BMu_remove_outlier_PC(BMu_values,factor=factor)].reset_index().drop('index',axis=1)

    def save_png_outliers(BMu_values,BMu_nb,filt = True):
        df_outliers = BMu_values[BMu_remove_outlier_PC(BMu_values,factor=factor)].reset_index().drop('index',axis=1)
        task = df_outliers.loc[0,'session']
        mus = df_outliers.loc[0,'channel'][2:]

        for i,row in df_outliers.iterrows():
            if filt:
                fig,ax = plt.subplots(2,1,figsize=((8,6)))
            else:
                fig,ax = plt.subplots(figsize=((8,3)))
                ax = [ax]

            subject = row['subject']
            group = row['P_C']
            session = row['session']
            channel = row['channel']
            val = row['value']
            task = row['task']
            trial = row['repetition']

            emg = load_file_subject_mvt_session2(subject,task,session,n=trial)[channel]

            ax[0].plot(np.arange(len(emg))/SAMPLING_RATE,emg)
            ax[0].set(title = f'S: {subject}, session: {session}, channel: {channel} BM = {val:3.5f}',xlabel='time (s)',ylabel=f'amplitude ({unit})')
            ax[1].set(title = f'Filtered EMG',xlabel='time (s)',ylabel=f'amplitude ({unit})')


            if filt:
                filter_emg = preprocessing(emg)
                ax[1].plot(np.arange(len(filter_emg))/SAMPLING_RATE,filter_emg)

            fig.tight_layout()

            path_folder = f"C:\\Users\\Anais\\Documents\\images_rapports_python\\outliers\\{task}_{mus}_{BMu_nb}{author}\\"
            if not exists(path_folder):
                print('create')
                os.mkdir(path_folder)
                
            fig.savefig(path_folder+f"{subject}{group}_{session}_{channel}.png")
            print(path_folder)
            plt.close('all')
  
    if save_png:
        save_png_outliers(BMu_values,BMu_nb)


    # BMu detailed values
   

    def text_sig_from_p(pPC,pRL,pRLC,pRLP,pPCR,pPCL,d=''):
        text = ""
        for p,t in zip([pPC,pPCR,pPCL,pRL,pRLC,pRLP],['control and lbp','right side of control and lbp','left side of control and lbp','right and left of all','right and left of control','right and left of lbp']):
                    if p != None and p <= 0.05 :
                        text += f"<br>The difference between <span style= 'background-color: #E0F0D1'> {t.upper()} patients IS significant </span>, p = {p:0.4f} <= 0.05{f', Cohen s d = {d:0.2f}' if (t == 'control and lbp') and (d!='') else ''  } "
                    elif p != None and p > 0.05:
                        text += f"<br>The difference between {t.upper()} patients IS NOT significant, p = {p:0.4f} > 0.05{f', Cohen s d = {d:0.2f}' if (t == 'control and lbp') and (d!='') else ''  } "
                    else:
                        pass

        return text
    
    def one_sample_per_subphase(BMu_detailed):
        output = {'BMu':[],'subject':[],'P_C':[],'session':[],'task':[],'channel':[],'value':[],'event':[]}
        task = BMu_detailed.loc[0,'task']
        BMu_number = BMu_detailed.loc[0,'BMu']

        for sub in BMu_detailed['subject'].unique():
            BMu_sub = BMu_detailed.loc[BMu_detailed['subject']==sub,:]

            for ses in BMu_sub['session'].unique():
                BMu_grouped_ses = BMu_sub.loc[BMu_sub['session']==ses,:]
                
                for ch in BMu_grouped_ses['channel'].unique():
                    for ev in BMu_grouped_ses['event'].unique():
                        output['value'] += [BMu_grouped_ses.loc[(BMu_grouped_ses['channel'] == ch) & (BMu_grouped_ses['event'] == ev),'value'].mean()]
                        output['event'] += [ev]
                        output ['channel'] += [ch]
                        output['task'] += [task]
                        output['session'] += [ses]
                        output['P_C'] += [BMu_sub.loc[:,'P_C'].values[0]]
                        output['subject'] += [sub]
                        output['BMu'] += [BMu_number]

        df_ouput = pd.DataFrame(output)
        return df_ouput
    

    def function_text_detailed_sig(BMu_detailed,ini_only=True,group_subphase=True,RL=True):
        if group_subphase:
            BMu_detailed_ = one_sample_per_subphase(BMu_detailed.copy(deep=True))
        else:
            BMu_detailed_ = BMu_detailed

        if ini_only: BMu_detailed_val = BMu_detailed_.loc[BMu_detailed_['session']=='INI',:].copy(deep=True)
        else: BMu_detailed_val = BMu_detailed_

        if len(list(BMu_detailed_val['event'].unique())) >1 : #ie if there are event to look at
            text_detailed_sig = ''
            for out in [True,False]:

                text_detailed_sig += f'<br>* WITH{"OUT" if not out else ""} OUTLIERS *<br>'
                for ev in list(BMu_detailed_val['event'].unique()):
                    BMu_df = BMu_detailed_val.loc[BMu_detailed_val['event'] == ev,:]
                    normalityd = check_normal_distribution_P_C(BMu_df,outlier_present = True,show=False) 

                    if False in normalityd :
                        dict_sig = comparison_PC_RL_HLLBP(BMu_df,test="mannwhitneyu", outlier_present=out,factor=factor, show =False, RL = RL_bool)
                    else:
                        dict_sig = comparison_PC_RL_HLLBP(BMu_df, test='ttest', outlier_present=out,factor=factor,show =False, RL = RL_bool)

                    pPCd,pRLd,pRLCd,pRLPd,pPCRd,pPCLd = dict_sig[('LBP','C','RL')],dict_sig[('R','L','PC')],dict_sig[('R','L','C')],dict_sig[('R','L','P')],dict_sig[('LBP','C','R')],dict_sig[('LBP','C','L')]
                    
                    text_detailed_sig += f'<br>{ev.upper()} <br>{text_sig_from_p(pPCd,pRLd,pRLCd,pRLPd,pPCRd,pPCLd)}<br>'
        else:
            text_detailed_sig = 'no events'

        return text_detailed_sig
    
    
    try:
        text_detailed_sig = function_text_detailed_sig(BMu_detailed,ini_only=True,group_subphase=True,RL=True)
        
    except:
        text_detailed_sig = 'No statistic for subphases'


    BMu_detailed["P_C_channel"] = BMu_detailed['P_C'] + '_' + BMu_detailed["channel"]

    fig,ax = plt.subplots(figsize =(12,3))
    sns.boxplot(x="event", y="value", hue="P_C_channel"  , data=BMu_detailed,showfliers=False, palette =[COLOR['CL'],COLOR['PL'],COLOR['CR'],COLOR['PR']],  hue_order=[f'C_{muscles[0]}',f'P_{muscles[0]}',f'C_{muscles[1]}',f'P_{muscles[1]}'],showmeans=True,
                meanprops={"marker": "+",
                        "markeredgecolor": "black",
                        "markersize": "10"}) 
    ax.set(ylabel =f'Values ({unit})')
    sns.move_legend(ax, "upper left", bbox_to_anchor=(1, 1))

    fig.tight_layout()

    if save_html & replace_html:
        fig.savefig(f'{repo_img}boxplot_detailed_wo_outliers.png')


    try:
        all_files_detailed_rel = glob(f'results\\detailed_rel\\BMu{str(BMu_nb)}{author}_{get_last_nb(BMu_nb,author)}*.csv')
        BMu_detailed_rel_csv = all_files_detailed_rel[-1]
        BMu_detailed_rel =pd.read_csv(BMu_detailed_rel_csv)
    except Exception as e:
        print(e)

    # 1. Set up multiple variables to store the titles, text within the report
    page_title_text=f'Report BMu{BMu_nb}{f"{author}" if author != "" else ""}{f"_{trial}" if trial != None else ""}'
    title_text = f'BMu {BMu_nb}{f"{author}" if author != "" else ""}{f"_{trial}" if trial != None else ""} '
    text = f'SUMMARY :<br>Preprocessing :{preprocessing.__name__} <br>Muscles :{muscles} <br>Task :{task} <br>MVIC : {mvc_computation,mvc_task} <br>CONTRACTION :{contraction_computation} <br>EVENT :{event_computation,event_type}'

    # text_significance_w_outliers = text_sig_from_p(pPCo,pRLo,pRLCo,pRLPo,pPCRo,pPCLo)
    # text_significance_wo_outliers = text_sig_from_p(pPC,pRL,pRLC,pRLP,pPCR,pPCL)
    try:
        text_sig = function_text_detailed_sig(BMu_detailed,ini_only=True,group_subphase=True,RL=True) 
    except:
        text_sig = 'Statistic error'
                # {df_test.to_html()}


    # 2. Combine them together using a long f-string
    style_css = '''   
    #header {
            background-color: #4C709C;
            color:white;
            text-align:center;
            padding:5px;
        }
    h2 {
        background-color: #7F9DC2;
        color:white;
        text-align:center;
        padding:5px;

    }
    h3 {
        color: #7F9DC2;
        font-weight: bold;

    }
    '''


    html = f'''


        <html>
            <head>
                <style>
                {style_css}
                </style>
                <title>{page_title_text}</title>
            </head>
            <body>
                <div id="header">
                <h1>{title_text}</h1>
                </div>

                <p>{text}</p>
                <p>({text_current_BM}. <br>{text_computed_BM}) <br><p>
                <p>{text_table_shape}<br></p>
                <p>{text_nb_missing_value}</p>
                <p>{text_nb_outliers}</p>
                <p>{text_mean_w_outliers}</p>
                <p>{text_mean_wo_outliers}</p>
                <h2>Results</h2>
                <p>{text_mean_w_outliers_rl}</p>
                <br>
                <h3>With outliers</h3>

                <p>{text_dict_sig_w}</p>
                <h3>Without outliers (factor = {factor})</h3>

                <p>{text_dict_sig_wo}</p>

                <h2>Boxplot</2>
        
                <h3>With outliers</h3>
                <img src="{repo_img}boxplot_w_outlier.png" alt="w out">
                <img src="{repo_img}boxplot_w_outlier_group.png" alt="w out">
                
                
                <h3>Without outliers (factor = {factor})</h3>
                <img src="{repo_img}boxplot_wo_outlier.png" alt="wo out">
                <img src="{repo_img}boxplot_wo_outlier_group.png" alt="w out">

                <br>
                <h2>Detailed results</h2>
                <h3>Validity and Boxplots</h3>
                <p>{text_detailed_sig}</p>
                <br>
                <img src="{repo_img}boxplot_detailed_wo_outliers.png" alt="w out">
                <br> 
                <h3>Dataframe (mean left side, all measures left side, every measures)<br></h3>
                <br>RESULTS<br>
                {BMu_values.head(15).to_html()}
                <br>
                {BMu_detailed_rel.head(15).to_html()}
                <br>
                {BMu_detailed.head(15).to_html()}
                <br>ERROR WHILE COMPUTING<br>
                {BMu_error.to_html()}
                <br>OUTLIERS (factor = {factor}) <br>
                {df_outliers_BM.head().to_html()}
                
            </body>
        </html>
        '''

    if save_html and (replace_html | (not exists_html)) :
        path = f"C:\\Users\\Anais\\Documents\\images_rapports_python\\{repo_html}\\"
        with open(path + f'report {title_text}.html', 'w') as f:
            f.write(html)
        print('HTML saved')

    # 3. Write the html string as an HTML file
    with open('html_report.html', 'w') as f:
        f.write(html)



            

    # filename = 'C:\\Users\\Anais\\Studio Code\\Project 1\\nslbp-bio_toolbox2\\html_report.html'
    # open_new_tab(filename)