
from functions.requirements import *
from functions.plot_function import *
from functions.c3d_function import *
from functions.processing_function import *


####################################################################################
# This file comprises all the functions related to the loading and saving of BMu
####################################################################################



# COMPUTING FUNCTION FOR ALL PARTICIPANTS


def BMu_patients_all_rel(BMu_function,nb_BMu,event_bool, mvc_bool, task,task_mvc,channel_BMu,session = 2,show = True,**args ):
    """Compute the BMu across all patient files

    Args:
        BMu_function (function): function that computes the BMu
        nb_BMu (int):number of the BMu
        event_bool (_type_): True if event have to be detected
        mvc_bool (_type_): True if MVC has to be used
        task (_type_): name of the studied task
        task_mvc (_type_): name of the studied MVC task, '' otherwise
        channel_BMu (_type_): list of the muscle studied. ex: ['L_LES','R_LES']
        session (int, optional): 'INI' or 'REL' or 2. Defaults to 2 to compute the BMu across INI and REL session
        show (bool, optional): shows the value at the end of computation. Defaults to True.

    Returns:
        Dataframe: Dataframe of the BMu values and detailed BMu values (for each subphase)
    """

    BMu_results = []
    BMu_result_detailed = []
    BMu_result_detailed_rel = []

    error = []
    assert session in ['INI','REL',2]
    assert task_mvc in ['Endurance_Sorensen','Endurance_Ito','']

    # for s in SUBJECT:
    for s in SUBJECT:

        for c in ['INI','REL' if session ==2 else session]:
            
            try:
                for nb in range(1, get_number_movement(s,task,c,format = 'csv')+1):
                    
                    file = load_file_subject_mvt_session2(s,mvt=task,session=c,n=nb)
                    
                    if event_bool == True:
                        event = load_event_subject_mvt_session2(s,task,c,n = nb)
                    else:
                        event = None
                    
                    
                    if mvc_bool:
                        sub_MVIC = load_file_subject_mvt_session2(s,task_mvc,c)
                        try:
                            sub_MVIC_event = load_event_subject_mvt_session2(s,task_mvc,c)
                        except:
                            sub_MVIC_event = pd.DataFrame(data=None,columns=['event','time_sec','time_min'])

                    else:
                        sub_MVIC_event = None

                    for ch in channel_BMu:

                        try:
                            if mvc_bool:
                                emg_sub_mvic = sub_MVIC[ch]
                            else:
                                emg_sub_mvic = None
                                
                            emg = file[ch]
                            kwargs = {'nb_BM': nb_BMu,'muscle':ch,'session':c,'subject':s}
                            
                            if args != {}:
                                for k,v in zip(list(args.keys()),list(args.values())):
                                    if k not in list(kwargs.keys()):
                                        kwargs[k]=v
                                    
                            output,output_detailed,output_rel = BMu_function(emg,event,emg_sub_mvic,sub_MVIC_event,**kwargs)
                            BMu_results += [[f'BMu{nb_BMu}',s,'P' if s in SUBJECT_P else 'C',c,task,nb,ch,output]]

                            informations = np.array([f'BMu{nb_BMu}',s,'P' if s in SUBJECT_P else 'C',c,task,nb,ch]*output_detailed.shape[0]).reshape(output_detailed.shape[0],-1)
                            df_informations = pd.DataFrame(informations,columns=['BMu','subject','P_C','session','task','trials','channel'])
                            df_detailed = pd.concat([df_informations,  output_detailed], axis=1, ignore_index=True)

                            informations_rel = np.array([f'BMu{nb_BMu}',f'P{s:02d}','PAT' if s in SUBJECT_P else 'CTR',c,task,nb,ch,'FM']*output_rel.shape[0]).reshape(output_rel.shape[0],-1)
                            df_informations_rel = pd.DataFrame(informations_rel,columns=['BMu','Participant','Group','Session','task','trials','channel','Operator'])
                            df_detailed_rel = pd.concat([df_informations_rel,  output_rel], axis=1, ignore_index=True)

                            
                            BMu_result_detailed += list(df_detailed.values)
                            BMu_result_detailed_rel += list(df_detailed_rel.values)


                            if show:
                                try:
                                    print(f"BMu{nb_BMu}, {s:02d}, {'P' if s in SUBJECT_P else 'C'} {c,task,nb,ch} : {output:.4f}")
                                except:
                                    print(f"BMu{nb_BMu}, {s:02d}, {'P' if s in SUBJECT_P else 'C'} {c,task,nb,ch} : None ")

                        except Exception as e: 
                            print([e,s,c,task,nb,ch])
                            error += [[e,s,c,task,nb,ch]]

            except Exception as e:
                print([e,s,c,task,'all','all'])
                error += [[e,s,c,task,'all','all']]
                
    
    return BMu_results,BMu_result_detailed,BMu_result_detailed_rel,error





def BMu_patients_all_rel_channel_averaged(BMu_function,BMu_preprocessing,nb_BMu,event_bool, mvc_bool, task,task_mvc,channel_BMu,session = 2,show = True,**args ):
    """Compute the BMu across all patient files

    Args:
        BMu_function (function): function that computes the BMu
        nb_BMu (int):number of the BMu
        event_bool (_type_): True if event have to be detected
        mvc_bool (_type_): True if MVC has to be used
        task (_type_): name of the studied task
        task_mvc (_type_): name of the studied MVC task, '' otherwise
        channel_BMu (_type_): list of the muscle studied. ex: ['L_LES','R_LES']
        session (int, optional): 'INI' or 'REL' or 2. Defaults to 2 to compute the BMu across INI and REL session
        show (bool, optional): shows the value at the end of computation. Defaults to True.

    Returns:
        Dataframe: Dataframe of the BMu values and detailed BMu values (for each subphase)
    """

    BMu_results = []
    BMu_result_detailed = []
    BMu_result_detailed_rel = []

    error = []
    assert session in ['INI','REL',2]
    assert task_mvc in ['Endurance_Sorensen','Endurance_Ito','']

    for s in SUBJECT:

        for c in ['INI','REL' if session ==2 else session]:
            
            try:
                for nb in range(1, get_number_movement(s,task,c,format = 'csv')+1):
                    
                    file = load_file_subject_mvt_session2(s,mvt=task,session=c,n=nb)
                    
                    if event_bool == True:
                        event = load_event_subject_mvt_session2(s,task,c,n = nb)
                    else:
                        event = None
                    
                    
                    if mvc_bool:
                        sub_MVIC = load_file_subject_mvt_session2(s,task_mvc,c)
                        try:
                            sub_MVIC_event = load_event_subject_mvt_session2(s,task_mvc,c)
                        except:
                            sub_MVIC_event = pd.DataFrame(data=None,columns=['event','time_sec','time_min'])

                    else:
                        sub_MVIC_event = None


                    try:
                        if mvc_bool:
                            emg_sub_mvic1 = sub_MVIC[channel_BMu[0]]
                            emg_sub_mvic2 = sub_MVIC[channel_BMu[1]]
                            emg_sub_mvic = (emg_sub_mvic1+emg_sub_mvic2)/2
                        else:
                            emg_sub_mvic = None
                        
                

                        if nb_BMu == 91:
                            emg1 = file[channel_BMu[0]][5*SAMPLING_RATE:-5*SAMPLING_RATE]
                            emg2 = file[channel_BMu[1]][5*SAMPLING_RATE:-5*SAMPLING_RATE]
                        else:
                            emg1 = file[channel_BMu[0]]
                            emg2 = file[channel_BMu[1]]


                        kwargs = {'nb_BM': nb_BMu,'muscle':channel_BMu[0],'session':c,'subject':s}
                        
                        if args != {}:
                            for k,v in zip(list(args.keys()),list(args.values())):
                                if k not in list(kwargs.keys()):
                                    kwargs[k]=v

                        filtered_emg_1 = np.array(BMu_preprocessing(emg1))
                        filtered_emg_2 = np.array(BMu_preprocessing(emg2))

                        emg = (filtered_emg_1 + filtered_emg_2) /2
                                
                        output,output_detailed,output_rel = BMu_function(emg,event,emg_sub_mvic,sub_MVIC_event,**kwargs)
                        BMu_results += [[f'BMu{nb_BMu}',s,'P' if s in SUBJECT_P else 'C',c,task,nb,f'R_L_{channel_BMu[0][2:]}',output]]

                        informations = np.array([f'BMu{nb_BMu}',s,'P' if s in SUBJECT_P else 'C',c,task,nb,f'R_L_{channel_BMu[0][2:]}']*output_detailed.shape[0]).reshape(output_detailed.shape[0],-1)
                        df_informations = pd.DataFrame(informations,columns=['BMu','subject','P_C','session','task','trials','channel'])
                        df_detailed = pd.concat([df_informations,  output_detailed], axis=1, ignore_index=False)

                        informations_rel = np.array([f'BMu{nb_BMu}',f'P{s:02d}','PAT' if s in SUBJECT_P else 'CTR',c,task,nb,f'R_L_{channel_BMu[0][2:]}','FM']*output_rel.shape[0]).reshape(output_rel.shape[0],-1)
                        df_informations_rel = pd.DataFrame(informations_rel,columns=['BMu','Participant','Group','Session','task','trials','channel','Operator'])
                        df_detailed_rel = pd.concat([df_informations_rel,  output_rel], axis=1, ignore_index=False)

                        
                        BMu_result_detailed += list(df_detailed.values)
                        BMu_result_detailed_rel += list(df_detailed_rel.values)


                        if show:
                            try:
                                print(f"BMu{nb_BMu}, {s:02d}, {'P' if s in SUBJECT_P else 'C'} {c,task,nb,f'R_L_{channel_BMu[0][2:]}'} : {output:.4f}")
                            except:
                                print(f"BMu{nb_BMu}, {s:02d}, {'P' if s in SUBJECT_P else 'C'} {c,task,nb,f'R_L_{channel_BMu[0][2:]}'} : None ")

                    except Exception as e: 
                        print([e,s,c,task,nb,f'R_L_{channel_BMu[0][2:]}'])
                        error += [[e,s,c,task,nb,f'R_L_{channel_BMu[0][2:]}']]

            except Exception as e:
                print([e,s,c,task,'all','all'])
                error += [[e,s,c,task,'all','all']]
                
    
    return BMu_results,BMu_result_detailed,BMu_result_detailed_rel,error




# SAVING FUNCTIONS


def save_BMu(BMu_result,file_name,path_repo = 'results\\',replace = False, format = '.csv'):
    """Save the nested list of results of the computation of a biomarkers into csv or json file

    Args:
        BMu_result (_type_): nested list
        file_name (_type_): BMu[number] or BMu[number]_[number]
        path_repo (str, optional): _description_. Defaults to 'results\'.
        replace (bool, optional): _description_. Defaults to False.
        format (str, optional): _description_. Defaults to '.csv'.
    """

    assert format in ['.csv','.json'], 'unknown format'
    assert type(BMu_result) == list, 'BMu is not a list'

    path_file = path_repo + file_name + '_values' + format
    file_ex = False

    if exists(path_file):
        file_ex = True

    if file_ex and not replace:
        print('The file already exists')
    
    else:
        df_BMu_result = pd.DataFrame(BMu_result, columns = ['BM','subject','P_C','session','task','repetition','channel','value'])
        if format == '.csv':
            df_BMu_result.to_csv(path_file, index = False)
        elif format == '.json':
            df_BMu_result.to_json(path_file,orient='index')

        if file_ex:
            print(f'file saved again {path_file}') 

        else:
            print(f'file saved {path_file}') 





def save_BMu_detailed(BMu_result,file_name,path_repo = 'results\\detailed\\',replace = False, format = '.csv'):
    """Save the nested list of results of the computation of a biomarkers into csv or json file

    Args:
        BMu_result (_type_): nested list
        file_name (_type_): file name without extension
        path_repo (str, optional): _description_. Defaults to 'results\'.
        replace (bool, optional): _description_. Defaults to False.
        format (str, optional): _description_. Defaults to '.csv'.
    """

    assert format in ['.csv','.json'], 'unknown format'

    path_file = path_repo + file_name + '_detailed' + format
    file_ex = False

    if exists(path_file):
        file_ex = True

    if file_ex and not replace:
        print('The file already exists')
    
    else:
        df_BMu_result = pd.DataFrame(BMu_result, columns = ['BMu','subject','P_C','session','task','trials','channel','value','event','rep'])

        if format == '.csv':
            df_BMu_result.to_csv(path_file, index = False)
        elif format == '.json':
            df_BMu_result.to_json(path_file,orient='index')

        if file_ex:
            print(f'file saved again {path_file}') 

        else:
            print(f'file saved {path_file}') 


def save_BMu_detailed_rel(BMu_result,file_name,path_repo = 'results\\detailed_rel\\',replace = False, format = '.csv'):
    """Save the nested list of results of the computation of a biomarkers into csv or json file

    Args:
        BMu_result (_type_): nested list
        file_name (_type_): file name without extension
        path_repo (str, optional): _description_. Defaults to 'results\'.
        replace (bool, optional): _description_. Defaults to False.
        format (str, optional): _description_. Defaults to '.csv'.
    """

    assert format in ['.csv','.json'], 'unknown format'

    path_file = path_repo + file_name + '_rel' + format
    file_ex = False

    if exists(path_file):
        file_ex = True

    if file_ex and not replace:
        print('The file already exists')
    
    else:
        df_BMu_result = pd.DataFrame(BMu_result, columns = ['BMu','Participant','Group','Session','task','trials','channel','Operator','Measure','Value'] )

        if format == '.csv':
            df_BMu_result.to_csv(path_file, index = False)
        elif format == '.json':
            df_BMu_result.to_json(path_file,orient='index')

        if file_ex:
            print(f'file saved again {path_file}') 

        else:
            print(f'file saved {path_file}') 




def save_BMu_error(BMu_result,file_name,path_repo = 'results\\error\\',replace = False, format = '.csv'):
    """Save the nested list of results of the computation of a biomarkers into csv or json file

    Args:
        BMu_result (_type_): nested list
        file_name (_type_): file name without extension
        path_repo (str, optional): _description_. Defaults to 'results\'.
        replace (bool, optional): _description_. Defaults to False.
        format (str, optional): _description_. Defaults to '.csv'.
    """

    assert format in ['.csv','.json'], 'unknown format'

    path_file = path_repo + file_name + '_error' + format
    file_ex = False

    if exists(path_file):
        file_ex = True

    if file_ex and not replace:
        print('The file already exists')
    
    else:
        df_BMu_result = pd.DataFrame(BMu_result, columns = ['error','subject','session','task','trial','channel'])

        if format == '.csv':
            df_BMu_result.to_csv(path_file, index = False)
        elif format == '.json':
            df_BMu_result.to_json(path_file,orient='index')

        if file_ex:
            print(f'file saved again {path_file}') 

        else:
            print(f'file saved {path_file}') 



















## OBSOLETE


# def load_BMu(file_name,path_repo = 'results\\', format = '.csv', file = '_values'):
#     """load the results (csv ou json) of a Biomarker computation into a dataframe

#     Args:
#         file_name (str): name of the file, without the file format
#         path_repo (str, optional): _description_. Defaults to 'results\'.
#         format (str, optional): _description_. Defaults to '.csv'.

#     Returns:
#         Dataframe: return the results of a biomarker computation
#     """

#     assert format in ['.csv','.json'], 'Unknown file format'

#     path_file = path_repo + file_name + file + format
#     assert exists (path_file), 'the file does no exist'

#     try :
#         if format == '.csv':
#             df = pd.read_csv(path_file)
#         elif format == '.json':
#             df = pd.read_json(path_file)

#     except Exception as e:
#         print(f'Unable to load file, {e}')

#     return df









# def BMu_patients_all_rel_exclude(BMu_function,nb_BMu,event_bool, mvc_bool, task,task_mvc,channel_BMu,exclude=True,lvl=10,session = 2,show = True,**args ):
#     """Compute the BMu across all patient files

#     Args:
#         BMu_function (function): function that computes the BMu
#         nb_BMu (int):number of the BMu
#         event_bool (_type_): True if event have to be detected
#         mvc_bool (_type_): True if MVC has to be used
#         task (_type_): name of the studied task
#         task_mvc (_type_): name of the studied MVC task, '' otherwise
#         channel_BMu (_type_): list of the muscle studied. ex: ['L_LES','R_LES']
#         session (int, optional): 'INI' or 'REL' or 2. Defaults to 2 to compute the BMu across INI and REL session
#         show (bool, optional): shows the value at the end of computation. Defaults to True.

#     Returns:
#         Dataframe: Dataframe of the BMu values and detailed BMu values (for each subphase)
#     """

#     BMu_results = []
#     BMu_result_detailed = []
#     BMu_result_detailed_rel = []

#     error = []
#     assert session in ['INI','REL',2]
#     assert task_mvc in ['Endurance_Sorensen','Endurance_Ito','']

#     df_excluded = pd.read_csv('C:\\Users\\Anais\\Studio Code\\Project 1\\nslbp-bio_toolbox2\\results\\excluded.csv')
#     df_excluded = df_excluded.loc[df_excluded['task']==task]
#     # for s in SUBJECT:
#     for s in SUBJECT:

#         for c in ['INI','REL' if session ==2 else session]:
            
#             try:
#                 for nb in range(1, get_number_movement(s,task,c,format = 'csv')+1):
                    
#                     file = load_file_subject_mvt_session2(s,mvt=task,session=c,n=nb)
                    
#                     if event_bool == True:
#                         event = load_event_subject_mvt_session2(s,task,c,n = nb)
#                     else:
#                         event = None
                    
                    
#                     if mvc_bool:
#                         sub_MVIC = load_file_subject_mvt_session2(s,task_mvc,c)
#                         try:
#                             sub_MVIC_event = load_event_subject_mvt_session2(s,task_mvc,c)
#                         except:
#                             sub_MVIC_event = pd.DataFrame(data=None,columns=['event','time_sec','time_min'])

#                     else:
#                         sub_MVIC_event = None

#                     for ch in channel_BMu:

#                         try:
#                             if mvc_bool:
#                                 emg_sub_mvic = sub_MVIC[ch]
#                             else:
#                                 emg_sub_mvic = None
                                
#                             emg = file[ch]
#                             kwargs = {'nb_BM': nb_BMu,'muscle':ch,'session':c,'subject':s}
                            
#                             if args != {}:
#                                 for k,v in zip(list(args.keys()),list(args.values())):
#                                     if k not in list(kwargs.keys()):
#                                         kwargs[k]=v

#                             if exclude:
#                                 exclude_sig = df_excluded.loc[(df_excluded['subject']==s)&(df_excluded['session']==c)&(df_excluded['channel']==ch)&(df_excluded['trial']==nb),:]
#                                 if exclude_sig.shape[0] != 0:
#                                     excluded_bool = True if ( exclude_sig['excluded'].values == 1 ) or ( exclude_sig['noise'].values >= lvl)  else False
#                                 else:
#                                     excluded_bool = False
                            
#                             if (not exclude) or (exclude and not excluded_bool):
#                                 output,output_detailed,output_rel = BMu_function(emg,event,emg_sub_mvic,sub_MVIC_event,**kwargs)
#                                 BMu_results += [[f'BMu{nb_BMu}',s,'P' if s in SUBJECT_P else 'C',c,task,nb,ch,output]]

#                                 informations = np.array([f'BMu{nb_BMu}',s,'P' if s in SUBJECT_P else 'C',c,task,nb,ch]*output_detailed.shape[0]).reshape(output_detailed.shape[0],-1)
#                                 df_informations = pd.DataFrame(informations,columns=['BMu','subject','P_C','session','task','trials','channel'])
#                                 df_detailed = pd.concat([df_informations,  output_detailed], axis=1, ignore_index=True)

#                                 informations_rel = np.array([f'BMu{nb_BMu}',f'P{s:02d}','PAT' if s in SUBJECT_P else 'CTR',c,task,nb,ch,'FM']*output_rel.shape[0]).reshape(output_rel.shape[0],-1)
#                                 df_informations_rel = pd.DataFrame(informations_rel,columns=['BMu','Participant','Group','Session','task','trials','channel','Operator'])
#                                 df_detailed_rel = pd.concat([df_informations_rel,  output_rel], axis=1, ignore_index=True)

                                
#                                 BMu_result_detailed += list(df_detailed.values)
#                                 BMu_result_detailed_rel += list(df_detailed_rel.values)


#                                 if show:
#                                     try:
#                                         print(f"BMu{nb_BMu}, {s:02d}, {'P' if s in SUBJECT_P else 'C'} {c,task,nb,ch} : {output:.4f}")
#                                     except:
#                                         print(f"BMu{nb_BMu}, {s:02d}, {'P' if s in SUBJECT_P else 'C'} {c,task,nb,ch} : None ")
#                             else:
#                                 raise Exception ('excluded')

#                         except Exception as e: 
#                             print([e,s,c,task,nb,ch])
#                             error += [[e,s,c,task,nb,ch]]

#             except Exception as e:
#                 print([e,s,c,task,'all','all'])
#                 error += [[e,s,c,task,'all','all']]
                
    
#     return BMu_results,BMu_result_detailed,BMu_result_detailed_rel,error






# def BMu_patients_all(BMu_function,nb_BMu,event_bool, mvc_bool, task,task_mvc,channel_BMu,session = 2,show = True,**args ): #obsolete
#     """Compute the BMu across all patient files

#     Args:
#         BMu_function (function): function that computes the BMu
#         nb_BMu (int):number of the BMu
#         event_bool (_type_): True if event have to be detected
#         mvc_bool (_type_): True if MVC has to be used
#         task (_type_): name of the studied task
#         task_mvc (_type_): name of the studied MVC task, '' otherwise
#         channel_BMu (_type_): list of the muscle studied. ex: ['L_LES','R_LES']
#         session (int, optional): 'INI' or 'REL' or 2. Defaults to 2 to compute the BMu across INI and REL session
#         show (bool, optional): shows the value at the end of computation. Defaults to True.

#     Returns:
#         Dataframe: Dataframe of the BMu values and detailed BMu values (for each subphase)
#     """

#     BMu_results = []
#     BMu_result_detailed = []

#     error = []
#     assert session in ['INI','REL',2]
#     assert task_mvc in ['Endurance_Sorensen','Endurance_Ito','']

#     # for s in SUBJECT:
#     for s in SUBJECT:

#         for c in ['INI','REL' if session ==2 else session]:
            
#             try:
#                 for nb in range(1, get_number_movement(s,task,c,format = 'csv')+1):
                    
#                     file = load_file_subject_mvt_session2(s,mvt=task,session=c,n=nb)
                    
#                     if event_bool == True:
#                         event = load_event_subject_mvt_session2(s,task,c,n = nb)
#                     else:
#                         event = None
                    
                    
#                     if mvc_bool:
#                         sub_MVIC = load_file_subject_mvt_session2(s,task_mvc,c)
#                         try:
#                             sub_MVIC_event = load_event_subject_mvt_session2(s,task_mvc,c)
#                         except:
#                             sub_MVIC_event = pd.DataFrame(data=None,columns=['event','time_sec','time_min'])

#                     else:
#                         sub_MVIC_event = None

#                     for ch in channel_BMu:

#                         try:
#                             if mvc_bool:
#                                 emg_sub_mvic = sub_MVIC[ch]
#                             else:
#                                 emg_sub_mvic = None
                                
#                             emg = file[ch]
#                             kwargs = {'nb_BM': nb_BMu,'muscle':ch,'session':c,'subject':s}
                            
#                             if args != {}:
#                                 for k,v in zip(list(args.keys()),list(args.values())):
#                                     if k not in list(kwargs.keys()):
#                                         kwargs[k]=v
                                    
#                             output,output_detailed = BMu_function(emg,event,emg_sub_mvic,sub_MVIC_event,**kwargs)
#                             BMu_results += [[f'BMu{nb_BMu}',s,'P' if s in SUBJECT_P else 'C',c,task,nb,ch,output]]

#                             informations = np.array([f'BMu{nb_BMu}',s,'P' if s in SUBJECT_P else 'C',c,task,nb,ch]*output_detailed.shape[0]).reshape(output_detailed.shape[0],-1)
#                             df_informations = pd.DataFrame(informations,columns=['BMu','subject','P_C','session','task','trials','channel'])
#                             df_detailed = pd.concat([df_informations,  output_detailed], axis=1, ignore_index=False)
                            
#                             BMu_result_detailed += list(df_detailed.values)

#                             if show:
#                                 try:
#                                     print(f"BMu{nb_BMu}, {s:02d}, {'P' if s in SUBJECT_P else 'C'} {c,task,nb,ch} : {output:.4f}")
#                                 except:
#                                     print(f"BMu{nb_BMu}, {s:02d}, {'P' if s in SUBJECT_P else 'C'} {c,task,nb,ch} : None ")

#                         except Exception as e: 
#                             print([e,s,c,task,nb,ch])
#                             error += [[e,s,c,task,nb,ch]]

#             except Exception as e:
#                 print([e,s,c,task,'all','all'])
#                 error += [[e,s,c,task,'all','all']]
                
    
#     return BMu_results,BMu_result_detailed,error










# def BMu_patients_all_channel_averaged(BMu_function,BMu_preprocessing,nb_BMu,event_bool, mvc_bool, task,task_mvc,channel_BMu,session = 2,show = True,**args ): #obsolete
#     """Compute the BMu across all patient files

#     Args:
#         BMu_function (function): function that computes the BMu
#         nb_BMu (int):number of the BMu
#         event_bool (_type_): True if event have to be detected
#         mvc_bool (_type_): True if MVC has to be used
#         task (_type_): name of the studied task
#         task_mvc (_type_): name of the studied MVC task, '' otherwise
#         channel_BMu (_type_): list of the muscle studied. ex: ['L_LES','R_LES']
#         session (int, optional): 'INI' or 'REL' or 2. Defaults to 2 to compute the BMu across INI and REL session
#         show (bool, optional): shows the value at the end of computation. Defaults to True.

#     Returns:
#         Dataframe: Dataframe of the BMu values and detailed BMu values (for each subphase)
#     """

#     BMu_results = []
#     BMu_result_detailed = []

#     error = []
#     assert session in ['INI','REL',2]
#     assert task_mvc in ['Endurance_Sorensen','Endurance_Ito','']

#     # for s in SUBJECT:
#     for s in SUBJECT:

#         for c in ['INI','REL' if session ==2 else session]:
            
#             try:
#                 for nb in range(1, get_number_movement(s,task,c,format = 'csv')+1):
                    
#                     file = load_file_subject_mvt_session2(s,mvt=task,session=c,n=nb)
                    
#                     if event_bool == True:
#                         event = load_event_subject_mvt_session2(s,task,c,n = nb)
#                     else:
#                         event = None
                    
                    
#                     if mvc_bool:
#                         sub_MVIC = load_file_subject_mvt_session2(s,task_mvc,c)
#                         try:
#                             sub_MVIC_event = load_event_subject_mvt_session2(s,task_mvc,c)
#                         except:
#                             sub_MVIC_event = pd.DataFrame(data=None,columns=['event','time_sec','time_min'])

#                     else:
#                         sub_MVIC_event = None

#                     # for ch in channel_BMu:

#                     try:
#                         if mvc_bool:
#                             emg_sub_mvic1 = sub_MVIC[channel_BMu[0]]
#                             emg_sub_mvic2 = sub_MVIC[channel_BMu[1]]
#                             emg_sub_mvic = (emg_sub_mvic1+emg_sub_mvic2)/2
#                         else:
#                             emg_sub_mvic = None
                        
                   

#                         if nb_BMu == 91:
#                             emg1 = file[channel_BMu[0]][5*SAMPLING_RATE:-5*SAMPLING_RATE]
#                             emg2 = file[channel_BMu[1]][5*SAMPLING_RATE:-5*SAMPLING_RATE]
#                         else:
#                             emg1 = file[channel_BMu[0]]
#                             emg2 = file[channel_BMu[1]]


#                         kwargs = {'nb_BM': nb_BMu,'muscle':channel_BMu[0],'session':c,'subject':s}
                        
#                         if args != {}:
#                             for k,v in zip(list(args.keys()),list(args.values())):
#                                 if k not in list(kwargs.keys()):
#                                     kwargs[k]=v

#                         filtered_emg_1 = np.array(BMu_preprocessing(emg1))
#                         filtered_emg_2 = np.array(BMu_preprocessing(emg2))

#                         emg = (filtered_emg_1 + filtered_emg_2) /2
                                
#                         output,output_detailed = BMu_function(emg,event,emg_sub_mvic,sub_MVIC_event,**kwargs)
#                         BMu_results += [[f'BMu{nb_BMu}',s,'P' if s in SUBJECT_P else 'C',c,task,nb,f'R_L_{channel_BMu[0][2:]}',output]]

#                         informations = np.array([f'BMu{nb_BMu}',s,'P' if s in SUBJECT_P else 'C',c,task,nb,f'R_L_{channel_BMu[0][2:]}']*output_detailed.shape[0]).reshape(output_detailed.shape[0],-1)
#                         df_informations = pd.DataFrame(informations,columns=['BMu','subject','P_C','session','task','trials','channel'])
#                         df_detailed = pd.concat([df_informations,  output_detailed], axis=1)
                        
#                         BMu_result_detailed += list(df_detailed.values)

#                         if show:
#                             print(f"BMu{nb_BMu}, {s:02d}, {'P' if s in SUBJECT_P else 'C'} {c,task,nb,'mean of channels'} : {output:.4f}")
                            
#                     except Exception as e: 
#                         print([e,s,c,task,nb,'both'])
#                         error += [[e,s,c,task,nb,'both']]

#             except Exception as e:
#                 print([e,s,c,task,'all','all'])
#                 error += [[e,s,c,task,'all','all']]
                
    
#     return BMu_results,BMu_result_detailed,error
